//
//  AppDelegate.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/8/25.
//  Copyright © 2017年 PJL. All rights reserved.
//
//com.pjl.pjlNet
#import <UIKit/UIKit.h>
#import "PJLVersionModel.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic)  PJLVersionModel *localModel;


@end

