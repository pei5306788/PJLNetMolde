//
//  ViewController.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/8/25.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "ViewController.h"
#import "PJLRequestModel.h"

#define titleArry @[@"正常请求",@"单／多张图片上传",@"单／多张图片下载"]

@interface ViewController (){


    UILabel *showLabel;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    showLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height - 100)];
    
    [showLabel setFont:[UIFont systemFontOfSize:12.0]];
    
    [showLabel setNumberOfLines:0];
    
    
    [showLabel setTextColor:[UIColor blackColor]];
    
    [self.view addSubview:showLabel];
    
    
    
    
    
    
    CGFloat getWidth = self.view.frame.size.width/titleArry.count;
    
    for(NSInteger i = 0 ; i < titleArry.count; i++){
    
        UIButton *modelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        
        modelButton.frame = CGRectMake(i * getWidth, 0, getWidth, 100);
        
        
        [modelButton setTitle:titleArry[i]
                     forState:UIControlStateNormal];
        
        [modelButton setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        
        [modelButton.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        
        [modelButton setBackgroundColor:[UIColor clearColor]];
        
        [modelButton.titleLabel setNumberOfLines:0];
        
        [self.view addSubview:modelButton];
        
        [modelButton addTarget:self
                        action:@selector(chooseNet:)
              forControlEvents:UIControlEventTouchUpInside];
        
        modelButton.tag = i ;
        
        
        
    
    }
    
    
    
    
    
       // Do any additional setup after loading the view, typically from a nib.
}


- (void)chooseNet:(UIButton*)paramButton{

    showLabel.text = titleArry[paramButton.tag];
    if(paramButton.tag == 0){
    
        
        /*测试数据  http://apicloud.mob.com
         /v1/postcode/query
         ?key=appkey&code=102629*/
        
        
        
        PJLRequestModel *pjlRequestModel = [[PJLRequestModel alloc]init];
        
         [PJLAFSectionMangeShareInstance shareInstance].PJLServer = @"http://localhost";
         [PJLAFSectionMangeShareInstance shareInstance].PJLServerPort = @"8080";
        
        
        pjlRequestModel.paramDic = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"title1",@"title",
                                    @"pjlAdd1",@"pjlAdd",
                                    nil];
        pjlRequestModel.apiName = @"todos";
        
        pjlRequestModel.isGet = NO;
        
        pjlRequestModel.pjlIsNeedCache = YES;
        
        
        [pjlRequestModel sendRequestWithsuccess:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
            
            //通知主线程刷新
            dispatch_async(dispatch_get_main_queue(), ^{
                self->showLabel.text = [NSString stringWithFormat:@"%@\n请求成功：%@",self->showLabel.text,responseObject];
                
            });
            
            
        } faileure:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
            
            NSLog(@"%@",responseObject);
            //通知主线程刷新
            dispatch_async(dispatch_get_main_queue(), ^{
                self->showLabel.text = [NSString stringWithFormat:@"%@\n请求失败：%@",self->showLabel.text,responseObject];
                
            });
            
            
        } progress:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
            
            NSProgress *getProgress = responseObject;
            
            
            
            //        getProgress.isCancelled
            
            
            NSLog(@"%@,%lf",responseObject,getProgress.fractionCompleted);
            //通知主线程刷新
            dispatch_async(dispatch_get_main_queue(), ^{
                self->showLabel.text = [NSString stringWithFormat:@"%@\n请求进度：%lf",self->showLabel.text,getProgress.fractionCompleted];
            });
            
        }];

    
    }
    
    if(paramButton.tag == 1){ //目前就是测试其他的还没仔细册是中
    
        
        PJLRequestModel *pjlRequestModel = [[PJLRequestModel alloc]init];
        
        NSDictionary *paramDic = [NSDictionary dictionaryWithObjectsAndKeys:@"txt_photo",@"paramName",
                                  @"",@"fileImgSize",
                                  @"ds_upload",@"storeName",
                                  nil];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"pjlTestImages" ofType:@"jpg"];
        pjlRequestModel.paramDic = paramDic;
        pjlRequestModel.baseUrl = @"http://192.168.1.141";
        pjlRequestModel.apiName = @"up_photo";
        pjlRequestModel.serverPort = @"5000";
        pjlRequestModel.filePathArry = @[filePath];
        pjlRequestModel.paramNameArry = @[@"txt_photo"];
    
        
        [pjlRequestModel upImagesRequestWithuSuccess:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
            
            //通知主线程刷新
            dispatch_async(dispatch_get_main_queue(), ^{
                self->showLabel.text = [NSString stringWithFormat:@"%@\n请求成功：%@",self->showLabel.text,responseObject];
                
            });
            
            
        } faileure:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
            
            NSLog(@"%@",responseObject);
            //通知主线程刷新
            dispatch_async(dispatch_get_main_queue(), ^{
                self->      showLabel.text = [NSString stringWithFormat:@"%@\n请求失败：%@",self->showLabel.text,responseObject];
                
            });
            
            
        } progress:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
            
            NSProgress *getProgress = responseObject;
            
            
            
            //        getProgress.isCancelled
            
            
            NSLog(@"%@,%lf",responseObject,getProgress.fractionCompleted);
            //通知主线程刷新
            dispatch_async(dispatch_get_main_queue(), ^{
                self->showLabel.text = [NSString stringWithFormat:@"%@\n请求进度：%lf",self->showLabel.text,getProgress.fractionCompleted];
            });
        }];

    
    }

    
    if(paramButton.tag == 2){
    //示列:http://www.baidu.com/img/bdlogo.png
        
        PJLRequestModel *pjlRequestModel = [[PJLRequestModel alloc]init];

        pjlRequestModel.baseUrl = @"http://www.baidu.com";
        pjlRequestModel.apiName = @"img";
        pjlRequestModel.fileName = @"/bdlogo.png";
        pjlRequestModel.serverPort = @""; //不需要端口 就这样写 ，不然就会读取默认的
        
        [pjlRequestModel downLoadRequestSucess:^(NSString *ApiName,
                                                 id responseObject,
                                                 NSString *otherFlag) {
            
            //通知主线程刷新
            dispatch_async(dispatch_get_main_queue(), ^{
                //回调或者说是通知主线程刷新，
                self->showLabel.text = [NSString stringWithFormat:@"%@\n下载成功：%@",self->showLabel.text,responseObject];
                
            });
            
        } faileure:^(NSString *ApiName,
                     id responseObject,
                     NSString *otherFlag) {
            //通知主线程刷新
            dispatch_async(dispatch_get_main_queue(), ^{
                self->showLabel.text = [NSString stringWithFormat:@"%@\n下载失败：%@",self->showLabel.text,responseObject];
                
            });
            
        } progress:^(NSString *ApiName,
                     id responseObject,
                     NSString *otherFlag) {
            
            NSProgress *getProgress = responseObject;
            
          
            //通知主线程刷新
            dispatch_async(dispatch_get_main_queue(), ^{
                self->showLabel.text = [NSString stringWithFormat:@"%@\n下载进度：%lf",self->showLabel.text,getProgress.fractionCompleted];
                
            });
            
        }];
    
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
