//
//  PJLRequestTestModel.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/8/31.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PJLUserNetObject.h"
@interface PJLRequestModel : PJLUserNetObject

/*
 上传和下载的图片不会起作用
 pjlIsNeedCache 这个保存请求获取的数据 默认是NO
 需要本地缓存 默认是不缓存本地的数据
 如果请求失败的话
 本地有相关数据就会优先读取本地数据的
 特别注意：这个有利有弊 ，
 利：服务器在出现问题，可以让用户读取曾经的数据
 弊：就是数据是过时的数据可能造成不必要的误会，占用额外的储存空间
 适用场景：数据更新的特别慢 题库等等

 */
@property (nonatomic) BOOL pjlIsNeedCache;


@property (nonatomic) NSDictionary *paramDic;

@property (nonatomic) NSString *apiName;

@property (nonatomic) BOOL isNeedhud; //是否需要提示框

@property (nonatomic) NSString *loadStr; //载录等待的时间

@property (nonatomic) NSString *loadSucessStr;

@property (nonatomic) NSString *loadErrorStr;

@property (nonatomic) BOOL isGet;

@property (nonatomic) NSString *otherFlag;

@property (nonatomic) NSString *baseUrl;

@property (nonatomic) NSString *serverPort;

- (void)sendRequestWithsuccess:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
                      faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
                      progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress;


/*
 上传图片
 withMimeType:(NSArray *)paramMimeTypeArry
 withName:(NSArray *)paramNameArry
 withImageDataArry:(NSArray *)imageDataArry
 withImageFilePathArry:(NSArray *)filePathArry
 withFileName:(NSArray *)fileNameArry
 withAppendPartWithHeaders:(NSDictionary *)paramPartWithHeaders
 withBodyData:(NSData *)body
 
 */

@property (nonatomic) NSArray *paramMimeTypeArry;

@property (nonatomic) NSArray *paramNameArry;

@property (nonatomic) NSArray *imageDataArry;

@property (nonatomic) NSArray *filePathArry;

@property (nonatomic) NSArray *fileNameArry;

@property (nonatomic) NSDictionary *paramPartWithHeaders;

@property (nonatomic) NSData *body;

@property (nonatomic) NSString *paramMethod;

- (void)upImagesRequestWithuSuccess:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
                         faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
                         progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress;

/*
 下载图片
 
 */


@property (nonatomic) NSString *fileName; //下载图片名字

- (void)downLoadRequestSucess:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
                     faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
                     progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress;

@end
