//
//  PJLRequestTestModel.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/8/31.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLRequestModel.h"

@implementation PJLRequestModel

- (void)sendRequestWithsuccess:(void (^)(NSString *, id, NSString *))success
                      faileure:(void (^)(NSString *, id, NSString *))failure
                      progress:(void (^)(NSString *, id, NSString *))progress{

    [self UserRequest:_paramDic
               PJLAPI:_apiName
           setBaseUrl:_baseUrl
        setServerPort:_serverPort
         setOtherFlag:_otherFlag
               setGet:_isGet
            isNeedHUD:_isNeedhud
              hunLoad:_loadStr
           hunSuccess:_loadSucessStr
              hunFail:_loadErrorStr
              success:^(NSString *ApiName,
                        id responseObject,
                        NSString *otherFlag) {
                  
                  
                  if(self.pjlIsNeedCache == YES){ // 如果开启本地缓存
                      
                      [self saveRequest:responseObject];
                   

                  }
                  success(ApiName,responseObject,otherFlag);
                  
                  
    } faileure:^(NSString *ApiName,
                 id responseObject, NSString *otherFlag) {
        
        if(self.pjlIsNeedCache == YES){ // 如果开启本地缓存
            
            id  getSaveId = [self getSaveRequest];
            
            if(getSaveId != nil){
                
                 success(ApiName,getSaveId,otherFlag);
                
            }else{
                
                 failure(ApiName,responseObject,otherFlag);
            }
            
            
            
        }else{
        
            failure(ApiName,responseObject,otherFlag);
            
        }
        
    } progress:^(NSString *ApiName,
                 id responseObject, NSString *otherFlag) {
        
         progress(ApiName,responseObject,otherFlag);
    }];
    

}


- (void)upImagesRequestWithuSuccess:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
                          faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
                          progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress{

    [self UserSendImagesRequest:_paramDic
          withRequestWithMethod:_paramMethod
                         PJLAPI:_apiName
                     setBaseUrl:_baseUrl
                  setServerPort:_serverPort
                   setOtherFlag:_otherFlag
                   withMimeType:_paramMimeTypeArry
                       withName:_paramNameArry
              withImageDataArry:_imageDataArry
          withImageFilePathArry:_filePathArry
                   withFileName:_fileNameArry
      withAppendPartWithHeaders:_paramPartWithHeaders
                   withBodyData:_body
                      isNeedHUD:_isNeedhud
                        hunLoad:_loadStr
                     hunSuccess:_loadSucessStr
                        hunFail:_loadErrorStr
                        success:^(NSString *ApiName,
                                  id responseObject,
                                  NSString *otherFlag) {
                            
                             success(ApiName,responseObject,otherFlag);
        
    } faileure:^(NSString *ApiName,
                 id responseObject,
                 NSString *otherFlag) {
        
         failure(ApiName,responseObject,otherFlag);
        
    } progress:^(NSString *ApiName,
                 id responseObject,
                 NSString *otherFlag) {
        progress(ApiName,responseObject,otherFlag);

        
    }];
    
}


- (void)downLoadRequestSucess:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
                     faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
                     progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress{

   
    [self UserDownFileWithPJLAPI:_apiName setBaseUrl:_baseUrl setServerPort:_serverPort withFileName:_fileName withOtherFlag:_otherFlag isNeedHUD:_isNeedhud hunLoad:_loadStr hunSuccess:_loadSucessStr hunFail:_loadErrorStr success:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
        
        success(ApiName,responseObject,otherFlag);
        
    } faileure:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
        
        failure(ApiName,responseObject,otherFlag);
        
    } progress:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
        
        progress(ApiName,responseObject,otherFlag);
        
    }];
    
}

- (void)saveRequest:(id)responseObject{ //保存本地请求获取的数据
    
    NSString *keyNmae = self.apiName;
    if(self.paramDic != nil && [self.paramDic isKindOfClass:[NSDictionary class]]){
        
        keyNmae = [NSString stringWithFormat:@"%@%@",keyNmae,[self convertToJSONData:self.paramDic]];
    }
    
    
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    
    @try{
        [defaults setObject:responseObject
                     forKey:keyNmae];
        
    }@catch(NSException *e){
        
        NSLog(@"本地保存错误%@",
              e.userInfo.description);
    }
    
    [defaults synchronize];
    
}

- (id)getSaveRequest{
 
    NSString *keyNmae = self.apiName;
    if(self.paramDic != nil && [self.paramDic isKindOfClass:[NSDictionary class]]){
        
        keyNmae = [NSString stringWithFormat:@"%@%@",keyNmae,[self convertToJSONData:self.paramDic]];
    }
    
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    
    @try{
        if(keyNmae == nil){
            return nil;
        }
        return  [defaults objectForKey:keyNmae];
        
    }@catch(NSException *e){
        
        return nil;
    }
    
    
}

- (NSString*)convertToJSONData:(id)infoDict
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:infoDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    NSString *jsonString = @"";
    
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }else
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除掉首尾的空白字符和换行字符
    
    [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    return jsonString;
}

@end
