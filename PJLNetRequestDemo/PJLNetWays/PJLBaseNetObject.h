//
//  PJLNetObject.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/8/25.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <Foundation/Foundation.h>



#import "PJLAFSectionMangeShareInstance.h" //这个是请求用到相关的单列



/*
 
 这个就是我们请求需要分装 的请求框架 让请求简单话
 */

@interface PJLBaseNetObject : NSObject

@property (nonatomic) NSDictionary *headers; //这个 是Aft4.0 带的东西
//设置授权
@property (nonatomic,strong) NSDictionary *setBaseValueForHttpHeadFileDic;

/*
 *有默认值
 *@"application/json",
 *@"text/html",
 *@"text/json",
 *@"text/javascript",
 *@"text/plain",
 *@"application/html",
 */
@property (nonatomic) NSSet<NSString *>  *pjlAcceptableContentTypes;

@property (nonatomic) NSTimeInterval  requestTimer; ///请求超时时间

/**
 *@param paramDic 请求的字典 可以为nil
 *@param APIName 接口名字  http://192.168.0.1:8080/接口名字？后面的是参互 //不能为空的
 *@param baseUrl 服务器地址  服务器地址:8080/  可以为空。如果是空的话 会寻找默认的服务器地址
 *param serverPort 端口号   “8080”  可以为空，为nil 是读取默认的端口号
 *                                         为@"" 长度为0  那就是不需要端口号
 *@param otherFlag 这个是标示 请求成功，或失败都会带过去的， 属于拓展功能
*
 *@param isGet  是否是get 方法 yes 是get  no  是 post
*
 *@param isNeed 是否需要提示请求交互显示（让用户等待请求成功再做其他操作）
 *
 *@param loadStr 等待请求成功中 提示的话语  可以为空
 *
 *@param loadSuccess 请求成功提示的语言   可以为空
 *
 *@param loadFail 请求失败的提示语     可以为空
 *
 *@param success 请求成功走的方法 ApiName 请求的接口名字 responseObject 请求获得参数 otherFlag 拓展标示
 *
 *@param progress 请求进度监听
 */

- (void)PJLRequest:(NSDictionary *)paramDic
            PJLAPI:(NSString *)APIName
        setBaseUrl:(NSString *)baseUrl
     setServerPort:(NSString *)serverPort
      setOtherFlag:(NSString *)otherFlag
            setGet:(BOOL)isGet
         isNeedHUD:(BOOL)isNeed
           hunLoad:(NSString *)loadStr
        hunSuccess:(NSString *)loadSuccess
           hunFail:(NSString *)loadFail
           success:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
          faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
          progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress;




-(void)PJLSimpleRequest:(NSDictionary *)paramDic
                 PJLAPI:(NSString *)APIName
                setOtherFlag:(NSString *)otherFlag
                 setGet:(BOOL)isGet
              isNeedHUD:(BOOL)isNeed
                hunLoad:(NSString *)loadStr
             hunSuccess:(NSString *)loadSuccess
                hunFail:(NSString *)loadFail
                success:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
               faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
               progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress;

/*图片上传*/


/**
 *@param paramMimeTypeArry 常见类型
 *常见的MIME类型
 *超文本标记语言文本 .html,.html text/html
 *普通文本 .txt text/plain
 *RTF文本 .rtf application/rtf
 *GIF图形 .gif image/gif
 *JPEG图形 .ipeg,.jpg image/jpeg
 *au声音文件 .au audio/basic
 *MIDI音乐文件 mid,.midi audio/midi,audio/x-midi
 *RealAudio音乐文件 .ra, .ram audio/x-pn-realaudio
 *MPEG文件 .mpg,.mpeg video/mpeg
 *AVI文件 .avi video/x-msvideo
 *GZIP文件 .gz application/x-gzip
 *TAR文件 .tar application/x-tar
*
* @param paramNameArry 上传中存在参数 name 和 paramMimeTypeArry 为空则不读
*
* @param imageDataArry 以data 形式分装上传
*
* @param filePathArry 以本地文件形式上传  文件到文件
*
* @param fileNameArry  文件名字     和 paramMimeTypeArry 为空则不读取
 *
* @param paramPartWithHeaders   文件头参数
 *
 *@param body 内容主题   和 paramPartWithHeaders 为空则不读取
 *
 */


/*
 上传图片
 */
- (void)PJLSendImagesRequest:(NSDictionary *)paramDic
    withRequestWithMethod:(NSString *)paramMethod
                   PJLAPI:(NSString *)APIName
               setBaseUrl:(NSString *)baseUrl
            setServerPort:(NSString *)serverPort
             setOtherFlag:(NSString *)otherFlag
             withMimeType:(NSArray *)paramMimeTypeArry
                 withName:(NSArray *)paramNameArry
        withImageDataArry:(NSArray *)imageDataArry
    withImageFilePathArry:(NSArray *)filePathArry
             withFileName:(NSArray *)fileNameArry
withAppendPartWithHeaders:(NSDictionary *)paramPartWithHeaders
             withBodyData:(NSData *)body
                  success:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
                 faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
                 progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress;



@property (nonatomic) NSURLSessionDownloadTask *pjlDownloadTask; //大文件下载


/*
 下载文件 目前只能支持get方法 还没写post 方法
 */

- (void)PJLDownFileWithPJLAPI:(NSString *)APIName
                   setBaseUrl:(NSString *)baseUrl
                setServerPort:(NSString *)serverPort
                 withFileName:(NSString *)fileName
                withOtherFlag:(NSString *)otherFlag
                      success:(void (^)( id responseObject))success
                     faileure:(void (^)( id responseObject))failure
                     progress:(void (^)( id responseObject))progress;

- (void)PJLStopDownLoad; //暂停下载


-(void)PJLStartDownLoad; //暂停后继续下载

@property (nonatomic) NSURLSessionDataTask *task; //请求出来的 task

@end
