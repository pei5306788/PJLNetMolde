//
//  PJLAFSectionMangeShareInstance.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/8/25.
//  Copyright © 2017年 PJL. All rights reserved.
//
#import "AFNetworking.h"
#import "PJLAFSectionMangeShareInstance.h"
static PJLAFSectionMangeShareInstance *pjlShareInstance;


@implementation PJLAFSectionMangeShareInstance

+(PJLAFSectionMangeShareInstance *)shareInstance{

    @synchronized (self) {
        
        if(pjlShareInstance == nil){
        
            pjlShareInstance = [[self alloc]init];
            /*下面初始化一些我们常用的网络请求*/
            //外部调用监听走的方法
            pjlShareInstance.pjlListenID = [[PJLCheckNetStatus alloc]init];
        
            [pjlShareInstance PJLListenNetStataus];
            
            pjlShareInstance.timeoutInterval = 10;
        }
    }
    

    return pjlShareInstance;
}

- (id )sharedHTTPSession{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
            manager.requestSerializer.timeoutInterval =  pjlShareInstance.timeoutInterval;
        
          afnetManager = manager;
        });
        return afnetManager;
}
    
- (id )sharedURLSession{
    static dispatch_once_t onceToken2;
    dispatch_once(&onceToken2, ^{
        
        if(pjlShareInstance.defaultSessionConfigurationKind == 0){
            
             urlsession = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        }else{
            
              urlsession = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
        }
        
       
    });
    return urlsession;
}
   
    
- (void)PJLListenNetStataus{

    //网络监控句柄
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    
    //要监控网络连接状态，必须要先调用单例的startMonitoring方法
    [manager startMonitoring];
    
//    [manager stopMonitoring];
    
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        if(pjlShareInstance.pjlListenID != nil){
            
            if([pjlShareInstance.pjlListenID respondsToSelector:@selector(ReachabilityStatusChangeBlock:)]){
                //如果这个对象存在这个方法的话
                
                [pjlShareInstance.pjlListenID ReachabilityStatusChangeBlock:status];
            }
            
        }
        
        //status:
        //AFNetworkReachabilityStatusUnknown          = -1,  未知
        //AFNetworkReachabilityStatusNotReachable     = 0,   未连接
        //AFNetworkReachabilityStatusReachableViaWWAN = 1,   3G
        //AFNetworkReachabilityStatusReachableViaWiFi = 2,   无线连接
        
        
        pjlShareInstance.aFNetworkReachabilityStatus = status; //监听把数据保存到参数里面
        
        NSLog(@"%ld", status);
    }];


}
    
    
@end
