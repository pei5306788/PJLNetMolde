//
//  PJLCheckNetStatus.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/18.
//  Copyright © 2017年 PJL. All rights reserved.
//
//这个方法 通过继承可以自定义里面的方法 目前最好的也就是自定义，如果不自定义 那以后会提供简单的方法
#import <Foundation/Foundation.h>


@interface PJLCheckNetStatus : NSObject

-(void)ReachabilityStatusChangeBlock:(NSInteger)aFNetworkReachabilityStatus;

@end
