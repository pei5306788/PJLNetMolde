//
//  PJLAFSectionMangeShareInstance.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/8/25.
//  Copyright © 2017年 PJL. All rights reserved.
//

/*
 当多次请求的时候section 是不会释放的，
 一般是用单列的方法 整个请求的，
 但出现问题了 就是请求会丢失，
 这个类就是为了避免这个
 问题二创建限的  当然 正常项目请求一般同时只有一个
*/

/*测试数据  http://apicloud.mob.com
 /v1/postcode/query
 ?key=appkey&code=102629*/

//#define PJLServer @"http://apicloud.mob.com";
//
//#define PJLServerPort  @""  //测试数据端口号没有 有的话要输入的
//
//
//
//#define PJLTestAPIname  @"v1/postcode/query"   //测试接口名字



#import <Foundation/Foundation.h>
#import "PJLCheckNetStatus.h"

static id afnetManager ;
static id urlsession ;

@interface PJLAFSectionMangeShareInstance : NSObject
    
+(PJLAFSectionMangeShareInstance *)shareInstance;

@property (nonatomic) NSString *PJLServer; //服务器地址

@property (nonatomic) NSString *PJLServerPort; //端口号

@property (nonatomic) NSString *PJLTestAPIname; //接口名字


/*
 * 0 defaultSessionConfiguration
 * 1 ephemeralSessionConfiguration
 */

@property (nonatomic) NSInteger defaultSessionConfigurationKind;

@property (nonatomic) NSInteger timeoutInterval; //默认是10 这个没用 在base里面

/*
 AFNetworkReachabilityStatusUnknown          = -1,  未知
 AFNetworkReachabilityStatusNotReachable     = 0,   未连接
 AFNetworkReachabilityStatusReachableViaWWAN = 1,   3G
 AFNetworkReachabilityStatusReachableViaWiFi = 2,   无线连接
 
 */

@property (nonatomic) NSInteger aFNetworkReachabilityStatus;

/*监听网络 pjlListenID 对象 需要存在方法
 -(void)ReachabilityStatusChangeBlock:(NSInteger)aFNetworkReachabilityStatus{
 
 }
 
 */

@property (nonatomic) PJLCheckNetStatus *pjlListenID;
    
- (id )sharedHTTPSession;
- (id )sharedURLSession;

@end
