//
//  PJLUserNetObject.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/8/31.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLBaseNetObject.h"

@interface PJLUserNetObject : PJLBaseNetObject //这个里面的方法最好别修改 


/*
 @param paramDic 请求的字典 可以为nil
 @param APIName 接口名字  http://192.168.0.1:8080/接口名字？后面的是参互 //不能为空的
 @param baseUrl 服务器地址  服务器地址:8080/  可以为空。如果是空的话 会寻找默认的服务器地址
 param serverPort 端口号   “8080”  可以为空，为nil 是读取默认的端口号
 为@"" 长度为0  那就是不需要端口号
 @param setOtherFlag 这个是标示 请求成功，或失败都会带过去的， 属于拓展功能
 
 @param isGet  是否是get 方法 yes 是get  no  是 post
 
 @param isNeed 是否需要提示请求交互显示（让用户等待请求成功再做其他操作）
 
 @param loadStr 等待请求成功中 提示的话语  可以为空
 
 @param loadSuccess 请求成功提示的语言   可以为空
 
 @param loadFail 请求失败的提示语     可以为空
 
 @param success 请求成功走的方法 ApiName 请求的接口名字 responseObject 请求获得参数 otherFlag 拓展标示
 
 @param success 请求成功走的方法 ApiName 请求的接口名字 responseObject 错误信息erreo otherFlag 拓展标示
 
 @param progress 请求进度监听
 
 */


- (void)UserRequest:(NSDictionary *)paramDic
            PJLAPI:(NSString *)APIName
        setBaseUrl:(NSString *)baseUrl
     setServerPort:(NSString *)serverPort
      setOtherFlag:(NSString *)otherFlag
            setGet:(BOOL)isGet
         isNeedHUD:(BOOL)isNeed
           hunLoad:(NSString *)loadStr
        hunSuccess:(NSString *)loadSuccess
           hunFail:(NSString *)loadFail
           success:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
          faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
          progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress;




-(void)UserSimpleRequest:(NSDictionary *)paramDic
                 PJLAPI:(NSString *)APIName
           setOtherFlag:(NSString *)otherFlag
                 setGet:(BOOL)isGet
              isNeedHUD:(BOOL)isNeed
                hunLoad:(NSString *)loadStr
             hunSuccess:(NSString *)loadSuccess
                hunFail:(NSString *)loadFail
                success:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
               faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
               progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress;




/*
 上传图片
 */
- (void)UserSendImagesRequest:(NSDictionary *)paramDic
        withRequestWithMethod:(NSString *)paramMethod
                       PJLAPI:(NSString *)APIName
                   setBaseUrl:(NSString *)baseUrl
                setServerPort:(NSString *)serverPort
                 setOtherFlag:(NSString *)otherFlag
                 withMimeType:(NSArray *)paramMimeTypeArry
                     withName:(NSArray *)paramNameArry
            withImageDataArry:(NSArray *)imageDataArry
        withImageFilePathArry:(NSArray *)filePathArry
                 withFileName:(NSArray *)fileNameArry
    withAppendPartWithHeaders:(NSDictionary *)paramPartWithHeaders
                 withBodyData:(NSData *)body
                    isNeedHUD:(BOOL)isNeed
                      hunLoad:(NSString *)loadStr
                   hunSuccess:(NSString *)loadSuccess
                      hunFail:(NSString *)loadFail
                  success:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
                 faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
                 progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress;



/*
 下载文件 只能支持get 方法
 */

- (void)UserDownFileWithPJLAPI:(NSString *)APIName
                   setBaseUrl:(NSString *)baseUrl
                setServerPort:(NSString *)serverPort
                 withFileName:(NSString *)fileName
                withOtherFlag:(NSString *)otherFlag
                     isNeedHUD:(BOOL)isNeed
                       hunLoad:(NSString *)loadStr
                    hunSuccess:(NSString *)loadSuccess
                       hunFail:(NSString *)loadFail
                       success:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
                      faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
                      progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress;


@end
