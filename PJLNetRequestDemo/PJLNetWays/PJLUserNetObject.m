//
//  PJLUserNetObject.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/8/31.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLUserNetObject.h"

@implementation PJLUserNetObject


- (void)UserRequest:(NSDictionary *)paramDic
             PJLAPI:(NSString *)APIName
         setBaseUrl:(NSString *)baseUrl
      setServerPort:(NSString *)serverPort
       setOtherFlag:(NSString *)otherFlag
             setGet:(BOOL)isGet
          isNeedHUD:(BOOL)isNeed
            hunLoad:(NSString *)loadStr
         hunSuccess:(NSString *)loadSuccess
            hunFail:(NSString *)loadFail
            success:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
           faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
           progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress{

    
    [self userRequestStartNeedHUD:isNeed
                          hunLoad:loadStr
                       hunSuccess:loadSuccess
                          hunFail:loadFail
                     withnApiName:APIName
                    withOtherFlag:otherFlag];

    [self PJLRequest:paramDic
              PJLAPI:APIName
          setBaseUrl:baseUrl
       setServerPort:serverPort
        setOtherFlag:otherFlag
              setGet:isGet
           isNeedHUD:isNeed
             hunLoad:loadStr
          hunSuccess:loadSuccess
             hunFail:loadFail success:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
                 
                 [self userRequestEndDealNeedHUD:isNeed
                                         hunLoad:loadStr
                                      hunSuccess:loadSuccess
                                         hunFail:loadFail
                               withRequestStatus:1
                                    withnApiName:APIName
                            withIDresponseObject:responseObject
                                   withOtherFlag:otherFlag];
                 
//                 if(self.is)
        
                 success(APIName,responseObject,otherFlag);
                 
    } faileure:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
        
        
        [self userRequestEndDealNeedHUD:isNeed
                                hunLoad:loadStr
                             hunSuccess:loadSuccess
                                hunFail:loadFail
                      withRequestStatus:2
                           withnApiName:APIName
                   withIDresponseObject:responseObject
                          withOtherFlag:otherFlag];
        

        
        failure(APIName,responseObject,otherFlag);
        
    } progress:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
        
        [self userRequestEndDealNeedHUD:isNeed
                                hunLoad:loadStr
                             hunSuccess:loadSuccess
                                hunFail:loadFail
                      withRequestStatus:0
                           withnApiName:APIName
                   withIDresponseObject:responseObject
                          withOtherFlag:otherFlag];
        

        
        progress(ApiName,responseObject,otherFlag);
        
    }];
    
}

-(void)UserSimpleRequest:(NSDictionary *)paramDic
                  PJLAPI:(NSString *)APIName
            setOtherFlag:(NSString *)otherFlag
                  setGet:(BOOL)isGet
               isNeedHUD:(BOOL)isNeed
                 hunLoad:(NSString *)loadStr
              hunSuccess:(NSString *)loadSuccess
                 hunFail:(NSString *)loadFail
                 success:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
                faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
                progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress{

    [self UserRequest:paramDic
               PJLAPI:APIName
           setBaseUrl:nil
        setServerPort:nil
         setOtherFlag:otherFlag
               setGet:isGet
            isNeedHUD:isNeed
              hunLoad:loadStr
           hunSuccess:loadSuccess
              hunFail:loadFail
              success:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
        
          success(APIName,responseObject,otherFlag);
        
    } faileure:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
        
        failure(APIName,responseObject,otherFlag);
        
    } progress:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
        
        progress(ApiName,responseObject,otherFlag);
        
    }];
}

/*请求中或失败的相关操作*/
- (void)userRequestEndDealNeedHUD:(BOOL)isNeed
                          hunLoad:(NSString *)loadStr
                       hunSuccess:(NSString *)loadSuccess
                          hunFail:(NSString *)loadFail
                withRequestStatus:(NSInteger)kind  //0 是请求中 1 是请求成功 2 是请求失败
                     withnApiName:(NSString *)apiName
             withIDresponseObject:(id)responseObject
                    withOtherFlag:(NSString *)otherFlag{

    //这边就是额外处理 一些参数的问题
    

}

/*开始请求的相关提醒的操作*/
- (void)userRequestStartNeedHUD:(BOOL)isNeed
                        hunLoad:(NSString *)loadStr
                     hunSuccess:(NSString *)loadSuccess
                        hunFail:(NSString *)loadFail
                   withnApiName:(NSString *)apiName
                    withOtherFlag:(NSString *)otherFlag{

}

/*
 上传图片
 */
- (void)UserSendImagesRequest:(NSDictionary *)paramDic
        withRequestWithMethod:(NSString *)paramMethod
                       PJLAPI:(NSString *)APIName
                   setBaseUrl:(NSString *)baseUrl
                setServerPort:(NSString *)serverPort
                 setOtherFlag:(NSString *)otherFlag
                 withMimeType:(NSArray *)paramMimeTypeArry
                     withName:(NSArray *)paramNameArry
            withImageDataArry:(NSArray *)imageDataArry
        withImageFilePathArry:(NSArray *)filePathArry
                 withFileName:(NSArray *)fileNameArry
    withAppendPartWithHeaders:(NSDictionary *)paramPartWithHeaders
                 withBodyData:(NSData *)body
                    isNeedHUD:(BOOL)isNeed
                      hunLoad:(NSString *)loadStr
                   hunSuccess:(NSString *)loadSuccess
                      hunFail:(NSString *)loadFail
                      success:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
                     faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
                     progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress{

    
    [self PJLSendImagesRequest:paramDic
         withRequestWithMethod:paramMethod
                        PJLAPI:APIName
                    setBaseUrl:baseUrl
                 setServerPort:serverPort
                  setOtherFlag:otherFlag
                  withMimeType:paramMimeTypeArry
                      withName:paramNameArry
             withImageDataArry:imageDataArry
         withImageFilePathArry:filePathArry
                  withFileName:fileNameArry
     withAppendPartWithHeaders:paramPartWithHeaders
                  withBodyData:body
                       success:^(NSString *ApiName,
                                 id responseObject,
                                 NSString *otherFlag) {
                           
                           [self userRequestEndDealNeedHUD:isNeed
                                                   hunLoad:loadStr
                                                hunSuccess:loadSuccess
                                                   hunFail:loadFail
                                         withRequestStatus:1
                                              withnApiName:APIName
                                      withIDresponseObject:responseObject
                                             withOtherFlag:otherFlag];
                           
                           success(APIName,responseObject,otherFlag);
        
    } faileure:^(NSString *ApiName,
                 id responseObject,
                 NSString *otherFlag) {
        
        
        [self userRequestEndDealNeedHUD:isNeed
                                hunLoad:loadStr
                             hunSuccess:loadSuccess
                                hunFail:loadFail
                      withRequestStatus:2
                           withnApiName:APIName
                   withIDresponseObject:responseObject
                          withOtherFlag:otherFlag];

        
        failure(APIName,responseObject,otherFlag);
        
    } progress:^(NSString *ApiName,
                 id responseObject,
                 NSString *otherFlag) {
        
        [self userRequestEndDealNeedHUD:isNeed
                                hunLoad:loadStr
                             hunSuccess:loadSuccess
                                hunFail:loadFail
                      withRequestStatus:0
                           withnApiName:APIName
                   withIDresponseObject:responseObject
                          withOtherFlag:otherFlag];

        
        progress(APIName,
                 responseObject,
                 otherFlag);
    }];
    

}


/*
 下载文件
 */

- (void)UserDownFileWithPJLAPI:(NSString *)APIName
                    setBaseUrl:(NSString *)baseUrl
                 setServerPort:(NSString *)serverPort
                  withFileName:(NSString *)fileName
                 withOtherFlag:(NSString *)otherFlag
                     isNeedHUD:(BOOL)isNeed
                       hunLoad:(NSString *)loadStr
                    hunSuccess:(NSString *)loadSuccess
                       hunFail:(NSString *)loadFail
                       success:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
                      faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
                      progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress{
    
    
    [self userRequestStartNeedHUD:isNeed
                          hunLoad:loadStr
                       hunSuccess:loadSuccess
                          hunFail:loadFail
                     withnApiName:APIName
                    withOtherFlag:otherFlag];
    
    [self PJLDownFileWithPJLAPI:APIName
                     setBaseUrl:baseUrl
                  setServerPort:serverPort
                   withFileName:fileName
                  withOtherFlag:otherFlag
                        success:^(id responseObject) {
        
        success(APIName,responseObject,otherFlag);
        
    } faileure:^(id responseObject) {
        
        failure(APIName,responseObject,otherFlag);
        
    } progress:^(id responseObject) {
        
        progress(APIName,responseObject,otherFlag);
        
    }];

}

@end
