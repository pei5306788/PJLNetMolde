//
//  PJLNetObject.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/8/25.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLBaseNetObject.h"

#import "AFNetworking.h"

@implementation PJLBaseNetObject

- (id)init{
    
    self = [super init];
    if(self){
        
        self.requestTimer = 30;
        
        self.pjlAcceptableContentTypes = [NSSet setWithObjects:
                                          @"application/json",
                                          @"text/html",
                                          @"text/json",
                                          @"text/javascript",
                                          @"text/plain",
                                          @"application/html",
                                          nil];
    }
    
    return self;
    
}

/*
 正常请求
 */
- (void)PJLRequest:(NSDictionary *)paramDic
            PJLAPI:(NSString *)APIName
        setBaseUrl:(NSString *)baseUrl
     setServerPort:(NSString *)serverPort
      setOtherFlag:(NSString *)otherFlag
            setGet:(BOOL)isGet
         isNeedHUD:(BOOL)isNeed
           hunLoad:(NSString *)loadStr
        hunSuccess:(NSString *)loadSuccess
           hunFail:(NSString *)loadFail
          success:( void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
          faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
          progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress{
    
    
    NSString *urlStr = [self checkUrlPJLAPI:APIName setBaseUrl:baseUrl setServerPort:serverPort];

    
    [self PJLRealRequest:paramDic
                 withUrl:urlStr
             withApiName:APIName
           withOtherFlag:otherFlag
                  setGet:isGet
                 success:^(
                           id responseObject
                           ) {
                     
                     success(APIName,responseObject,otherFlag);

        
    } faileure:^(
                 id responseObject
                 ) {
        
         failure(APIName,responseObject,otherFlag);
        
    } progress:^(
                 id responseObject
                 ) {
        
         progress(APIName,responseObject,otherFlag);
        
    }];
    
    
}

- (NSString *)checkUrlPJLAPI:(NSString *)APIName
                  setBaseUrl:(NSString *)baseUrl
               setServerPort:(NSString *)serverPort{

    NSString *urlStr = @"";
    
    
    if(baseUrl == nil){ //如果这个没传的话就用默认的
        
        urlStr = [PJLAFSectionMangeShareInstance shareInstance].PJLServer;
        
    }else{
        
        urlStr = baseUrl;
    }
    
    if(serverPort == nil ){
        
        if([PJLAFSectionMangeShareInstance shareInstance].PJLServerPort.length != 0){ //传的不是是@""
            
            urlStr = [NSString stringWithFormat:@"%@:%@",urlStr,[PJLAFSectionMangeShareInstance shareInstance].PJLServerPort];
            
        }
        
    }else if(serverPort.length != 0){ ////传的不是是@""
        
        urlStr = [NSString stringWithFormat:@"%@:%@",urlStr,serverPort];
        
    }
    
    if(APIName != nil){
        
        urlStr = [NSString stringWithFormat:@"%@/%@",urlStr,APIName];
        
    }
    
    return urlStr;

}


//把请求再次简化一下
- (void)PJLRealRequest:(NSDictionary *)paramDic
               withUrl:(NSString *)urlStr
           withApiName:(NSString *)APIName
         withOtherFlag:(NSString *)otherFlag
                setGet:(BOOL)isGet
               success:(void (^)(  id responseObject))success
              faileure:(void (^)(  id responseObject))failure
              progress:(void (^)(  id responseObject))progress{

    
    AFHTTPSessionManager *manager = [[PJLAFSectionMangeShareInstance shareInstance] sharedHTTPSession];
    
    manager.responseSerializer.acceptableContentTypes = self.pjlAcceptableContentTypes;
    
    manager.requestSerializer.timeoutInterval = self.requestTimer; //请求超时时间
    
    if([self.setBaseValueForHttpHeadFileDic isKindOfClass:[NSDictionary class]]){
        
        NSArray *getValuesArry = self.setBaseValueForHttpHeadFileDic.allValues;
        
        NSArray *getKeyArry = self.setBaseValueForHttpHeadFileDic.allKeys;
        
        for(NSInteger i = 0; i < getKeyArry.count; i ++){
            
            [manager.requestSerializer setValue:getValuesArry[i] forHTTPHeaderField:getKeyArry[i]];
            
            
        }
    
    }
    
    if(isGet){
        
     
        
        [manager GET:urlStr
          parameters:paramDic
             headers:self.headers
            progress:^(NSProgress * _Nonnull downloadProgress) { //请求进度
                
                progress(downloadProgress);
                
            } success:^(NSURLSessionDataTask * _Nonnull task,
                        id  _Nullable responseObject) { //请求成功
            self.task = task;
                success(responseObject);
                
            } failure:^(NSURLSessionDataTask * _Nullable task,
                        NSError * _Nonnull error) { //请求失败
            self.task = task;
                failure(error.userInfo);
                
            }];
        
        
        
    }else{
        
        [manager POST:urlStr
           parameters:paramDic
              headers:self.headers
             progress:^(NSProgress * _Nonnull uploadProgress) { //请求进度
                 
                 progress(uploadProgress);
                 
             } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) { //请求成功
                 
                 self.task = task;
                 
                 success(responseObject);
                 
                 
             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) { //请求失败
                 
                 self.task = task;
                 failure(error.userInfo);
                 
             }];
    }
    


}



-(void)PJLSimpleRequest:(NSDictionary *)paramDic
                 PJLAPI:(NSString *)APIName
           setOtherFlag:(NSString *)otherFlag
                 setGet:(BOOL)isGet
              isNeedHUD:(BOOL)isNeed
                hunLoad:(NSString *)loadStr
             hunSuccess:(NSString *)loadSuccess
                hunFail:(NSString *)loadFail
                success:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
               faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
               progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress{

    [self PJLRequest:paramDic
              PJLAPI:APIName
          setBaseUrl:nil
       setServerPort:nil
        setOtherFlag:otherFlag
              setGet:isGet
           isNeedHUD:isNeed
             hunLoad:loadStr
          hunSuccess:loadSuccess
             hunFail:loadFail
             success:^(NSString *ApiName
                       , id responseObject,
                       NSString *otherFlag) {
        
                 success(APIName,responseObject,otherFlag);
                 
    } faileure:^(NSString *ApiName,
                 id responseObject,
                 NSString *otherFlag) {
        
        failure(APIName,responseObject,otherFlag);
        
    } progress:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
        
         progress(APIName,responseObject,otherFlag);
        
    }];
    
    

}

/*
 上传图片
 */
- (void)PJLSendImagesRequest:(NSDictionary *)paramDic
    withRequestWithMethod:(NSString *)paramMethod
                   PJLAPI:(NSString *)APIName
               setBaseUrl:(NSString *)baseUrl
            setServerPort:(NSString *)serverPort
             setOtherFlag:(NSString *)otherFlag
             withMimeType:(NSArray *)paramMimeTypeArry
                 withName:(NSArray *)paramNameArry
        withImageDataArry:(NSArray *)imageDataArry
    withImageFilePathArry:(NSArray *)filePathArry
             withFileName:(NSArray *)fileNameArry
withAppendPartWithHeaders:(NSDictionary *)paramPartWithHeaders
             withBodyData:(NSData *)body
                  success:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))success
                 faileure:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))failure
                 progress:(void (^)( NSString *ApiName, id responseObject, NSString *otherFlag))progress{

    NSString *urlStr = [self checkUrlPJLAPI:APIName
                                 setBaseUrl:baseUrl
                              setServerPort:serverPort]; //服务器地址 接口地址
    //数据打包准备发送
 NSMutableURLRequest *request  =  [self pjlSendMessageReadyWithParam:paramDic
                                              withRequestWithMethod:paramMethod
                                                             PJLAPI:APIName
                                                         setBaseUrl:baseUrl
                                                      setServerPort:serverPort
                                                       setOtherFlag:otherFlag
                                                       withMimeType:paramMimeTypeArry
                                                           withName:paramNameArry
                                                  withImageDataArry:imageDataArry
                                              withImageFilePathArry:filePathArry
                                                       withFileName:fileNameArry
                                          withAppendPartWithHeaders:paramPartWithHeaders
                                                       withBodyData:body
                                                          withUrlStr:urlStr];
    
    [self sendFileWithPJLAPI:APIName
                setOtherFlag:otherFlag
                 withRequest:request success:^( id responseObject) {
                     
                     success(APIName,responseObject,otherFlag);
                     
                 } faileure:^( id responseObject) {
                     
                     failure(APIName,responseObject,otherFlag);
                     
                 } progress:^( id responseObject) {
                     
                     progress(APIName,responseObject,otherFlag);
                     
                 }];
   
}


- (NSMutableURLRequest *)pjlSendMessageReadyWithParam:(NSDictionary *)paramDic
                                withRequestWithMethod:(NSString *)paramMethod
                                               PJLAPI:(NSString *)APIName
                                           setBaseUrl:(NSString *)baseUrl
                                        setServerPort:(NSString *)serverPort
                                         setOtherFlag:(NSString *)otherFlag
                                         withMimeType:(NSArray *)paramMimeTypeArry
                                             withName:(NSArray *)paramNameArry
                                    withImageDataArry:(NSArray *)imageDataArry
                                withImageFilePathArry:(NSArray *)filePathArry
                                         withFileName:(NSArray *)fileNameArry
                            withAppendPartWithHeaders:(NSDictionary *)paramPartWithHeaders
                                         withBodyData:(NSData *)body
                                           withUrlStr:(NSString *)urlStr{


    
    if(paramMethod == nil){
        
        paramMethod = @"POST";
        
    }

    
    //打包需要上传的数据
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer]
                                    multipartFormRequestWithMethod:paramMethod
                                    URLString:urlStr
                                    parameters:paramDic
                                    constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                        
                                        //方法一
                                        
                                        for(NSInteger i = 0; i < filePathArry.count; i ++){ //这是文件到文件的方法
                                            
                                            
                                            if(paramMimeTypeArry == nil ){
                                                
                                                [formData appendPartWithFileURL:[NSURL fileURLWithPath:filePathArry[i]]
                                                                           name:paramNameArry[i]
                                                                          error:nil];
                                            }else{
                                                
                                                NSError *error;
                                                
                                                [formData appendPartWithFileURL:[NSURL fileURLWithPath:filePathArry[i]]             name:paramNameArry[i]
                                                                       fileName:fileNameArry[i]
                                                                       mimeType:paramMimeTypeArry[i]
                                                                          error:&error];
                                                
                                                if(error != nil){
                                                    
                                                    NSLog(@"%@",error);
                                                }
                                            }
                                        }
                                        
                                        //方法二
                                        
                                        for(NSInteger i = 0; i < imageDataArry.count; i ++){ //这是nsdata到文件的方法
                                            if(paramMimeTypeArry == nil ){
                                                
                                                [formData appendPartWithFormData:imageDataArry[i] name:paramNameArry[i]];
                                                
                                                
                                            }else{
                                                [formData appendPartWithFileData:imageDataArry[i]
                                                                            name:paramNameArry[i]
                                                                        fileName:fileNameArry[i]
                                                                        mimeType:paramMimeTypeArry[i]];
                                            }
                                        }
                                        
                                        //方法三
                                        
                                        if(paramPartWithHeaders != nil && body != nil){ //另一种方法
                                            [formData appendPartWithHeaders:paramPartWithHeaders body:body];
                                            
                                        }
                                        
                                    } error:nil];
    
        /**
         裴下一版本要添加的请求头
         */
        
        if([self.setBaseValueForHttpHeadFileDic isKindOfClass:[NSDictionary class]]){
            
            
            NSArray *getValuesArry = self.setBaseValueForHttpHeadFileDic.allValues;
            
            NSArray *getKeyArry = self.setBaseValueForHttpHeadFileDic.allKeys;
            
            for(NSInteger i = 0; i < getKeyArry.count; i ++){
                
                [request setValue:getValuesArry[i] forHTTPHeaderField:getKeyArry[i]];
                
                
            }
        
        }

    


    return request;
}

- (void)sendFileWithPJLAPI:(NSString *)APIName
              setOtherFlag:(NSString *)otherFlag
               withRequest:(NSMutableURLRequest *)request
                   success:(void (^)( id responseObject))success
                  faileure:(void (^)( id responseObject))failure
                  progress:(void (^)( id responseObject))progress{

    AFURLSessionManager *managerUrlSection = [[PJLAFSectionMangeShareInstance shareInstance] sharedURLSession];
    
    
    /*
     acceptableContentTypes 主要这个伤心 设置types 我尝试多次，但是感觉不行 这个是类似打补丁的方式加上去的
     */
    AFHTTPSessionManager *managerHttpSection =[[PJLAFSectionMangeShareInstance shareInstance] sharedHTTPSession];
    
    managerHttpSection.responseSerializer.acceptableContentTypes =  self.pjlAcceptableContentTypes;
    
    managerUrlSection.responseSerializer = managerHttpSection.responseSerializer;
    
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [managerUrlSection
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      
                      //这个方法是在后台走的嘿嘿
                      
                       progress(uploadProgress);
                     
                   
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response,
                                      id  _Nullable responseObject,
                                      NSError * _Nullable error) {
                      
                     
                      if (error) {
                          
                           failure(error);
                          
                      } else {
                          
                           success(responseObject);
                          
                      }
                  }];
    
    [uploadTask resume];
    

}

/*
 下载文件
 */

- (void)PJLDownFileWithPJLAPI:(NSString *)APIName
                   setBaseUrl:(NSString *)baseUrl
                setServerPort:(NSString *)serverPort
                 withFileName:(NSString *)fileName
                withOtherFlag:(NSString *)otherFlag
                      success:(void (^)(  id responseObject))success
                     faileure:(void (^)(  id responseObject))failure
                     progress:(void (^)(  id responseObject))progress{

    
    NSString *urlStr = [self checkUrlPJLAPI:APIName setBaseUrl:baseUrl setServerPort:serverPort];
    
    if(fileName != nil){
    
        urlStr = [NSString stringWithFormat:@"%@%@",urlStr,fileName];
    
    }
    
    //获取文件地址
    
    [self downFile:urlStr
           success:^( id responseObject) {
               
               success(responseObject);
        
    } faileure:^(id responseObject) {
        
                failure(responseObject);
        
    } progress:^(id responseObject) {
        
                progress(responseObject);
        
    }]; //下载文件地址
    
    
}


- (void)downFile:(NSString *)urlStr
         success:(void (^)( id responseObject))success
        faileure:(void (^)( id responseObject))failure
        progress:(void (^)( id responseObject))progress{

    
    //默认配置
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    //AFN3.0+基于封住URLSession的句柄
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    //请求
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    
    //下载Task操作
    _pjlDownloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        
        // @property int64_t totalUnitCount;     需要下载文件的总大小
        // @property int64_t completedUnitCount; 当前已经下载的大小
        
        // 给Progress添加监听 KVO
        //        NSLog(@"%f",1.0 * downloadProgress.completedUnitCount / downloadProgress.totalUnitCount);
        //        // 回到主队列刷新UI
        //        dispatch_async(dispatch_get_main_queue(), ^{
        //            // 设置进度条的百分比
        //
        //            self.progressView.progress = 1.0 * downloadProgress.completedUnitCount / downloadProgress.totalUnitCount;
        //        });
        progress(downloadProgress);
        
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        
        //- block的返回值, 要求返回一个URL, 返回的这个URL就是文件的位置的路径
        
        NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
        NSString *path = [cachesPath stringByAppendingPathComponent:response.suggestedFilename];
        return [NSURL fileURLWithPath:path];
        
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        //设置下载完成操作
        // filePath就是你下载文件的位置，你可以解压，也可以直接拿来使用
        
      
        
        if(error != nil){ //下载失败报错了哇
            
            failure(error);
            
        }else{ //下载成功
            
              NSString *imgFilePath = [filePath path];// 将NSURL转成NSString
            
            success(imgFilePath);
        }
        
    }];
    
    [_pjlDownloadTask resume];

}

- (void)PJLStopDownLoad{

    if(_pjlDownloadTask != nil){
    //暂停下载
        [_pjlDownloadTask suspend];
    }

}
-(void)PJLStartDownLoad{

    if(_pjlDownloadTask != nil){
        //暂停下载
        [_pjlDownloadTask resume];
    }


}

@end
