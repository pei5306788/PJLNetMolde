//
//  main.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/8/25.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
