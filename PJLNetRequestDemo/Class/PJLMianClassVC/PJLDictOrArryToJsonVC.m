//
//  PJLDictOrArryToJsonVC.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/1/16.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import "PJLDictOrArryToJsonVC.h"
#import "PJLParamCell.h"
#import "PJLArryCell.h"
#import "PJLParamModel.h"
#import "PJLArryModel.h"
#import "PJLUilityModel.h"
#import "PJLNetConfigModel.h"
@interface PJLDictOrArryToJsonVC (){
    
    NSMutableArray *_dataList;
    
}

@end

@implementation PJLDictOrArryToJsonVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createRightButton];
    
    if([self.paramModel isKindOfClass:[PJLParamModel class]]){
        
        PJLParamModel *getParamModel = self.paramModel;
        
        if(getParamModel.pjlKey == nil || [getParamModel.pjlKey isEqualToString:@"<null>"]){
            
            self.title = @"";
            
        }else{
            self.title = getParamModel.pjlKey;
            
        }
    }
    
    if([self.paramModel isKindOfClass:[PJLArryModel class]]){
        
//        PJLArryModel *getModel = self.paramModel;
        
        self.title = @"数组转json";
    }
    
    if([self.paramModel isKindOfClass:[PJLNetConfigModel class]]){
        
        
        self.title = @"headDic";
        
    }
    
    _dataList = [[NSMutableArray alloc]init];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    [self createCancerButton];
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
    
}

- (void)createCancerButton{
    
    UIButton *cancerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancerButton setTitle:@"取消" forState:UIControlStateNormal];
    [cancerButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    CGSize getSize = [cancerButton.titleLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    
    cancerButton.frame = CGRectMake(0, 0, getSize.width, getSize.height);
    
    UIBarButtonItem *cacnerItem = [[UIBarButtonItem alloc]initWithCustomView:cancerButton];
    
    
    self.navigationItem.leftBarButtonItem = cacnerItem;
    
    [cancerButton addTarget:self
                     action:@selector(cacnerButtonClip)
           forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)cacnerButtonClip{
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}


- (void)createRightButton{
    
    UIButton *finshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [finshButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [finshButton setTitle:@"完成" forState:UIControlStateNormal];
    
    CGSize getSize = [finshButton.titleLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    
    finshButton.frame = CGRectMake(0, 0, getSize.width, getSize.height);
    
    UIBarButtonItem *finshItem = [[UIBarButtonItem alloc]initWithCustomView:finshButton];
    
   
    
    [finshButton addTarget:self
                    action:@selector(finshButtonClip)
          forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *addButtion = [UIButton buttonWithType:UIButtonTypeContactAdd];
    
    addButtion.frame = CGRectMake(0, 0, 44.0, 44.0);
    
    [addButtion addTarget:self
                   action:@selector(addButtonClip)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *addButtonItem = [[UIBarButtonItem alloc]initWithCustomView:addButtion];
    
    self.navigationItem.rightBarButtonItems = @[finshItem,addButtonItem];
    
}

- (void)finshButtonClip{
    
    [self.view endEditing:YES];
    NSString *jsonString = @"";
    
    PJLUilityModel *uilityModel = [[PJLUilityModel alloc]init];
    
    if(self.kind == 0){ //数组转josn
        
        NSMutableArray *jsonArry = [[NSMutableArray alloc]init];
        
        for(PJLArryModel *eachModel in _dataList){
            
            if(eachModel.contentStr != nil
               && eachModel.contentStr.length > 0){
                [jsonArry addObject:eachModel.contentStr];
                
            }
            
        }
        jsonString = [uilityModel convertToJsonData:jsonArry];
    }else if(self.kind == 1){ //字典转json
        
        NSMutableDictionary *modelDic = [[NSMutableDictionary alloc]init];
        
        for(PJLParamModel *eachModel in  _dataList){
            
            if(eachModel.pjlKey != nil
               &&eachModel.pjlValue != nil
               &&eachModel.pjlKey.length!= 0
               &&eachModel.pjlKey.length!= 0){
                [modelDic setValue:eachModel.pjlValue
                            forKey:eachModel.pjlKey];
            }
            
        }
        jsonString = [uilityModel convertToJsonData:modelDic];
    }
    
    if([self.paramModel isKindOfClass:[PJLParamModel class]]){
        
        PJLParamModel *getParamModel = self.paramModel;
        
        getParamModel.pjlValue = jsonString;
    }
    
    if([self.paramModel isKindOfClass:[PJLArryModel class]]){
        
        PJLArryModel *getModel = self.paramModel;
        
        getModel.contentStr = jsonString;
    }
    
    if([self.paramModel isKindOfClass:[PJLNetConfigModel class]]){
        
        PJLNetConfigModel *getConfigModel = self.paramModel;
        getConfigModel.setBaseValueForHttpHeadFileDicJson = jsonString;
        
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)addButtonClip{
    if(self.kind == 0){
        
        PJLArryModel *addArryModel = [[PJLArryModel alloc]init];
        
        [_dataList addObject:addArryModel];
    }else{
        
        PJLParamModel *addModel = [[PJLParamModel alloc]init];
    
        [_dataList addObject:addModel];
        
    }
    
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(self.kind == 0){
        
        static NSString *identifient = @"PJLArryCell";
        
        PJLArryCell *cell = [tableView dequeueReusableCellWithIdentifier:identifient];
        if(cell == nil){
            
            cell = [[PJLArryCell alloc]initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:identifient];
            
            cell.superView = self;
            
        }
        
        [cell setModel:_dataList[indexPath.row]
             wtihIndex:indexPath];
        
        return cell;
        
    }else{
        
        static NSString *identifient = @"PJLParamCell";
        
        PJLParamCell *cell = [tableView dequeueReusableCellWithIdentifier:identifient];
        if(cell == nil){
            
            cell = [[PJLParamCell alloc]initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:identifient];
            cell.superView = self;
            cell.isNotNeedSave = YES;
            //            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
            [cell setModel:_dataList[indexPath.row]];
        
        // Configure the cell...
        
        return cell;
        
    }
    
  
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 44 + 20;
    
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
    
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 添加一个删除按钮
    UITableViewRowAction *deleteRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除"handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
       
        [_dataList removeObjectAtIndex:indexPath.row];
        
        [tableView reloadData];
        
    }];
    
    return @[deleteRowAction];
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
