//
//  PJLParamSetVC.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/27.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLParamSetVC.h"
#import "PJLParamCell.h"
#import "PJLParamModel.h"
#import "PJLFinshRequestSetVC.h"
#import "PJLTestApiWay.h"
@interface PJLParamSetVC ()<UIActionSheetDelegate>{
    
    NSMutableArray *_dataList;
    
    PJLParamModel *_pjlParamModel;
}

@end

@implementation PJLParamSetVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _pjlParamModel = [[PJLParamModel alloc]init];
    self.title = _pjlApiNameModel.apiName;
    
    _dataList = [[NSMutableArray alloc]init];
    [self addProjectButton];
   
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)getData{
    
    [_pjlParamModel getSqlCheckContinue:@[@"superId",@"==",_pjlApiNameModel.id]
                        withSucessBlock:^(NSArray *returnArry) {
        
        [self->_dataList removeAllObjects];
                            
        [self->_dataList addObjectsFromArray:returnArry];
                            
        [self.tableView reloadData];
    }];
    
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    if(_dataList.count > 0){
        
        [self.tableView reloadData];
    
    }else{
    
        [self getData];
    }
    
}

- (void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    for( PJLParamModel *eachModel in _dataList){
        
        [eachModel saveOrUpDataModel:@[@"id",@"=",eachModel.id]];
        
    }
    
}

- (void)addProjectButton{
    
    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
    
    myButton.frame = CGRectMake(0, 0, 44, 44);
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:myButton];
    
   
    
    UIButton *netButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [netButton setTitle:@"下一步" forState:UIControlStateNormal];
    
    CGSize getSize = [netButton.titleLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    
    netButton.frame = CGRectMake(0, 0, getSize.width, getSize.height);
    [netButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *netItem = [[UIBarButtonItem alloc]initWithCustomView:netButton];
    
    netButton.tag = 1;
    self.navigationItem.rightBarButtonItems = @[netItem,rightItem];
    
    
    [myButton addTarget:self
                 action:@selector(addButtonClip:)
       forControlEvents:UIControlEventTouchUpInside];
    
    [netButton addTarget:self
                  action:@selector(addButtonClip:)
        forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)addButtonClip:(UIButton *)paramButton{
 
    [self.view endEditing:YES];
    
    if(paramButton.tag == 0){
        PJLParamModel *model = [[PJLParamModel alloc]init];
        model.superId = _pjlApiNameModel.id;
        [model saveModel];
        [self getData];
        
    }
    
    if(paramButton.tag == 1){
        
        PJLFinshRequestSetVC *pjlFinishRequst = [[PJLFinshRequestSetVC alloc]init];
        pjlFinishRequst.pjlApiNameModel = _pjlApiNameModel;
        [self.navigationController pushViewController:pjlFinishRequst animated:YES];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
    return _dataList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     static NSString *identifient = @"cell";
    
     PJLParamCell *cell = [tableView dequeueReusableCellWithIdentifier:identifient];
        if(cell == nil){
            
            cell = [[PJLParamCell alloc]initWithStyle:UITableViewCellStyleSubtitle
                                         reuseIdentifier:identifient];
            cell.superView = self;
//            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    
    [cell setModel:_dataList[indexPath.row]];
    
    // Configure the cell...
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 44 + 20;
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
    
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 添加一个删除按钮
    UITableViewRowAction *deleteRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除"handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"确定要删除吗" message:@"" preferredStyle:(UIAlertControllerStyleAlert) ];
        
        UIAlertAction *cancerClip = [UIAlertAction actionWithTitle:@"不删除" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
           
        }];
        
        UIAlertAction *makerSureClip = [UIAlertAction actionWithTitle:@"确定删除" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            
            PJLTestApiWay *modelTestApi = [[PJLTestApiWay alloc]init];
            
            [modelTestApi deleteModel:self->_dataList[indexPath.row] withBlock:^(BOOL finsh) {
                
                if(finsh == YES){
                    [self->_dataList removeObjectAtIndex:indexPath.row];
                    [self viewDidAppear:YES];
                }
            }];
        }];
        
        
        [alertController addAction:makerSureClip];
    
        [alertController addAction:cancerClip];
        
        
        [self presentViewController:alertController animated:true completion:^{
            
        }];
      
    }];
    
    return @[deleteRowAction];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
