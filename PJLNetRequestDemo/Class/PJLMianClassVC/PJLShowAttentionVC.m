//
//  PJLShowAttentionVC.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/1/10.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import "PJLShowAttentionVC.h"

#import "PJLTextFieldShowMessageCell.h"

#import "PJLRecordModel.h"

#import "ShowDetailTextView.h"

@interface PJLShowAttentionVC (){
    
    NSMutableArray *_dataList;
    
    NSMutableArray *_contentArryList;
    
}

@end

@implementation PJLShowAttentionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"控制台（请求已经发出，结果在下面）";
    
   
    self.tableView.estimatedRowHeight = 44.0;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
//    self.tableVi
   
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self createCancerButton];
}

- (void)createCancerButton{
    
    UIButton *cancerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancerButton.frame = CGRectMake(0, 0, 60, 44);
    [cancerButton setTitle:@"取消" forState:UIControlStateNormal];
    
    [cancerButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [cancerButton addTarget:self
                     action:@selector(cancerButton)
           forControlEvents:UIControlEventTouchUpInside];
    
    CGSize getSize = [cancerButton.titleLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    cancerButton.frame = CGRectMake(0, 0, getSize.width, 44.0);
    
    UIBarButtonItem *cancerItem = [[UIBarButtonItem alloc]initWithCustomView:cancerButton];
    
    
    self.navigationItem.rightBarButtonItem = cancerItem;
    
}

- (void)cancerButton{
    
    [self dismissViewControllerAnimated:YES completion:^{
    
    }];
    
}

- (void)setDataList:(NSMutableArray *)dataArry{
    
    if(_dataList == nil){
        
         _dataList = [[NSMutableArray alloc]init];
        
    }
    if(_contentArryList == nil){
        
        _contentArryList = [[NSMutableArray alloc]init];
        
    }
    
    [_contentArryList removeAllObjects];
    
    [_dataList removeAllObjects];
    
    for(PJLRecordModel *eachModel in dataArry){
        
       
        NSArray *getSepeArry = [eachModel.resposeString componentsSeparatedByString:@"\n"];
     
        [_contentArryList addObject:getSepeArry];
    
    }
    
    [_dataList addObjectsFromArray:dataArry];
//    _dataList = dataArry;
    
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
    if(_dataList == nil){
        return 0;
        
    }
    
    return _dataList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
    return [_contentArryList[section] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifient = @"customTableViewcell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifient];
    if(cell == nil){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:identifient];
      
        
        //            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    cell.textLabel.numberOfLines = 0;
    
    NSString *getSring = _contentArryList[indexPath.section][indexPath.row];
    
   
    cell.textLabel.text =  [getSring stringByRemovingPercentEncoding];
//    cell.textLabel.text = [getSring stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
 
//    [cell setModel:_dataList[indexPath.section]
//    withResposeStr:_contentArryList[indexPath.section][indexPath.row]];
    
//    _heightArry[indexPath.section][indexPath.row] = []
    return cell;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//
//    return 60.0;
//
//
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    PJLRecordModel *getModel = _dataList[section];
    
    if(getModel.headCellHeight == nil){
        
        return 1;
    }
    
    return [getModel.headCellHeight integerValue];
    
   
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    ShowDetailTextView *showDetailTextView = [[ShowDetailTextView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0)];
    showDetailTextView.pjlSuperView = tableView;
    [showDetailTextView setModel:_dataList[section]];
    
    return showDetailTextView;
    
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
