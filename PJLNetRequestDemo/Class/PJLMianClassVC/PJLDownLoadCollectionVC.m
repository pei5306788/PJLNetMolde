//
//  PJLDownLoadCollectionVC.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2022/6/21.
//  Copyright © 2022 PJL. All rights reserved.
//

#import "PJLDownLoadCollectionVC.h"
#import "PJLEditCollectionViewCell.h"
#import "PJLDownLoadSqlModel.h"
#import "PJLAPI.h"
#import "PJLTestApiWay.h"
@interface PJLDownLoadCollectionVC (){
    
    PJLDownLoadSqlModel *_pjlDownLoadSqlModel;
    
    NSMutableArray *_dataList;
    
}

@end

@implementation PJLDownLoadCollectionVC

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _pjlDownLoadSqlModel = [[PJLDownLoadSqlModel alloc]init];
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    
    [self.collectionView registerClass:[PJLEditCollectionViewCell class] forCellWithReuseIdentifier:@"PJLEditCollectionViewCell"];
    
    // Do any additional setup after loading the view.
    
    [self addProjectButton];
    
    _dataList = [[NSMutableArray alloc]init];
//    [self.collectionView setBackgroundColor:[UIColor systemBlueColor]];
    
    [self upDataRequest];
   
}

- (void)upDataRequest{
    
    [_pjlDownLoadSqlModel getSqlCheckContinue:nil
                              withSucessBlock:^(NSArray *returnArry) {
        
        
        [self->_dataList removeAllObjects];
        
        if([returnArry isKindOfClass:[NSArray class]]){
            
            [self->_dataList addObjectsFromArray:returnArry];
        }
        [self.collectionView reloadData];
    }];
    
}

- (void)addProjectButton{
    
    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
    
    myButton.frame = CGRectMake(0, 0, 44, 44);
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:myButton];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    [myButton addTarget:self
                 action:@selector(addButtonClip)
       forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)addButtonClip{
    

    [self editAddPJLDownLoadSqlModel:nil];
    
    
}

- (void)editAddPJLDownLoadSqlModel:(PJLDownLoadSqlModel *)pjlDownLoadSqlModel{
    
    
    /**
     pjlRequestModel.baseUrl = @"http://www.baidu.com";
     pjlRequestModel.apiName = @"img";
     pjlRequestModel.fileName = @"/bdlogo.png";
     pjlRequestModel.serverPort = @""; //不需要端口 就这样写 ，不然就会读取默认的
     */
    
    UIAlertController *pjlAlertController = [UIAlertController alertControllerWithTitle:@"添加/编辑图片地址"
                         message:@"编辑服务器地址" preferredStyle:(UIAlertControllerStyleAlert)];
    
    [pjlAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        textField.placeholder = @"输入服务器地址：例如：http(s)://www.baidu.com";
        if(pjlDownLoadSqlModel != nil){
            
            textField.text = pjlDownLoadSqlModel.baseUrl;
        }
        
            
    }];
    
    [pjlAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        textField.placeholder = @"端口号 例如：80（可以不填写）";
        
        if(pjlDownLoadSqlModel != nil){
            
            textField.text = pjlDownLoadSqlModel.serverPort;
        }
            
    }];
  
    
    [pjlAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
//        img
        textField.placeholder = @"接口名字 例如：img";
        
        if(pjlDownLoadSqlModel != nil){
            
            textField.text = pjlDownLoadSqlModel.apiName;
        }
            
    }];
    
    
    
    [pjlAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        textField.placeholder = @"图片名字 例如：/bdlogo.png";
        
        if(pjlDownLoadSqlModel != nil){
            
            textField.text = pjlDownLoadSqlModel.fileName;
        }
            
    }];
    
    UIAlertAction *alertMakeSure = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
        PJLDownLoadSqlModel *getPJLDownLoadSqlModel = pjlDownLoadSqlModel;
        
        if(getPJLDownLoadSqlModel == nil){
            
            getPJLDownLoadSqlModel = [[PJLDownLoadSqlModel alloc]init];
            
        }
        getPJLDownLoadSqlModel.baseUrl = pjlAlertController.textFields[0].text;
        getPJLDownLoadSqlModel.serverPort = pjlAlertController.textFields[1].text;
        
        getPJLDownLoadSqlModel.apiName =  pjlAlertController.textFields[2].text;
        getPJLDownLoadSqlModel.fileName =  pjlAlertController.textFields[3].text;
        
       

        if(getPJLDownLoadSqlModel.id == nil){
            [getPJLDownLoadSqlModel saveModel];
         
            
        }else{
            [getPJLDownLoadSqlModel saveOrUpDataModel:@[@"id",@"=",getPJLDownLoadSqlModel.id]];
            
        }
        [self upDataRequest];
        
    }];
    
    
    UIAlertAction *alertCancer = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        

    }];
    
    [pjlAlertController addAction:alertMakeSure];
    
    [pjlAlertController addAction:alertCancer];
    
    [self presentViewController:pjlAlertController animated:false completion:^{
        
    }];
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
#warning Incomplete implementation, return the number of sections
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of items
    return _dataList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PJLEditCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PJLEditCollectionViewCell" forIndexPath:indexPath];
    
    [cell setModel:_dataList[indexPath.row] withKind:0];
    
    // Configure the cell
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    [self makeyouChooice:_dataList[indexPath.row]];
    
    
//    [self addButtonClip];
}

- (void)makeyouChooice:(PJLDownLoadSqlModel *)pjlDownLoadSqlModel{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"请选择您的操作" message:@"" preferredStyle:(isPad?UIAlertControllerStyleAlert:UIAlertControllerStyleActionSheet)];
    
    
    UIAlertAction *cancerAction = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        
        
    }];
    
    
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"删除" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
              PJLTestApiWay *modelTestApi = [[PJLTestApiWay alloc]init];
              
        [modelTestApi deleteModel:pjlDownLoadSqlModel withBlock:^(BOOL finsh) {
                  
            [self upDataRequest];
        }];
        
    }];
    
    UIAlertAction *editAction = [UIAlertAction actionWithTitle:@"编辑" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
        [self editAddPJLDownLoadSqlModel:pjlDownLoadSqlModel];
        
    }];
    
    
    UIAlertAction *runAction = [UIAlertAction actionWithTitle:@"下载" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
        pjlDownLoadSqlModel.status = @"1";
        
        [self.collectionView reloadData];
       
    }];
  
    
//    [alertController addAction:nextClip];
    [alertController addAction:editAction];
    [alertController addAction:deleteAction];
    [alertController addAction:cancerAction];
    [alertController addAction:runAction];
    
    if(isPad){
        
      
   
        [self presentViewController:alertController animated:true completion:^{
            
        }];
        
    }else{
        
        [self presentViewController:alertController animated:true completion:^{
            
        }];
        
        
    }
    
    
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
