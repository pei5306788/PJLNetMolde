//
//  PJLWKWebVC.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2022/6/22.
//  Copyright © 2022 PJL. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PJLWKWebVC : UIViewController

@property (nonatomic) NSString *urlString;

@end

NS_ASSUME_NONNULL_END
