//
//  PJLAPIListTableVC.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/26.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLAPIListTableVC.h"
#import "PJLApiNameModel.h"
#import "PJLParamSetVC.h"
#import "PJLTestApiWay.h"
#import "PJLAPI.h"
@interface PJLAPIListTableVC ()<UIAlertViewDelegate>{
    
    NSMutableArray *_dataList;
    
    PJLApiNameModel *_pjlApiNameModel;
}

@end

@implementation PJLAPIListTableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dataList = [[NSMutableArray alloc]init];
    
    
    self.title = [NSString stringWithFormat:@"接口名字：%@",_deliverModel.projectName];
    
    _pjlApiNameModel = [[PJLApiNameModel alloc]init];
   
    [self addProjectButton];
    
    self.tableView.estimatedRowHeight = 60;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)getData{
    
    [_pjlApiNameModel getSqlCheckContinue:@[@"superID",@"=",_deliverModel.id]
                          withSucessBlock:^(NSArray *returnArry) {
        
        [self->_dataList removeAllObjects];
                              
        [self->_dataList addObjectsFromArray:returnArry];
                              
        [self.tableView reloadData];
    }];
    
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
     [self getData];
    
}

- (void)addProjectButton{
    
    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
    
    myButton.frame = CGRectMake(0, 0, 44, 44);
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:myButton];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    [myButton addTarget:self
                 action:@selector(addButtonClip)
       forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)addButtonClip{
    
    
    UIAlertController *pjlAlertController = [UIAlertController alertControllerWithTitle:@"添加接口名"
                                                                                message:[NSString stringWithFormat:@"%@/(这里需要填写的Api)?xx=xx",_deliverModel.serverAdress] preferredStyle:(UIAlertControllerStyleAlert)];
    
    [pjlAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        textField.placeholder = @"请输入接口名(api)";
            
    }];
    
    [pjlAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        textField.placeholder = @"备注(方便记录)";
            
    }];
    
    UIAlertAction *alertMakeSure = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
        
        PJLApiNameModel *saveModel = [[PJLApiNameModel alloc]init];
        saveModel.apiName = pjlAlertController.textFields[0].text;
        saveModel.reMark = pjlAlertController.textFields[1].text;
        saveModel.superID = self->_deliverModel.id;
        [saveModel saveModel];
        
    
    
        [self getData];
        
        
    }];
    
    
    UIAlertAction *alertCancer = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        
        
    }];
    
    [pjlAlertController addAction:alertMakeSure];
    
    [pjlAlertController addAction:alertCancer];
    
    
    [self presentViewController:pjlAlertController animated:true completion:^{
        
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
    return _dataList.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    
    return _deliverModel.serverAdress;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifient = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifient];
    if(cell == nil){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifient];
   
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    }
    
    cell.textLabel.numberOfLines = 0;
    
    cell.detailTextLabel.numberOfLines = 0;
    
    PJLApiNameModel *getModel = _dataList[indexPath.row];
    [cell.detailTextLabel setNumberOfLines:2];
  
    cell.detailTextLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    
    if(getModel.apiName.length == 0 || getModel.apiName == nil){
        
         cell.detailTextLabel.text = [NSString stringWithFormat:@"%@(%@)",_deliverModel.serverAdress,getModel.recordMessage];
         cell.textLabel.text = [NSString stringWithFormat:@"%@",getModel.reMark];
        if(getModel.reMark == nil ||getModel.reMark.length == 0){
            
             cell.textLabel.text = [NSString stringWithFormat:@"%@",@"没备注，没接口名"];
        }
        
    }else{
        
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@/%@(%@)",_deliverModel.serverAdress,getModel.apiName,getModel.recordMessage];
        cell.textLabel.text = [NSString stringWithFormat:@"%@:%@",
                               getModel.reMark,getModel.apiName];
    }
    
    cell.textLabel.textColor = [UIColor whiteColor];
    
    [cell setBackgroundColor:[UIColor systemTealColor]];
    
    cell.detailTextLabel.textColor  = [UIColor whiteColor];

    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"请选择您的操作" message:@"" preferredStyle:((isPad?UIAlertControllerStyleAlert:UIAlertControllerStyleActionSheet))];
    
    
    UIAlertAction *cancerAction = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        
        
    }];
    
    
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"删除" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
        PJLTestApiWay *modelTestApi = [[PJLTestApiWay alloc]init];
        
        [modelTestApi deleteModel:self->_dataList[indexPath.row] withBlock:^(BOOL finsh) {
            
            if(finsh == YES){
                
                [self viewDidAppear:YES];
            }
        }];
        
    }];
    
    UIAlertAction *editAction = [UIAlertAction actionWithTitle:@"编辑" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
        [self editClip:indexPath.row];
        
    }];
    
    
    UIAlertAction *runAction = [UIAlertAction actionWithTitle:@"运行" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
      
        PJLTestApiWay *modelTestApi = [[PJLTestApiWay alloc]init];
        modelTestApi.superVC = self;
        [modelTestApi setTestApiModel:self->_dataList[indexPath.row] withPJLRequestModel:nil];
    }];
    
    UIAlertAction *nextClip = [UIAlertAction actionWithTitle:@"下一步" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
        PJLParamSetVC *pjlParamSetVC = [[PJLParamSetVC alloc]init];
        
        pjlParamSetVC.pjlApiNameModel = self->_dataList[indexPath.row];
        
        [self.navigationController pushViewController:pjlParamSetVC animated:YES];
        
    }];
    
    [alertController addAction:nextClip];
    [alertController addAction:editAction];
    [alertController addAction:deleteAction];
    [alertController addAction:cancerAction];
    [alertController addAction:runAction];
    
    [self presentViewController:alertController animated:true completion:^{
        
    }];
    
   
    
    
}

//- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    return YES;
//
//}
//
//- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // 添加一个删除按钮
//    UITableViewRowAction *deleteRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除"handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
//
//        PJLTestApiWay *modelTestApi = [[PJLTestApiWay alloc]init];
//
//        [modelTestApi deleteModel:self->_dataList[indexPath.row] withBlock:^(BOOL finsh) {
//
//            if(finsh == YES){
//
//                [self viewDidAppear:YES];
//            }
//        }];
//    }];
//
//    // 删除一个置顶按钮
//    UITableViewRowAction *topRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"开始测试"handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
//        NSLog(@"点击了开始测试");
//
//
//
//
//        //        // 1. 更新数据
//        ////        [self.dataSource exchangeObjectAtIndex:indexPath.row withObjectAtIndex:0];
//        //
//        //        // 2. 更新UI
//        //        NSIndexPath *firstIndexPath = [NSIndexPath indexPathForRow:0 inSection:indexPath.section];
//        //        [tableView moveRowAtIndexPath:indexPath toIndexPath:firstIndexPath];
//    }];
//    topRowAction.backgroundColor = [UIColor blueColor];
//
//    // 添加一个更多按钮
//    UITableViewRowAction *moreRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"修改"handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
//
//        PJLApiNameModel *getModel = self->_dataList[indexPath.row];
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"修改接口名"
//                                                       message:[NSString stringWithFormat:@"%@/(这里需要填写的Api)?xx=xx",self->_deliverModel.serverAdress]
//                                                      delegate:self
//                                             cancelButtonTitle:@"取消"
//                                             otherButtonTitles:@"确定",
//                              nil];
//
//        alert.tag = indexPath.row;
//
//        [alert setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
//        [alert textFieldAtIndex:0].placeholder = @"请输入接口名(api)";
//        [alert textFieldAtIndex:1].placeholder = @"备注(这个接口是获取图片)";
//        //    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeURL];
//        [[alert textFieldAtIndex:1] setSecureTextEntry:NO];
//
//        [alert textFieldAtIndex:0].text = getModel.apiName;
//        [alert textFieldAtIndex:1].text = getModel.reMark;
//
//        [alert show];
//
//    }];
//    moreRowAction.backgroundEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
//
//    // 将设置好的按钮放到数组中返回
//    return @[deleteRowAction, topRowAction, moreRowAction];
//
//}


- (void)editClip:(NSInteger)row{
    
    PJLApiNameModel *getModel = self->_dataList[row];
    
    
    
    UIAlertController *pjlAlertController = [UIAlertController alertControllerWithTitle:@"修改接口名"
                                                                                message:[NSString stringWithFormat:@"%@/(这里需要填写的Api)?xx=xx",self->_deliverModel.serverAdress] preferredStyle:(UIAlertControllerStyleAlert)];
    
    [pjlAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        textField.placeholder = @"请输入接口名(api)";
        
        textField.text = getModel.apiName;
            
    }];
    
    [pjlAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        textField.placeholder = @"备注(例如：这个接口是获取图片)";
        
        textField.text = getModel.reMark;
            
    }];
    
    UIAlertAction *alertMakeSure = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
        PJLApiNameModel *getModel = self->_dataList[row];
        
       
        
        getModel.apiName  =  pjlAlertController.textFields[0].text;
        getModel.reMark  =   pjlAlertController.textFields[1].text;
        [getModel saveOrUpDataModel:@[@"id",@"=",getModel.id]];
        [self viewDidAppear:YES];
        
    }];
    
    
    UIAlertAction *alertCancer = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        
        
    }];
    
    [pjlAlertController addAction:alertMakeSure];
    
    [pjlAlertController addAction:alertCancer];
    
    
    [self presentViewController:pjlAlertController animated:true completion:^{
        
    }];
    
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark aertViewDeletgate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex NS_DEPRECATED_IOS(2_0, 9_0){
    
    if(alertView.cancelButtonIndex == buttonIndex){ //取消按钮
        
        return;
    }
    
    if([alertView.title isEqualToString:@"修改接口名"]){
        
        PJLApiNameModel *getModel = _dataList[alertView.tag];
        getModel.apiName = [alertView textFieldAtIndex:0].text;
        getModel.reMark = [alertView textFieldAtIndex:1].text;
        [getModel saveOrUpDataModel:@[@"id",@"=",getModel.id]];
        
    }else{
    
        PJLApiNameModel *saveModel = [[PJLApiNameModel alloc]init];
        saveModel.apiName = [alertView textFieldAtIndex:0].text;
        saveModel.reMark = [alertView textFieldAtIndex:1].text;
        saveModel.superID = _deliverModel.id;
        [saveModel saveModel];
        
    }
    
    [self getData];
    
}

@end
