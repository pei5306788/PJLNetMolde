//
//  PJLDictOrArryToJsonVC.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/1/16.
//  Copyright © 2018年 PJL. All rights reserved.
//
#import "PJLBaseTableVC_1.h"
#import "PJLParamModel.h"
@interface PJLDictOrArryToJsonVC : PJLBaseTableVC_1

@property (nonatomic) NSInteger kind; // 0 是 数组转josn  1 是字典转json

@property (nonatomic) id  getid;  //返回的参数

@property (nonatomic) id  paramModel;

@end
