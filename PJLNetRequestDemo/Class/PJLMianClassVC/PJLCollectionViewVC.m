//
//  PJLCollectionViewVC.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2022/6/21.
//  Copyright © 2022 PJL. All rights reserved.
//

#import "PJLCollectionViewVC.h"
#import "PJLCollectionViewCell.h"
#import "PJLMainChooseVC.h"
#import "PJLDownLoadCollectionVC.h"
#import "PJLWKWebVC.h"
@interface PJLCollectionViewVC (){
    
    NSMutableArray *_dataList;
}

@end

@implementation PJLCollectionViewVC

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"网络功能列表";
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    [self.collectionView registerClass:[PJLCollectionViewCell class] forCellWithReuseIdentifier:@"PJLCollectionViewCell"];
    
    // Do any additional setup after loading the view.
    
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    
    _dataList = [[NSMutableArray alloc]init];
    
    [_dataList addObjectsFromArray:@[@"项目：接口管理",
//                                     @"我的项目图片上传管理",
                                     @"项目：图片下载管理",
    @"项目：代码地址"]];
    
//    [self.collectionView setBackgroundColor:[UIColor systemBlueColor]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
#warning Incomplete implementation, return the number of sections
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of items
    return _dataList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PJLCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PJLCollectionViewCell" forIndexPath:indexPath];
    
    [cell setModel:_dataList[indexPath.row] withKind:0];
    // Configure the cell
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(indexPath.row == 0){
        PJLMainChooseVC *mainVC = [[PJLMainChooseVC alloc]init];
        
        [self.navigationController pushViewController:mainVC animated:true];
        
    }
    
    if(indexPath.row == 1){
        //图片下载
        UICollectionViewFlowLayout *flowLayOut = [[UICollectionViewFlowLayout alloc]init];
        flowLayOut.itemSize = CGSizeMake(200, self.view.frame.size.height - 64);
        PJLDownLoadCollectionVC *pjlDownLoadCollectionVC = [[PJLDownLoadCollectionVC alloc]initWithCollectionViewLayout:flowLayOut];
        
        pjlDownLoadCollectionVC.title = _dataList[indexPath.row];
        
        
        [self.navigationController pushViewController:pjlDownLoadCollectionVC animated:true];
    }
    
    if(indexPath.row == 2){
        
        PJLWKWebVC *pjlWKWebVC = [[PJLWKWebVC alloc]init];
        
        pjlWKWebVC.urlString = @"https://gitee.com/pei5306788/PJLNetMolde.git";
        pjlWKWebVC.title = _dataList[indexPath.row];
        [self.navigationController pushViewController:pjlWKWebVC animated:true];
        
        
        
    }
    
//https://gitee.com/pei5306788/PJLNetMolde.git
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
