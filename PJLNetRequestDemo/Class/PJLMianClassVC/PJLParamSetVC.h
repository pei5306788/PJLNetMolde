//
//  PJLParamSetVC.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/27.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLBaseTableVC_1.h"
#import "PJLApiNameModel.h"
@interface PJLParamSetVC : PJLBaseTableVC_1

@property (nonatomic) PJLApiNameModel *pjlApiNameModel;

@end
