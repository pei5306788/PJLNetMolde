//
//  PJLWKWebVC.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2022/6/22.
//  Copyright © 2022 PJL. All rights reserved.
//

#import "PJLWKWebVC.h"
#import <WebKit/WebKit.h>
@interface PJLWKWebVC (){
    
    WKWebView *_wkWebView;
    
}

@end

@implementation PJLWKWebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _wkWebView = [[WKWebView alloc]init];
    
    [self.view addSubview:_wkWebView];
    _wkWebView.translatesAutoresizingMaskIntoConstraints = false;
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:_wkWebView attribute:(NSLayoutAttributeLeft) relatedBy:(NSLayoutRelationEqual) toItem:self.view attribute:(NSLayoutAttributeLeft) multiplier:1.0 constant:0];
    
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:_wkWebView attribute:(NSLayoutAttributeTop) relatedBy:(NSLayoutRelationEqual) toItem:self.view attribute:(NSLayoutAttributeTop) multiplier:1.0 constant:0];
    
    
    NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem:_wkWebView attribute:(NSLayoutAttributeRight) relatedBy:(NSLayoutRelationEqual) toItem:self.view attribute:(NSLayoutAttributeRight) multiplier:1.0 constant:0];
    
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:_wkWebView attribute:(NSLayoutAttributeBottom) relatedBy:(NSLayoutRelationEqual) toItem:self.view attribute:(NSLayoutAttributeBottom) multiplier:1.0 constant:0];
    
    
    [left setActive:true];
    
    [top setActive:true];
    
    [right setActive:true];
    
    [bottom setActive:true];
    
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:self.urlString]];
    
    
    [_wkWebView loadRequest:request];
    
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
