//
//  PJLFinshRequestSetVC.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/28.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLFinshRequestSetVC.h"
#import "PJLNetConfigCell.h"
#import "PJLNetConfigModel.h"

@interface PJLFinshRequestSetVC (){
    
    PJLNetConfigModel *_pjlNetConfigModel;
    
    NSMutableArray *_dataList;
    
    
    PJLNetConfigModel *_recordConfigModel;
    
}

@end

@implementation PJLFinshRequestSetVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"最后配置";
    
    _dataList = [[NSMutableArray alloc]init];
    
    
    [_dataList addObject:@"设置请求时间:"];
    
    [_dataList addObject:@"请求类型:"];
    
    [_dataList addObject:@"请求头文件:"];
    
    _pjlNetConfigModel = [[PJLNetConfigModel alloc]init];
    
      [self getDataList];
    
     [self createRightButton];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    [_recordConfigModel saveOrUpDataModel:@[@"id",@"=",_recordConfigModel.id]];
    
    
}

- (void)createRightButton{
    
    UIButton *finishButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [finishButton setTitle:@"完成" forState:UIControlStateNormal];
    [finishButton setTitleColor:[UIColor blackColor]
                       forState:UIControlStateNormal];
    CGSize getSize = [finishButton.titleLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    finishButton.frame = CGRectMake(0,
                                    0,
                                    getSize.width,
                                    44.0);
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:finishButton];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    [finishButton addTarget:self
                     action:@selector(finshClip)
           forControlEvents:UIControlEventTouchUpInside];
}

- (void)getDataList{
    
   
    
    [_pjlNetConfigModel getSqlCheckContinue:@[@"superId",
                                              @"=",
                                              _pjlApiNameModel.id]
                            withSucessBlock:^(NSArray *returnArry) {
                                
                                if(returnArry.count > 0){
                                    
                                    _recordConfigModel = returnArry.firstObject;
                                    
                                    _recordConfigModel.userModel = self;
                                    
                                      [self.tableView reloadData];
                                }else{
                                    
                                    _recordConfigModel = [[PJLNetConfigModel alloc]init];
                                    _recordConfigModel.superId = _pjlApiNameModel.id;
                                    [_recordConfigModel saveModel];
                                     
                                     [self getDataList];
                                }
                                
                              
                                
                            }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
    if(_recordConfigModel == nil){
        
        return 0;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
    return _dataList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifient = @"cell";
    
    PJLNetConfigCell *cell = [tableView dequeueReusableCellWithIdentifier:identifient];
    if(cell == nil){
        
        cell = [[PJLNetConfigCell alloc]initWithStyle:UITableViewCellStyleSubtitle
                                  reuseIdentifier:identifient];
        cell.pjlSuperView = self;
        
        //            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    [cell setModel:_recordConfigModel
     withLeftTitle:_dataList[indexPath.row]
         withIndex:indexPath.row];
    
    return cell;  
}

- (void)finshClip{
    
    [self.navigationController popToViewController:self.navigationController.viewControllers[1] animated:YES];

}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
