//
//  PJLAPIListTableVC.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/26.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLBaseTableVC.h"
#import "PJLMianVCSqlModel.h"

@interface PJLAPIListTableVC : PJLBaseTableVC

@property(nonatomic) PJLMianVCSqlModel *deliverModel; //传递app

@end
