//
//  PJLMainChooseVC.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/25.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLMainChooseVC.h"
#import "PJLMianVCSqlModel.h"
#import "PJLAPIListTableVC.h"
#import "PJLTestApiWay.h"
#import "AppDelegate.h"
#import "PJLAPI.h"
@interface PJLMainChooseVC ()<UIAlertViewDelegate>{
    
     PJLMianVCSqlModel *_pjlMainVCSqlModel;
    
     NSMutableArray *_dataList;
}

@end

@implementation PJLMainChooseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的项目接口管理";
    
      _pjlMainVCSqlModel = [[PJLMianVCSqlModel alloc]init];
    
    self.tableView.estimatedRowHeight = 44.0;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self addProjectButton];
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [self upDataRequest];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    [appDelegate.localModel getSqlCheckContinue:nil withSucessBlock:^(NSArray *returnArry) {

        if(returnArry.count == 0){

            [appDelegate.localModel createAttio:self];
        }else{

            PJLVersionModel *getModel = returnArry.firstObject;

            [getModel createAttio:self];

        }

    }];
    
}

- (void)upDataRequest{
   
    [_pjlMainVCSqlModel getSqlCheckContinue:nil
                            withSucessBlock:^(NSArray *returnArry) {
                            
        [self->_dataList removeAllObjects];
                            
        if(self->_dataList == nil){
                                    
            self->_dataList = [[NSMutableArray alloc]initWithArray:returnArry];
                                    
                                }else{
                                    
                                    [self->_dataList addObjectsFromArray:returnArry];
                                }
                                
                                [self.tableView reloadData];
                                
        }];
    
}

- (void)addProjectButton{
    
    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
    
    myButton.frame = CGRectMake(0, 0, 44, 44);
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:myButton];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    [myButton addTarget:self
                 action:@selector(addButtonClip)
       forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)addButtonClip{
    
    UIAlertController *pjlAlertController = [UIAlertController alertControllerWithTitle:@"添加项目"
                                                                                message:@"请输入项目名称和服务器地址" preferredStyle:(UIAlertControllerStyleAlert)];
    
    [pjlAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        textField.placeholder = @"输入项目名称";
            
    }];
    
    [pjlAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        textField.placeholder = @"https://127.0.0.1(:8080)";
            
    }];
    
    UIAlertAction *alertMakeSure = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
        
        PJLMianVCSqlModel *getSaveModel = [[PJLMianVCSqlModel alloc]init];
    
        getSaveModel.projectName = pjlAlertController.textFields[0].text;
    
        getSaveModel.serverAdress = pjlAlertController.textFields[1].text;
    
        [getSaveModel saveModel];
        
        [self upDataRequest];
        
    }];
    
    
    UIAlertAction *alertCancer = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        

    }];
    
    [pjlAlertController addAction:alertMakeSure];
    
    [pjlAlertController addAction:alertCancer];
    
    
    [self presentViewController:pjlAlertController animated:true completion:^{
        
    }];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
    return _dataList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
    static NSString *identifient = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifient];
    if(cell == nil){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifient];
    
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        [cell setSelectionStyle:(UITableViewCellSelectionStyleNone)];
    }
    
    PJLMianVCSqlModel *getModel = _dataList[indexPath.row];
    
    cell.textLabel.numberOfLines = 0;
    cell.detailTextLabel.numberOfLines = 0;
    cell.textLabel.text = [NSString stringWithFormat:@"项目名称:%@",getModel.projectName];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"项目服务器地址:%@",getModel.serverAdress];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    [cell setBackgroundColor:[UIColor systemTealColor]];
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"请选择您的操作" message:@"" preferredStyle:(isPad?UIAlertControllerStyleAlert:UIAlertControllerStyleActionSheet)];
    
    
    UIAlertAction *cancerAction = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        
        
    }];
    
    
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"删除" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
              PJLTestApiWay *modelTestApi = [[PJLTestApiWay alloc]init];
              
        [modelTestApi deleteModel:self->_dataList[indexPath.row] withBlock:^(BOOL finsh) {
                  
                  if(finsh == YES){
                      
                      [self viewDidAppear:YES];
                  }
              }];
        
    }];
    
    UIAlertAction *editAction = [UIAlertAction actionWithTitle:@"编辑" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
        [self editClip:indexPath.row];
        
    }];
    
    
    UIAlertAction *runAction = [UIAlertAction actionWithTitle:@"运行" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
      
        PJLTestApiWay *modelTestApi = [[PJLTestApiWay alloc]init];
        modelTestApi.superVC = self;
        [modelTestApi setTestApiModel:self->_dataList[indexPath.row] withPJLRequestModel:nil];
    }];
    
    UIAlertAction *nextClip = [UIAlertAction actionWithTitle:@"下一步" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
        PJLAPIListTableVC *apiListTableVC = [[PJLAPIListTableVC alloc]init];
        apiListTableVC.deliverModel = self->_dataList[indexPath.row];
        [self.navigationController pushViewController:apiListTableVC animated:YES];
        
    }];
    
    [alertController addAction:nextClip];
    [alertController addAction:editAction];
    [alertController addAction:deleteAction];
    [alertController addAction:cancerAction];
    [alertController addAction:runAction];
    
    if(isPad){
        
      
   
        [self presentViewController:alertController animated:true completion:^{
            
        }];
        
    }else{
        
        [self presentViewController:alertController animated:true completion:^{
            
        }];
        
        
    }
    
    
    
    
}

- (void)editClip:(NSInteger)row{
    
    PJLMianVCSqlModel *getModel = _dataList[row];
    
    
    
    UIAlertController *pjlAlertController = [UIAlertController alertControllerWithTitle:@"修改项目"
                                                                                message:@"请输入项目名称和服务器地址" preferredStyle:(UIAlertControllerStyleAlert)];
    
    [pjlAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        textField.placeholder = @"输入项目名称";
        
        textField.text = getModel.projectName;
            
    }];
    
    [pjlAlertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        textField.placeholder = @"https://127.0.0.1(:8080)";
        
        textField.text = getModel.serverAdress;
            
    }];
    
    UIAlertAction *alertMakeSure = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
        
        getModel.projectName = pjlAlertController.textFields[0].text;
    
        getModel.serverAdress = pjlAlertController.textFields[1].text;
    
        [getModel saveOrUpDataModel:@[@"id",@"=",getModel.id]];
        
        [self upDataRequest];
        
        
    }];
    
    
    UIAlertAction *alertCancer = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        
        
    }];
    
    [pjlAlertController addAction:alertMakeSure];
    
    [pjlAlertController addAction:alertCancer];
    
    if(isPad){
        
//        [self p]
        
        [self presentViewController:pjlAlertController animated:true completion:^{
            
        }];
        
    }else{
        [self presentViewController:pjlAlertController animated:true completion:^{
            
        }];
        
    }
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark alertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex NS_DEPRECATED_IOS(2_0, 9_0){
    
    if(alertView.cancelButtonIndex == buttonIndex){ //取消按钮
        
        return;
    }
    
    if([alertView.title isEqualToString:@"修改项目"]){
        
        PJLMianVCSqlModel *getModel = _dataList[alertView.tag];
        
        getModel.projectName = [alertView textFieldAtIndex:0].text;
        
        getModel.serverAdress = [alertView textFieldAtIndex:1].text;
        
        [getModel saveOrUpDataModel:@[@"id",@"=",getModel.id]];
        
    }else{
        PJLMianVCSqlModel *getSaveModel = [[PJLMianVCSqlModel alloc]init];
    
        getSaveModel.projectName = [alertView textFieldAtIndex:0].text;
    
        getSaveModel.serverAdress = [alertView textFieldAtIndex:1].text;
    
        [getSaveModel saveModel];
        
    }
    
    [self upDataRequest];
  
}


@end
