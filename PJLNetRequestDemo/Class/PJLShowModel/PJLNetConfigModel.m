//
//  PJLNetConfigModel.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/29.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLNetConfigModel.h"

@implementation PJLNetConfigModel

- (id)init{
    
    self = [super init];
    if(self){
        
        self.userModel = self;
        self.netTimerCount = @"30.0";
        
        
        self.netRequestKind = @"Get";
        
        self.setBaseValueForHttpHeadFileDicJson = @"";
        
        
    }
    return self;
    
}


@end
