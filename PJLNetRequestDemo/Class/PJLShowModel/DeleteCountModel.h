//
//  DeleteCountModel.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/1/15.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeleteCountModel : NSObject

@property (nonatomic) NSInteger deleteCount;

@end
