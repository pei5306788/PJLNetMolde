//
//  PJLMianVCSqlModel.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/26.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <PJLSQLModel.h>

@interface PJLMianVCSqlModel : PJLSQLModel

@property (nonatomic) NSString *id; //项目id

@property (nonatomic) NSString *projectName; //项目名字

@property (nonatomic) NSString *serverAdress; //服务器地址

@end
