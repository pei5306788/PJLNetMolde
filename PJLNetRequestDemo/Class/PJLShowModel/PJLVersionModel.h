//
//  PJLVersionModel.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/3/6.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import <PJLSQLModel/PJLSQLModel.h>
#import <UIKit/UIKit.h>
@interface PJLVersionModel : PJLSQLModel

@property (nonatomic) NSString *id;

@property (nonatomic) NSString *localVersion;


- (void)createAttio:(UIViewController *)superVC;

@end
