//
//  PJLDownLoadSqlModel.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2022/6/21.
//  Copyright © 2022 PJL. All rights reserved.
//

#import "PJLSQLModel.h"

NS_ASSUME_NONNULL_BEGIN
/**
 pjlRequestModel.baseUrl = @"http://www.baidu.com";
 pjlRequestModel.apiName = @"img";
 pjlRequestModel.fileName = @"/bdlogo.png";
 pjlRequestModel.serverPort = @""; //不需要端口 就这样写 ，不然就会读取默认的
 */
@interface PJLDownLoadSqlModel : PJLSQLModel

@property (nonatomic) NSString *id;

@property (nonatomic) NSString *downPathAdress; //下载地址

@property (nonatomic) NSString *status; // 0 是没下载 1 时下载中  2 是下载完成

@property (nonatomic) NSString *baseUrl;

@property (nonatomic) NSString *apiName;

@property (nonatomic) NSString *fileName;

@property (nonatomic) NSString *serverPort;



@end

NS_ASSUME_NONNULL_END
