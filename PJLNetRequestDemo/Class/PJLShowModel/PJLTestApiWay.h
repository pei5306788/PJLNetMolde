//
//  PJLTestApiWay.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/1/9.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PJLRequestModel.h"
#import <UIKit/UIKit.h>
@interface PJLTestApiWay : NSObject

/*
 测试数据接口
 */

- (void)setTestApiModel:(id)model
    withPJLRequestModel:(PJLRequestModel *)pjlNetModel;


/*
 删除对象包括关联关系
 */

- (void)deleteModel:(id)model
          withBlock:(void (^) (BOOL finsh))finshBlock;

@property (nonatomic) UIViewController *superVC;

@end
