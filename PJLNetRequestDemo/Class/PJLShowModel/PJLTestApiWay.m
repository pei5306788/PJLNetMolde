//
//  PJLTestApiWay.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/1/9.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import "PJLTestApiWay.h"


#import "PJLMianVCSqlModel.h"
#import "PJLApiNameModel.h"
#import "PJLParamModel.h"
#import "PJLNetConfigModel.h"
#import "PJLShowAttentionVC.h"
#import "PJLRecordModel.h"
#import "DeleteCountModel.h"
#import "PJLDownLoadSqlModel.h"
@interface PJLTestApiWay(){
 
    NSMutableArray *_recordInfoListArry;
    
    PJLShowAttentionVC *_pjlShowAttentionVC;
    
}

@end

@implementation PJLTestApiWay

- (id)init{
    
    self = [super init];
    if(self){
        
        _recordInfoListArry = [[NSMutableArray alloc]init];
    }
    return self;
    
}

- (void)setTestApiModel:(id)model
    withPJLRequestModel:(PJLRequestModel *)pjlNetModel{
    
    if([model isKindOfClass:[PJLMianVCSqlModel class]]){
        
        PJLMianVCSqlModel *getModel = model;
        
        NSString *serverAdress = @"";
        
        NSString *severPort = @"";
        
        NSArray *serverAressArry =  [getModel.serverAdress componentsSeparatedByString:@":"];
        
        if(serverAressArry.count == 3){
            
            severPort = serverAressArry.lastObject;
            
        }else{
            
            severPort = nil;
        }
        
        if(serverAressArry.count >= 2){
            
            serverAdress = [NSString stringWithFormat:@"%@:%@",serverAressArry.firstObject,
                                    serverAressArry[1]];
            
        }
        
        PJLApiNameModel *apiNameModel = [[PJLApiNameModel alloc]init];
     
        if(pjlNetModel != nil){  //貌似只能是单个接口请求
            
           
            pjlNetModel.baseUrl = serverAdress;
            pjlNetModel.serverPort = severPort;
            
            [apiNameModel getSqlCheckContinue:@[@"superID",
                                                @"=",
                                                getModel.id,
                                                @"id",
                                                @"=",
                                                pjlNetModel.otherFlag]
                              withSucessBlock:^(NSArray *returnArry) {
                                  
                                  if(returnArry.count > 0){
                                      
                                      [self setTestApiModel:returnArry.firstObject
                                        withPJLRequestModel:pjlNetModel];
                                  }
                              }];
            
            return;
        }
        
        [apiNameModel getSqlCheckContinue:@[@"superID",@"=",getModel.id]
                          withSucessBlock:^(NSArray *returnArry) {
            
                              for(PJLApiNameModel *eachModel in returnArry){
                                  
                                  PJLRequestModel  *eachNetModel = [[PJLRequestModel alloc]init];
                                  eachNetModel.baseUrl = serverAdress;
                                  eachNetModel.serverPort = severPort;
                                  [self  setTestApiModel:eachModel
                                     withPJLRequestModel:eachNetModel]; //这个就是单个项目 每个接口都需要走
                                  
                              }
        }];
    }
    
    if([model isKindOfClass:[PJLApiNameModel class]]){
        
         PJLApiNameModel *getModel = model;
        
        if(pjlNetModel.baseUrl == nil || pjlNetModel == nil){ //看是不是要返回上一层
            
            PJLMianVCSqlModel *progectSqlModel = [[PJLMianVCSqlModel alloc]init];
            
            [progectSqlModel getSqlCheckContinue:@[@"id",@"=",getModel.superID] withSucessBlock:^(NSArray *returnArry) {
                
                if(returnArry.count > 0){ //说明存在上一层存在的
                    
                    PJLRequestModel  *newRequestModel = [[PJLRequestModel alloc]init];
                    newRequestModel.apiName = getModel.apiName;
                    newRequestModel.otherFlag = getModel.id;
                    [self setTestApiModel:returnArry.firstObject withPJLRequestModel:newRequestModel];
                    
                }
                
            }];
            
            
            return; //需要返回的那就寻找到上层表
        }
        
        pjlNetModel.apiName = getModel.apiName;
        
        PJLParamModel *paramModel = [[PJLParamModel alloc]init];
        
        [paramModel getSqlCheckContinue:@[@"superId",@"=",getModel.id]
                        withSucessBlock:^(NSArray *returnArry) {
            
                            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                            
                            NSString *apiNameID = @"";
                            
                                for(PJLParamModel *eachModel in  returnArry){
                                    
                                    if(eachModel.pjlKey == nil
                                       ||eachModel.pjlKey.length == 0
                                       || [eachModel.pjlKey isEqualToString:@"<null>"]){
                                        
                                        continue;
                                    }
                                    
                                    if(eachModel.pjlValue == nil
                                       || eachModel.pjlValue.length == 0
                                       || [eachModel.pjlValue isEqualToString:@"<null>"]){
                                        
                                        continue;
                                    }
                                    
                                    apiNameID = eachModel.superId;
                                    
                                    [dic setValue:eachModel.pjlValue forKey:eachModel.pjlKey];
                                    
                                }
                                
                            
                           
                            PJLNetConfigModel *config = [[PJLNetConfigModel alloc]init];
                            
                            [config getSqlCheckContinue:@[@"superId",@"=",getModel.id] withSucessBlock:^(NSArray *returnArry) {
                                
                                if(returnArry.count > 0){
                                    
                                    PJLNetConfigModel *modelConfig = returnArry.firstObject;
                                    
                                    pjlNetModel.requestTimer = [modelConfig.netTimerCount integerValue];
                                    
                                    if(modelConfig.setBaseValueForHttpHeadFileDicJson.length == 0
                                       || modelConfig.setBaseValueForHttpHeadFileDicJson == nil){
                                        
                                        pjlNetModel.setBaseValueForHttpHeadFileDic = nil;
                                    }else{
                                        
                                        NSDictionary *headDic = [self dictionaryWithJsonString:modelConfig.setBaseValueForHttpHeadFileDicJson];
                                        
                                        if([headDic isKindOfClass:[NSDictionary class]]){
                                            
                                            pjlNetModel.setBaseValueForHttpHeadFileDic = headDic;
                                            
                                        }else{
                                            
                                            pjlNetModel.setBaseValueForHttpHeadFileDic = nil;
                                            
                                        }
                                    }
                                    
                                    pjlNetModel.paramDic = dic;
                                    
                                    if([modelConfig.netRequestKind isEqualToString:@"Get"]){
                                        
                                        pjlNetModel.isGet = YES;
                                        
                                       
                                    }else{
                                        
                                        pjlNetModel.isGet = NO;
                                        
                                    }
                                    [self finshRequest:pjlNetModel withApiId:apiNameID];
                                }
                                
                                
                            }];
        }];
    }
}

- (void)finshRequest:(PJLRequestModel *)pjlNetModel
         withApiId:(NSString *)apiId{
    
    [self addTntionVC];
    
    if(apiId.length != 0){
        
        PJLApiNameModel *testAPIName = [[ PJLApiNameModel alloc]init];
        
        [testAPIName getSqlCheckContinue:@[@"id",
                                           @"=",
                                           apiId]
                         withSucessBlock:^(NSArray *returnArry) {
                            
                             if(returnArry.count > 0){
                              
                                 [self finshRequest:pjlNetModel
                                     withApiIdModel:returnArry.firstObject];
                             }
                             
                         }];
        
    }else{
        
        [self finshRequest:pjlNetModel
            withApiIdModel:nil];
    }
}

- (void)finshRequest:(PJLRequestModel *)pjlNetModel
           withApiIdModel:(PJLApiNameModel *)apiNameModel{
 
    PJLRecordModel *saveModel = [[PJLRecordModel alloc]init];
    saveModel.apiName = pjlNetModel.apiName;
    if(pjlNetModel.serverPort != nil){
        
        saveModel.serverStr = [NSString stringWithFormat:@"%@:%@",pjlNetModel.baseUrl,
                               pjlNetModel.serverPort];
        
    }else{
        
        saveModel.serverStr = [NSString stringWithFormat:@"%@",pjlNetModel.baseUrl];
    }
    
    saveModel.paramStr = [NSString stringWithFormat:@"%@",[self dictionaryToJson: pjlNetModel.paramDic]];
    saveModel.requestTimer = [NSString stringWithFormat:@"%f",pjlNetModel.requestTimer ];
    
    pjlNetModel.otherFlag = saveModel;
    
    [pjlNetModel  sendRequestWithsuccess:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
        PJLRecordModel *saveModel1;
        if([otherFlag isKindOfClass:[PJLRecordModel class]]){
            
            saveModel1 = (PJLRecordModel *)otherFlag;
            saveModel1.resposeString = [NSString stringWithFormat:@"%@",[self dictionaryToJson:responseObject]];
        }
        
        
        if(saveModel1 != nil){
            
            [self->_recordInfoListArry addObject:saveModel1];
            
            [self addTntionVC];
            
        }
        
        if(apiNameModel != nil){
            
            apiNameModel.statusRecord = @"2";
            [apiNameModel checkStatus];
            [apiNameModel saveOrUpDataModel:@[@"id",@"=",[NSString stringWithFormat:@"%@",apiNameModel.id]]];
        }
        
    } faileure:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
        
        PJLRecordModel *saveModel1;
        if([otherFlag isKindOfClass:[PJLRecordModel class]]){
            
            saveModel1 = (PJLRecordModel *)otherFlag;
            saveModel1.resposeString = [NSString stringWithFormat:@"%@",[self dictionaryToJson:responseObject]];
        }
        
        
        if(saveModel1 != nil){
            
            [self->_recordInfoListArry addObject:saveModel1];
            
            [self addTntionVC];
            
        }
        
        if(apiNameModel != nil){
            
            apiNameModel.statusRecord = @"1";
            [apiNameModel checkStatus];
            [apiNameModel saveOrUpDataModel:@[@"id",@"=",[NSString stringWithFormat:@"%@",apiNameModel.id]]];
        }
        
        
        
    } progress:^(NSString *ApiName, id responseObject, NSString *otherFlag) {
        
        
        
    }];
    
}

- (void)addTntionVC{
   
    if(_pjlShowAttentionVC == nil){
        
        _pjlShowAttentionVC = [[PJLShowAttentionVC alloc]init];
     
        [_pjlShowAttentionVC setModalPresentationStyle:UIModalPresentationCustom];
        
        UINavigationController *modelNA = [[UINavigationController alloc]initWithRootViewController:_pjlShowAttentionVC];
        
        if(_superVC != nil){
            
            [_superVC presentViewController:modelNA
                                   animated:YES
                                 completion:^{
                                     
                
            }];
        }
        
        
    }
    
    [_pjlShowAttentionVC setDataList: _recordInfoListArry];
    
}



-(NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
{
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err)
    {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

-(NSString *)dictionaryToJson:(NSDictionary *)dic
{
    if([dic isKindOfClass:[NSDictionary class]]){
        
        return [NSString stringWithFormat:@"%@",dic];
        
    }
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&error];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

/*
 删除对象包括关联关系
 */

- (void)deleteModel:(id)model
          withBlock:(void (^) (BOOL finsh))finshBlock{
    
    
    if([model isKindOfClass:[PJLDownLoadSqlModel class]]){
        
        PJLDownLoadSqlModel *pjlDownLoadSqlModel = model;
        
        [pjlDownLoadSqlModel deleteContinue:@[@"id",@"=",pjlDownLoadSqlModel.id] withSucessBlock:finshBlock];
     
        
        return;
        
    }
    
    if([model isKindOfClass:[PJLMianVCSqlModel class]]){ //如果是要删除项目
        
        PJLMianVCSqlModel *progectModel = model;
        
        PJLApiNameModel *apiNameModel = [[PJLApiNameModel alloc]init];
        
        
        [apiNameModel getSqlCheckContinue:@[@"superID",@"=",progectModel.id] withSucessBlock:^(NSArray *returnArry) {
        
            
            if(returnArry.count == 0){ //全部都删除了哇 最后删除这个项目
                
                [progectModel deleteContinue:@[@"id",@"=",progectModel.id] withSucessBlock:^(BOOL isSucess) {

                    finshBlock(YES);
                    
                }];

            }else{
             
                //如果还存在的话 那就继续检查删除最新的一个 还没完成删除的
                  finshBlock(NO);
                [self deleteModel:returnArry.firstObject withBlock:^(BOOL finsh) {
                    
                    
                    if(finsh == YES){ //这个接口完成删除了
                        
                        //再次查询是否有其他的接口
                        [self deleteModel:progectModel withBlock:^(BOOL finsh) {
                            
                            if(finsh == YES){
                                
                                  finshBlock(YES);
                            }
                            
                        }];
                        
                    }
                    
                }];
                
            }
        }];
    }
    
    if([model isKindOfClass:[PJLApiNameModel class]]){  //删除项目中的 某个接口
        
        PJLApiNameModel *apiNameModel = model;
        
        PJLParamModel *deleteModel = [[PJLParamModel alloc]init];
       
        [deleteModel getSqlCheckContinue:@[@"superId",@"=",apiNameModel.id] withSucessBlock:^(NSArray *returnArry) {
            
            if(returnArry.count == 0){
                
                PJLNetConfigModel *deleteConfigModel = [[PJLNetConfigModel alloc]init];
                
                [deleteConfigModel deleteContinue:@[@"superId",@"=",apiNameModel.id] withSucessBlock:^(BOOL isSucess) {
                    
                    [apiNameModel deleteContinue:@[@"id",@"=",apiNameModel.id] withSucessBlock:^(BOOL isSucess) {
                        
                         finshBlock(YES);
                    }];
                    
                }];
               
            }else{
                
                finshBlock(NO);
                PJLParamModel *getNeedDeleteModel = returnArry.firstObject;
                
                [self deleteModel:getNeedDeleteModel withBlock:^(BOOL finsh) {
                   
                    if(finsh == YES){  //参数重的一个被删除了 那么再次检查参数
                        
                        [self deleteModel:apiNameModel withBlock:^(BOOL finsh) {
                            
                            if(finsh == YES){
                                
                                finshBlock(YES);
                            }
                        }];
                    }
                    
                }];
                
                
            }
        }];
        
        
    }
    
    if([model isKindOfClass:[PJLParamModel class]]){ //删除接口中的参数
        
        PJLParamModel *getModel = model;
    
        [getModel deleteContinue:@[@"id",@"=",getModel.id] withSucessBlock:^(BOOL isSucess) {
           
            finshBlock(YES);
            
        }];
    }
    
    
}

@end
