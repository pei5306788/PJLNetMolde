//
//  PJLApiNameModel.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/27.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <PJLSQLModel/PJLSQLModel.h>

@interface PJLApiNameModel : PJLSQLModel

@property (nonatomic) NSString *id;    //表内的id

@property (nonatomic) NSString *apiName;  //接口名字

@property (nonatomic) NSString *reMark; //备注

@property (nonatomic) NSString *superID; //上一张表的id

@property (nonatomic) NSString *statusRecord; //状态记录 0 是未尝试 1 是上一次尝试失败 2 是上一次尝试成功

@property (nonatomic) NSString *recordMessage; //成功或失败的备注

- (void)checkStatus;

@end
