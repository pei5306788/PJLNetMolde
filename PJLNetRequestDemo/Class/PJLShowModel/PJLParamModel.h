//
//  PJLParamModel.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/28.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <PJLSQLModel/PJLSQLModel.h>

@interface PJLParamModel : PJLSQLModel

@property (nonatomic) NSString *id;

@property (nonatomic) NSString *pjlKey; //字典key

@property (nonatomic) NSString *pjlValue; //字典value

@property (nonatomic) NSString *superId; //对应项目的ID

@end
