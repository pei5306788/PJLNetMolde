//
//  PJLVersionModel.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/3/6.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import "PJLVersionModel.h"
#import <UIKit/UIKit.h>

@implementation PJLVersionModel

- (id)init{
    
    self = [super init];
    if(self){
        
        self.userModel = self;
    }
    return self;
    
}

- (void)createAttio:(UIViewController *)superVC{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
   

    NSString *getVersion =  [infoDictionary objectForKey:@"CFBundleShortVersionString"];

    if(![getVersion isEqualToString:[NSString stringWithFormat:@"%@",self.localVersion]]){
        
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"简单说明" message:@"本应用就是方便测试api，没有隐藏功能，请大家放心使用。使用->配置服务器地址->配置接口名->配置参数->配置网络属性->点击选择开始请求->控制台" preferredStyle:(UIAlertControllerStyleAlert)];
        
        
        
        
    
        UIAlertAction *cancerClip = [UIAlertAction actionWithTitle:@"知道了" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alertController addAction:cancerClip];
        
        [superVC presentViewController:alertController animated:true completion:^{
            
        }];
       
    
        
        self.localVersion = [NSString stringWithFormat:@"%@",getVersion];
        
        [self saveOrUpDataModel:@[@"id",@"=",[NSString stringWithFormat:@"%@",self.id]]];
        
    }
    
    
  
    
}

@end
