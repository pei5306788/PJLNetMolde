//
//  PJLNetConfigModel.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/29.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <PJLSQLModel/PJLSQLModel.h>

@interface PJLNetConfigModel : PJLSQLModel

@property (nonatomic) NSString *id;

@property (nonatomic) NSString *superId;

@property (nonatomic) NSString *netTimerCount; //参数超时时间

@property (nonatomic) NSString *netRequestKind; //请求类型 getORPost;

@property (nonatomic) NSString *setBaseValueForHttpHeadFileDicJson; //设置授权 json方式保存在数据库里面的



@end
