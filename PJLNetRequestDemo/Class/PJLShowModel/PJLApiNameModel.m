//
//  PJLApiNameModel.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/27.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLApiNameModel.h"

@implementation PJLApiNameModel

- (id)init{
    
    self = [super init];
    if(self){
        
        self.userModel = self;
        
        self.reMark = @"0";
        
        self.recordMessage = @"从未测试过";
        
    }
    return self;
    
}

- (void)checkStatus{
    // 0 是未尝试 1 是上一次尝试失败 2 是上一次尝试成功
    NSArray *messageArry = @[@"从未测试过",
                             @"上一次失败",
                             @"上一次成功"];
    
    if([self.statusRecord integerValue] < messageArry.count){
        
        self.recordMessage = messageArry[[self.statusRecord integerValue]];
    }
    
}

@end
