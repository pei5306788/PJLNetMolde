//
//  PJLRecordModel.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/1/11.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import <PJLSQLModel/PJLSQLModel.h>

@interface PJLRecordModel : PJLSQLModel

@property (nonatomic) NSString *id;

@property (nonatomic) NSString *serverStr; //服务器地址

@property (nonatomic) NSString *requestTimer; //请求时间

@property (nonatomic) NSString *headDicStr; //头部文件

@property (nonatomic) NSString *apiName; //接口名字

@property (nonatomic) NSString *superId; //上一层ip

@property (nonatomic) NSString *resposeString; //返回字符串

@property (nonatomic) NSString *paramStr;// 传入参数说明

@property (nonatomic) NSString *cellHeight; //高度

@property (nonatomic) NSString *headCellHeight; //头高度


//@property (nonatomic) NSString *

@end
