//
//  ShowDetailTextView.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/27.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "ShowDetailTextView.h"
#import "PJLRecordModel.h"
@interface ShowDetailTextView(){
    
    UITextView *_showDetailTextView;
    
    PJLRecordModel *_recordModel;
    
}

@end

@implementation ShowDetailTextView

- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        _showDetailTextView = [[UITextView alloc]initWithFrame:self.bounds];
        
        [self addSubview:_showDetailTextView];
        
        _showDetailTextView.scrollEnabled = YES;
        
        _showDetailTextView.textColor = [UIColor whiteColor];
        
        [_showDetailTextView setBackgroundColor:[UIColor grayColor]];
        
        [_showDetailTextView setFont:[UIFont systemFontOfSize:28]];
        
        [self.layer setMasksToBounds:YES];
    }
    return self;
    
}

- (CGFloat)setModel:(id)model{
    
    CGFloat returnHeight = 0.0;
    
    if([model isKindOfClass:[PJLRecordModel class]]){
        _recordModel = model;
        
        _showDetailTextView.text = [NSString stringWithFormat:@"服务器地址:%@\n接口名字:%@\n 传参数:%@",
                                    _recordModel.serverStr,
                                    _recordModel.apiName,
                                    _recordModel.paramStr];
        
     
        CGSize getSize = [_showDetailTextView sizeThatFits:CGSizeMake(_showDetailTextView.frame.size.width
                                                                      , CGFLOAT_MAX)];
        
        _showDetailTextView.frame = CGRectMake(_showDetailTextView.frame.origin.x,
                                               _showDetailTextView.frame.origin.y,
                                               _showDetailTextView.frame.size.width,
                                               getSize.height);
        
        
        returnHeight = CGRectGetMaxY(_showDetailTextView.frame);
        
        
        if(returnHeight == [_recordModel.headCellHeight floatValue] ){
            
        }else{
            
            _recordModel.headCellHeight = [NSString stringWithFormat:@"%lf",returnHeight];
            
            if(self.pjlSuperView != nil){
                
                UITableView *modelTableView = _pjlSuperView;
                
                [modelTableView reloadData];
            }
            
        }
        
        
    }
    
//    [self ta]
    
    self.frame = CGRectMake(self.frame.origin.x,
                            self.frame.origin.y,
                            self.frame.size.width,
                            returnHeight);
    
    

    return returnHeight;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
