//
//  PJLEditCollectionViewCell.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2022/6/21.
//  Copyright © 2022 PJL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PJLDownLoadSqlModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PJLEditCollectionViewCell : UICollectionViewCell

- (void)setModel:(PJLDownLoadSqlModel *)pjlDownLoadSqlModel
        withKind:(NSInteger)kind;

@end

NS_ASSUME_NONNULL_END
