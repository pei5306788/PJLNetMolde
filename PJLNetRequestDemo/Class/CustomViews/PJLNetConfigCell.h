//
//  PJLNetConfigCell.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/29.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PJLNetConfigCell : UITableViewCell

@property (nonatomic) id pjlSuperView;

- (void)setModel:(id)model
   withLeftTitle:(NSString *)leftTitle
       withIndex:(NSInteger)paramIndex;

@end
