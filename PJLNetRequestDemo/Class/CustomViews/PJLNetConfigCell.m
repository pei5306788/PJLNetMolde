//
//  PJLNetConfigCell.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/29.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLNetConfigCell.h"
#import "PJLNetConfigModel.h"
#import "PJLActionSheetWay.h"
@interface PJLNetConfigCell()<UITextFieldDelegate>{
    
    UITextField *_middleTextField;
    
    UILabel *_leftLabel;
    
    PJLNetConfigModel *_pjlNetConfigModel;
    
    NSInteger _indexPathRow;
    
    PJLActionSheetWay *_pjlActionSheetWay;
}

@end

@implementation PJLNetConfigCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
     
        _middleTextField = [[UITextField alloc]initWithFrame:CGRectMake(10,
                                                                        0,
                                                                        [UIScreen mainScreen].bounds.size.width - 10,
                                                                        44.0)];
        
        _middleTextField.delegate = self;
        [self.contentView addSubview:_middleTextField];
        
        
        _leftLabel = [[UILabel alloc]init];
        _leftLabel.textAlignment = NSTextAlignmentRight;
        [_middleTextField setLeftViewMode:UITextFieldViewModeAlways];
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
        
        rightButton.frame = CGRectMake(0, 0, _middleTextField.frame.size.height, _middleTextField.frame.size.height);
        
        [rightButton addTarget:self
                        action:@selector(dicToStr)
              forControlEvents:UIControlEventTouchUpInside];
        
        [_middleTextField setRightView:rightButton];
        
    }
    return self;
    
}

- (void)dicToStr{
    
    _pjlActionSheetWay = [[PJLActionSheetWay alloc]init];
    
    _pjlActionSheetWay.superModelID = _pjlNetConfigModel;
    
    _pjlActionSheetWay.superView = _pjlSuperView;
    
    [_pjlActionSheetWay createActionSheetView];
    
}

-(void)setModel:(id)model
  withLeftTitle:(NSString *)leftTitle
      withIndex:(NSInteger)paramIndex{
    
    _indexPathRow = paramIndex;
    
    _pjlNetConfigModel = model;
    
    [_middleTextField setRightViewMode:UITextFieldViewModeNever];
    
    if(_indexPathRow == 0){
        
        _middleTextField.text = [NSString stringWithFormat:@"%@",_pjlNetConfigModel.netTimerCount];
        
    }
    
    if(_indexPathRow == 1){
     
        _middleTextField.text = [NSString stringWithFormat:@"%@",_pjlNetConfigModel.netRequestKind];
        
    }
    
    if(_indexPathRow == 2){
        
        _middleTextField.text = _pjlNetConfigModel.setBaseValueForHttpHeadFileDicJson;
        
        [_middleTextField setRightViewMode:UITextFieldViewModeAlways];
    }
    _leftLabel.text = leftTitle;
    
    CGSize getSzie = [_leftLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    
    _leftLabel.frame = CGRectMake(0, 0, getSzie.width, getSzie.height);
    
    [_middleTextField setLeftView:_leftLabel];
    
   
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark textFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    
    
    if(_indexPathRow == 0){
        
       
        _pjlNetConfigModel.netTimerCount = textField.text;
    }
    
    if(_indexPathRow == 1){
        
        _pjlNetConfigModel.netRequestKind = textField.text;
        
    }
    
    if(_indexPathRow == 2){
        
        _pjlNetConfigModel.setBaseValueForHttpHeadFileDicJson = textField.text;
       
    }
    
    [_pjlNetConfigModel saveOrUpDataModel:@[@"id",
                                            @"=",
                                            _pjlNetConfigModel.id]];
    
}


@end
