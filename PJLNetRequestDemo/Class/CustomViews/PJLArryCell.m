//
//  PJLArryCell.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/1/16.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import "PJLArryCell.h"
#import "PJLArryModel.h"
#import "PJLActionSheetWay.h"
@interface PJLArryCell()<UITextFieldDelegate>{
    
    UITextField *_middleTextField;
    
    UILabel *_leftLabel;
    
    UIButton *_rightButton;
    
    PJLArryModel *_recordModel;
    
    PJLActionSheetWay *_pjlActionSheetWay;
}

@end

@implementation PJLArryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        
        _middleTextField = [[UITextField alloc]initWithFrame:CGRectMake(0, 10, [UIScreen mainScreen].bounds.size.width, 44)];
        
        [self.contentView addSubview:_middleTextField];
        
        _leftLabel = [[UILabel alloc]init];
        
        [_middleTextField setLeftView:_leftLabel];
        
        [_middleTextField setLeftViewMode:UITextFieldViewModeAlways];
        
        _middleTextField.delegate = self;
        
        _rightButton = [UIButton buttonWithType:UIButtonTypeInfoDark];
        
        _rightButton.frame = CGRectMake(0, 0,_middleTextField.frame.size.height , _middleTextField.frame.size.height);
        
        [_middleTextField setRightView:_rightButton];
        
        [_middleTextField setRightViewMode:UITextFieldViewModeAlways];
        
        [_rightButton addTarget:self
                         action:@selector(jsonDetailButton)
               forControlEvents:UIControlEventTouchUpInside];
        
    }
    return self;
    
}

- (void)setModel:(id)model
       wtihIndex:(NSIndexPath *)indexPath{
    
    _leftLabel.text = [NSString stringWithFormat:@"%ld_%ld:",indexPath.section,indexPath.row];
    
    CGSize getSize = [_leftLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    
    _leftLabel.frame = CGRectMake(0, 0, getSize.width, getSize.height);
    
    [_middleTextField setLeftView:_leftLabel];
    
    if([model isKindOfClass:[PJLArryModel class]]){
        
        _recordModel = model;
        
        NSString *getString = _recordModel.contentStr;
        
        _middleTextField.text = getString;
        
    }
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)jsonDetailButton{
    
    _pjlActionSheetWay = [[PJLActionSheetWay alloc]init];
    
    _pjlActionSheetWay.superModelID = _recordModel;
    
    _pjlActionSheetWay.superView = _superView;
    
    [_pjlActionSheetWay createActionSheetView];
    
}

#pragma mark textFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    
    _recordModel.contentStr = textField.text;
}

@end
