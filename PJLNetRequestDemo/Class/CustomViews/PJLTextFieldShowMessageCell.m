//
//  PJLTextFieldShowMessageCell.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/1/10.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import "PJLTextFieldShowMessageCell.h"
#import "PJLRecordModel.h"
@interface PJLTextFieldShowMessageCell(){
    
    UITextView *_middleTextView;
    
    PJLRecordModel *_recordModel;
}

@end

@implementation PJLTextFieldShowMessageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        
        _middleTextView = [[UITextView alloc]initWithFrame:CGRectMake(0, 10, [UIScreen mainScreen].bounds.size.width, 0)];
        
        _middleTextView.editable = NO;
        
//        _middleTextView.scrollEnabled = NO;
        
        [self.contentView addSubview:_middleTextView];
    }
    return self;
    
}

- (CGFloat)setModel:(id)model
     withResposeStr:(NSString *)returnStr{
    
    _recordModel = model;
    
    CGFloat rerunHeight = 0.0;
    
    _middleTextView.text = [NSString stringWithFormat:@"%@",returnStr];
    
    _middleTextView.textColor = [UIColor greenColor];
    
    CGSize getSize = [_middleTextView sizeThatFits:CGSizeMake(_middleTextView.frame.size.width, CGFLOAT_MAX)];
    
//    CGFloat retrunHeight = 0.0;
    
    
    
    _middleTextView.frame = CGRectMake(_middleTextView.frame.origin.x,
                                       _middleTextView.frame.origin.y,
                                       _middleTextView.frame.size.width,
                                       getSize.height <= 40 ? getSize.height : 40);
    
    rerunHeight = CGRectGetMaxY(_middleTextView.frame);
    
    rerunHeight = rerunHeight + 10;
    
    
    
    return rerunHeight;
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
