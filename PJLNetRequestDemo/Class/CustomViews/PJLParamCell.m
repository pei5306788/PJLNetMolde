//
//  PJLParamCell.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/28.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLParamCell.h"
#import "PJLParamModel.h"
#import "PJLDictOrArryToJsonVC.h"
#import "PJLActionSheetWay.h"
@interface PJLParamCell()<UITextFieldDelegate,UIActionSheetDelegate>{
 
    UITextField *_leftTextField;
    
    UITextField *_rightTextField;
    
    PJLParamModel *_recordModel;
    
    PJLActionSheetWay *_pjlActionSheetWay;
}

@end

@implementation PJLParamCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        
        _leftTextField = [[UITextField alloc]initWithFrame:CGRectMake(0,
                                                                      10,
                                                                      [UIScreen mainScreen].bounds.size.width/2.0,
                                                                      44.0)];
        
        
        [self.contentView addSubview:_leftTextField];
        
        _rightTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_leftTextField.frame), _leftTextField.frame.origin.y, _leftTextField.frame.size.width, _leftTextField.frame.size.height)];
        
        [self.contentView addSubview:_rightTextField];
        
        
        UILabel *leftLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
        
        [leftLabel setFont:[UIFont systemFontOfSize:13.0]];
        
        leftLabel.text = @"key:";
        
        
        
        CGSize getSize = [leftLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
        
        leftLabel.frame = CGRectMake(0, 0, getSize.width, getSize.height);
        
        [_leftTextField setLeftView:leftLabel];
        
        [_leftTextField setLeftViewMode:UITextFieldViewModeAlways];
        
        UILabel *rightLabel = [[UILabel alloc]init];
        [rightLabel setFont:[UIFont systemFontOfSize:13.0]];
        rightLabel.text = @"value:";
        CGSize getSize2 = [rightLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
        
        rightLabel.frame = CGRectMake(0, 0, getSize2.width, getSize2.height);
        
        [_rightTextField setLeftViewMode:UITextFieldViewModeAlways];
        
        [_rightTextField setLeftView:rightLabel];
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
        rightButton.frame = CGRectMake(0,
                                       0,
                                       _rightTextField.frame.size.height,
                                       _rightTextField.frame.size.height);
        
        [_rightTextField setRightView:rightButton];
        
        [_rightTextField setRightViewMode:UITextFieldViewModeAlways];
        
        _leftTextField.delegate = self;
        
        _rightTextField.delegate = self;
        
        [_leftTextField setBackgroundColor:[UIColor colorWithRed:23.0/255.0 green:242.0/255.0 blue:188.0/255.0 alpha:1]];
        
        [_rightTextField setBackgroundColor:[UIColor colorWithRed:16.0/255.0 green:239.0/255.0 blue:242.0/255.0 alpha:1]];
        
        [rightButton addTarget:self
                        action:@selector(chooseSpecialValue)
              forControlEvents:UIControlEventTouchUpInside];
        
        _rightTextField.tag = 1;
        
        [self setSelectionStyle:(UITableViewCellSelectionStyleNone)];
    }
    return self;
}

- (void)setModel:(id)model{
    
    _recordModel = model;
    
    if([model isKindOfClass:[PJLParamModel class]]){
        
        if(_recordModel.pjlKey == nil || [_recordModel.pjlKey isEqualToString:@"<null>"]){
            
              _leftTextField.text = @"";
        }else{
               _leftTextField.text = _recordModel.pjlKey;
        }
        
        if(_recordModel.pjlValue == nil || [_recordModel.pjlValue isEqualToString:@"<null>"]){
            
            _rightTextField.text = @"";
        }else{
            
            _rightTextField.text = _recordModel.pjlValue;

        }
     
    
      
        
        
    }
    
}

- (void)chooseSpecialValue{
    
    [self endEditing:YES];
    
    _pjlActionSheetWay = [[PJLActionSheetWay alloc]init];
    
    _pjlActionSheetWay.superModelID = _recordModel;
    
    _pjlActionSheetWay.superView = _superView;
    
    [_pjlActionSheetWay createActionSheetView];
    
//    UIActionSheet *pjlActionSheet = [[UIActionSheet alloc]initWithTitle:@"选择value特殊来源"
//                                                               delegate:self
//                                                      cancelButtonTitle:@"取消"
//                                                 destructiveButtonTitle:@"数组转json"
//                                                      otherButtonTitles:@"字典转json",
//                                     nil];
//
//    [pjlActionSheet showInView:self];
}


#pragma mark  textFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if(textField.tag == 0){
        
        _recordModel.pjlKey = textField.text;
        
    }else{
        
        _recordModel.pjlValue = textField.text;
        
    }
    
    if(_isNotNeedSave == NO){
        
        [_recordModel saveOrUpDataModel:@[@"id",@"==",_recordModel.id]];
        
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//#pragma mark UIActionSheetDelegate
//
//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex NS_DEPRECATED_IOS(2_0, 8_3) __TVOS_PROHIBITED{
//
//    if(buttonIndex == actionSheet.cancelButtonIndex){
//
//        return;
//    }
//
//    if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"数组转json"]){
//
//        if([_superView isKindOfClass:[UIViewController class]]){
//
//            PJLDictOrArryToJsonVC *pjlDictorArryToJsonVC = [[PJLDictOrArryToJsonVC alloc]init];
//            pjlDictorArryToJsonVC.pjlParamModel = _recordModel;
//            pjlDictorArryToJsonVC.kind = 0;
//            UINavigationController *pjlNavc = [[UINavigationController alloc]initWithRootViewController:pjlDictorArryToJsonVC];
//
//            UIViewController *superVc = _superView;
//
//            [superVc presentViewController:pjlNavc animated:YES completion:^{
//
//            }];
//
//        }
//    }
//
//    if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"字典转json"]){
//
//        PJLDictOrArryToJsonVC *pjlDictorArryToJsonVC = [[PJLDictOrArryToJsonVC alloc]init];
//        pjlDictorArryToJsonVC.pjlParamModel = _recordModel;
//        pjlDictorArryToJsonVC.kind = 1;
//        UINavigationController *pjlNavc = [[UINavigationController alloc]initWithRootViewController:pjlDictorArryToJsonVC];
//
//        UIViewController *superVc = _superView;
//
//        [superVc presentViewController:pjlNavc animated:YES completion:^{
//
//        }];
//
//    }
//
//}


@end
