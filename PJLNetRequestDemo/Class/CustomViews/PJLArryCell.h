//
//  PJLArryCell.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/1/16.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PJLArryCell : UITableViewCell

@property (nonatomic) id superView;

- (void)setModel:(id)model
       wtihIndex:(NSIndexPath *)indexPath;

@end
