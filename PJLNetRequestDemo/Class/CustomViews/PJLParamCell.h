//
//  PJLParamCell.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/28.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PJLParamCell : UITableViewCell

- (void)setModel:(id)model;

@property (nonatomic) BOOL isNotNeedSave;

@property (nonatomic) id superView;

@end
