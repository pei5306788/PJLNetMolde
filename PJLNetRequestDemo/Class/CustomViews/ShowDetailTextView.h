//
//  ShowDetailTextView.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2017/12/27.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowDetailTextView : UIView

- (CGFloat)setModel:(id)model;

@property (nonatomic) id  pjlSuperView;

@end
