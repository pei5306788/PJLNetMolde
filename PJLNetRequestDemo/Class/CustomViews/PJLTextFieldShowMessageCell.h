//
//  PJLTextFieldShowMessageCell.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/1/10.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PJLTextFieldShowMessageCell : UITableViewCell

- (CGFloat)setModel:(id)model
     withResposeStr:(NSString *)returnStr;

@property (nonatomic) id  pjlSuperView;

@property (nonatomic) NSMutableArray *superHeightArry;

@property (nonatomic) NSIndexPath *indexPath;

@end
