//
//  PJLCollectionViewCell.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2022/6/21.
//  Copyright © 2022 PJL. All rights reserved.
//

#import "PJLCollectionViewCell.h"
#import "CustomLabel.h"
@interface PJLCollectionViewCell(){
    UILabel *_middleLabel;
}

@end

@implementation PJLCollectionViewCell

- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if(self){
        
        _middleLabel = [[CustomLabel alloc]init];
        
        
        [_middleLabel setNumberOfLines:0];
        
        _middleLabel.textColor = [UIColor whiteColor];
        _middleLabel.textAlignment = NSTextAlignmentCenter;
        
        [_middleLabel setFont:[UIFont systemFontOfSize:24]];
        
        [self addSubview:_middleLabel];
        
        
        _middleLabel.frame = self.bounds;
        
        [self setBackgroundColor:[UIColor systemTealColor]];
        
    }
    
    return self;
    
}

- (void)setModel:(NSString *)titleString
        withKind:(NSInteger)kind{
    
    
    _middleLabel.text = titleString;
}

@end
