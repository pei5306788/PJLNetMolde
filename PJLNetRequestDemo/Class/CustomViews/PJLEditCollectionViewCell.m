//
//  PJLEditCollectionViewCell.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2022/6/21.
//  Copyright © 2022 PJL. All rights reserved.
//

#import "PJLEditCollectionViewCell.h"
#import "CustomLabel.h"
#import "PJLRequestModel.h"
#import "CustomImageView.h"
@interface PJLEditCollectionViewCell(){
    
    CustomLabel *_middleLabel;
    
    NSMutableAttributedString *_modelString;
    
    CustomImageView *_downImageView;
    
    
    
}

@end

@implementation PJLEditCollectionViewCell

- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if(self){
     
        
        _middleLabel = [[CustomLabel alloc]initWithFrame:self.bounds];
        [_middleLabel setFont:[UIFont systemFontOfSize:24.0]];
        [_middleLabel setNumberOfLines:0];
        _middleLabel.text = @"";
        _middleLabel.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:_middleLabel];
        
        
        _downImageView = [[CustomImageView alloc]initWithFrame:CGRectMake(0, self.frame.size.height - 100, self.frame.size.width, 100)];
        
        [_downImageView setContentMode:(UIViewContentModeScaleToFill)];
        
        [self addSubview:_downImageView];
        
        [_downImageView setBackgroundColor:[UIColor grayColor]];
        _downImageView.alpha = 0.5;
        [self setBackgroundColor:[UIColor systemTealColor]];
    }
    
    return self;
    
}

- (void)setModel:(PJLDownLoadSqlModel *)pjlDownLoadSqlModel
        withKind:(NSInteger)kind{
    
  
    
    if(kind == 0){
        _modelString = [[NSMutableAttributedString alloc]init];
      
        [self setTitleContent:[NSString stringWithFormat:@"服务器地址:%@\n端口：%@\n接口名字:%@\n图片名称：%@",pjlDownLoadSqlModel.baseUrl,pjlDownLoadSqlModel.serverPort,pjlDownLoadSqlModel.apiName,pjlDownLoadSqlModel.fileName] withKind:1];
        
    
        if([pjlDownLoadSqlModel.status intValue] == 1){
            
            [self beginDowLoad:pjlDownLoadSqlModel];
        }
        
       
        
    }
    
    
    
}

- (void)beginDowLoad:(PJLDownLoadSqlModel *)pjlDownLoadSqlModel{
    
  
   
    [self setTitleContent:@"日志:开始下载图片" withKind:1];
    
    //示列:http://www.baidu.com/img/bdlogo.png
        
        PJLRequestModel *pjlRequestModel = [[PJLRequestModel alloc]init];

        pjlRequestModel.baseUrl = pjlDownLoadSqlModel.baseUrl;
        pjlRequestModel.apiName = pjlDownLoadSqlModel.apiName;
        pjlRequestModel.fileName = pjlDownLoadSqlModel.fileName;
        pjlRequestModel.serverPort = pjlDownLoadSqlModel.serverPort; //不需要端口 就这样写 ，不然就会读取默认的
        
        [pjlRequestModel downLoadRequestSucess:^(NSString *ApiName,
                                                 id responseObject,
                                                 NSString *otherFlag) {
            
            //通知主线程刷新
            dispatch_async(dispatch_get_main_queue(), ^{
                //回调或者说是通知主线程刷新，
//                self->_middleLabel.text = [NSString stringWithFormat:@"%@\n下载成功：%@",self->_middleLabel.text,responseObject];
                
                [self setTitleContent:[NSString stringWithFormat:@"\n下载成功本地保存地址：%@",responseObject] withKind:1];
                
                pjlDownLoadSqlModel.status = @"0";
                
                
                NSData *fileData = [NSData dataWithContentsOfFile:responseObject];
                
                
                UIImage *image = [UIImage imageWithData:fileData];
                
                [self->_downImageView setImage:image];
                
                
            });
            
        } faileure:^(NSString *ApiName,
                     id responseObject,
                     NSString *otherFlag) {
            //通知主线程刷新
            dispatch_async(dispatch_get_main_queue(), ^{
//                self->_middleLabel.text = [NSString stringWithFormat:@"%@\n下载失败：%@",self->_middleLabel.text,responseObject];
                
                [self setTitleContent:[NSString stringWithFormat:@"\n下载失败：%@",responseObject] withKind:1];
                
                pjlDownLoadSqlModel.status = @"0";
                
            });
            
        } progress:^(NSString *ApiName,
                     id responseObject,
                     NSString *otherFlag) {
            
            NSProgress *getProgress = responseObject;
            
          
            //通知主线程刷新
            dispatch_async(dispatch_get_main_queue(), ^{
//                self->_middleLabel.text = [NSString stringWithFormat:@"%@\n下载进度：%lf",self->_middleLabel.text,getProgress.fractionCompleted];
                
                [self setTitleContent:[NSString stringWithFormat:@"\n下载进度：%lf",getProgress.fractionCompleted] withKind:1];
                
               
                
            });
            
        }];
}

- (void)setTitleContent:(NSString *)addData
               withKind:(NSInteger)kind{
    
    NSMutableParagraphStyle *modelStyle = [[NSMutableParagraphStyle alloc]init];
    modelStyle.alignment = NSTextAlignmentCenter;
    PJLQRTitleNobject *model_1 = [[PJLQRTitleNobject alloc]init];
    if(kind == 0){
        model_1.labeFont = [UIFont systemFontOfSize:28];
        
    }
    
    if(kind == 1){
        
        model_1.labeFont = [UIFont systemFontOfSize:14];
        
    }
    model_1.labelColor = [UIColor whiteColor];
    model_1.lableText = addData;
    model_1.modelParagrapStyle = modelStyle;
    
    [_modelString appendAttributedString:[_middleLabel pjlMoreInfo2:@[model_1]]];
    
    _middleLabel.attributedText = _modelString;
    
}



@end
