//
//  PJLActionSheetWay.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/1/18.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import "PJLActionSheetWay.h"
#import <UIKit/UIKit.h>
#import "PJLDictOrArryToJsonVC.h"
#import "PJLParamModel.h"
#import "PJLArryModel.h"
#import "PJLNetConfigModel.h"
@interface PJLActionSheetWay()<UIActionSheetDelegate>{
    
    UIActionSheet *_actionSheet;
}

@end

@implementation PJLActionSheetWay

- (id)init{
    self = [super init];
    if(self){
        
        
    }
    return self;
}

- (void)createActionSheetView{
    
    
    
   _actionSheet = [[UIActionSheet alloc]initWithTitle:@"选择value特殊来源"
                                                               delegate:self
                                                      cancelButtonTitle:@"取消"
                                                 destructiveButtonTitle:@"数组转json"
                                                      otherButtonTitles:@"字典转json",
                                     @"查看详细信息",
                                     nil];
    
    [_actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    
}

#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex NS_DEPRECATED_IOS(2_0, 8_3) __TVOS_PROHIBITED{
    
    if(buttonIndex == actionSheet.cancelButtonIndex){
        
        return;
    }
    
    if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"数组转json"]){
        
        if([_superView isKindOfClass:[UIViewController class]]){
            
            PJLDictOrArryToJsonVC *pjlDictorArryToJsonVC = [[PJLDictOrArryToJsonVC alloc]init];
            pjlDictorArryToJsonVC.paramModel = _superModelID;
            pjlDictorArryToJsonVC.kind = 0;
            UINavigationController *pjlNavc = [[UINavigationController alloc]initWithRootViewController:pjlDictorArryToJsonVC];
            
            UIViewController *superVc = _superView;
            
            [superVc presentViewController:pjlNavc animated:YES completion:^{
                
            }];
            
        }
    }
    
    if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"字典转json"]){
        
        PJLDictOrArryToJsonVC *pjlDictorArryToJsonVC = [[PJLDictOrArryToJsonVC alloc]init];
        pjlDictorArryToJsonVC.paramModel = _superModelID;
        pjlDictorArryToJsonVC.kind = 1;
        UINavigationController *pjlNavc = [[UINavigationController alloc]initWithRootViewController:pjlDictorArryToJsonVC];
        
        UIViewController *superVc = _superView;
        
        [superVc presentViewController:pjlNavc animated:YES completion:^{
            
        }];
        
    }
    
    if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"查看详细信息"]){
        
        NSString *jsonSting = @"";
        
        if([self.superModelID isKindOfClass:[PJLParamModel class]]){
            
            PJLParamModel *getModel = self.superModelID;
            
            jsonSting = getModel.pjlValue;
            
        }
        
        if([self.superModelID isKindOfClass:[PJLArryModel class]]){
            
            PJLArryModel *getModel = self.superModelID;
            
            jsonSting = getModel.contentStr;
            
        }
        
        if([self.superModelID isKindOfClass:[PJLNetConfigModel class]]){
            
            PJLNetConfigModel *getModel = self.superModelID;
            
            jsonSting = getModel.setBaseValueForHttpHeadFileDicJson;
            
        }
        
        
        UIAlertView *uiactionSheet = [[UIAlertView alloc]initWithTitle:@"下面就是你的数据"
                                                               message:jsonSting
                                                              delegate:nil
                                                     cancelButtonTitle:@"确定"
                                                     otherButtonTitles: nil];
        
        [uiactionSheet show];
        
    }
    
}

@end
