//
//  PJLUilityModel.m
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/1/17.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import "PJLUilityModel.h"

@implementation PJLUilityModel

- (id)init{
    
    self = [super init];
    if(self){
        
    }
    return self;
    
}

-(NSString *)convertToJsonData:(id)objectID{
    
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:objectID
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    NSString *jsonString;
    
    if (!jsonData) {
        
        NSLog(@"%@",error);
        
    }else{
        
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    return jsonString;
}

@end
