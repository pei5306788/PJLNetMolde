//
//  PJLUilityModel.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/1/17.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PJLUilityModel : NSObject

-(NSString *)convertToJsonData:(id)objectID;

@end
