//
//  PJLActionSheetWay.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2018/1/18.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PJLActionSheetWay : NSObject

@property (nonatomic) id  superView;

@property (nonatomic) id  superModelID;

- (void)createActionSheetView;

@end
