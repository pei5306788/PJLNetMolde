//
//  PJLAPI.h
//  PJLNetRequestDemo
//
//  Created by 裴建兰 on 2022/6/20.
//  Copyright © 2022 PJL. All rights reserved.
//

#import <Foundation/Foundation.h>
#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

NS_ASSUME_NONNULL_BEGIN

@interface PJLAPI : NSObject

@end

NS_ASSUME_NONNULL_END
