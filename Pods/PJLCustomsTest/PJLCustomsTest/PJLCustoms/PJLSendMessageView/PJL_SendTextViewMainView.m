//
//  PJL_SendMessageMainView.m
//  PJLCustomsTest
//
//  Created by 裴建兰 on 2017/3/20.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJL_SendTextViewMainView.h"

@interface PJL_SendTextViewMainView(){

    
    

}

@end

@implementation PJL_SendTextViewMainView

- (id)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    if(self){
         _pjl_SendTextFieldView = [[PJL_SendTextFiled alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width * 2 /3, self.frame.size.height)];

        _pjl_SendTextFieldView.center = CGPointMake(self.frame.size.width/2.0, _pjl_SendTextFieldView.center.y);
        
        _selfOrginFrame = self.frame;
        
        [self addSubview:_pjl_SendTextFieldView];
        
        [_pjl_SendTextFieldView setBackgroundColor:[UIColor orangeColor]];
        
        
        __weak typeof(self) weakSelf = self;
        [_pjl_SendTextFieldView  addTextViewEvent:^(NSString *textStr) { //监听键盘存在的时候触发的事件
            
            [UIView beginAnimations:nil context:nil];
            weakSelf.frame = CGRectMake(weakSelf.selfOrginFrame.origin.x,
                                        weakSelf.selfOrginFrame.origin.y - (_pjl_SendTextFieldView.frame.size.height - _selfOrginFrame.size.height),
                                        weakSelf.frame.size.width,
                                        weakSelf.pjl_SendTextFieldView.frame.size.height);
            [UIView commitAnimations];
        }];

        
        [self setBackgroundColor:[UIColor greenColor]];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
