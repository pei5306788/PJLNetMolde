//
//  PJL_SendTextFiled.h
//  PJLCustomsTest
//
//  Created by 裴建兰 on 2017/3/20.
//  Copyright © 2017年 PJL. All rights reserved.
//
#import <UIKit/UIKit.h>
typedef void(^PJL_SendTextViewValueChange)(NSString *textStr);  //添加外面监听用的



@interface PJL_SendTextFiled : UITextView


@property (nonatomic)CGFloat maxWith; //最大的宽度 默认是初始化的宽度

@property (nonatomic)CGFloat minWith; //最小的宽度。默认是初始化宽度

@property (nonatomic)CGFloat maxHeight; //设置就是无限高

@property (nonatomic)CGFloat minHeight; //最小的高度，默认是初始化的高度


@property (nonatomic) PJL_SendTextViewValueChange pjl_sendTextViewChangeBlock;  //添加外面监听用的

/*
 监听内容变化
 */

- (void)addTextViewEvent:(PJL_SendTextViewValueChange )sendTextFieldBlock;

@end
