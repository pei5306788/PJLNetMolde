//
//  PJL_SendTextFiled.m
//  PJLCustomsTest
//
//  Created by 裴建兰 on 2017/3/20.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJL_SendTextFiled.h"
#import "PJLAttributeWay.h"

@interface PJL_SendTextFiled ()<UITextViewDelegate>{

}

@end
@implementation PJL_SendTextFiled


- (id)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    if(self){
    
        
        self.delegate = self;
    
        [self setFont:[UIFont systemFontOfSize:17.0]];
        
        _minWith = 0;
        
        _minHeight = self.frame.size.height;
        
        _maxWith = self.frame.size.width;
        
        _maxHeight = 0;
        
        self.layoutManager.allowsNonContiguousLayout = NO;
        
        self.scrollEnabled = NO;
        
    }
    return self;
}

/*
 监听内容变化
 */

- (void)addTextViewEvent:(PJL_SendTextViewValueChange )sendTextFieldBlock{

    
    _pjl_sendTextViewChangeBlock = sendTextFieldBlock;
    
}

- (void)textValueChage{
    
    CGSize getSize = CGSizeMake(_maxWith, MAXFLOAT);
    
    CGSize getFrameSize = [self sizeThatFits:getSize];
    
    CGRect getFrame = CGRectMake(0, 0, getFrameSize.width, getFrameSize.height);
    
    
    if(_maxHeight != 0){ //如果设置的最大的指的吧
    
        if(getFrame.size.height > _maxHeight){
        
            
            getFrame.size.height = _maxHeight;
        
        }
    }
    
    if(_minWith!= 0){ //如果设置最小的显示距离
    
        
        if(getFrame.size.width < _minWith){
            
            
            getFrame.size.width = _minWith;
            
        }

    
    }
    
    
    if(_minHeight != 0){
    
        if(getFrame.size.height <= _minHeight){
            
            
            getFrame.size.height = _minHeight;
            
        }

    
    }
    
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, _maxWith, getFrame.size.height);
    NSLog(@"*****%@****%@",NSStringFromCGRect(self.frame),NSStringFromCGRect(getFrame));
    if(_pjl_sendTextViewChangeBlock != nil){
        _pjl_sendTextViewChangeBlock(self.text);
    }

}

- (void)textViewDidChange:(UITextView *)textView{

    [self textValueChage];
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
