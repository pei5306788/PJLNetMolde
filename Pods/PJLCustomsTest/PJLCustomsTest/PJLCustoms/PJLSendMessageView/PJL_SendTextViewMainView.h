//
//  PJL_SendMessageMainView.h
//  PJLCustomsTest
//
//  Created by 裴建兰 on 2017/3/20.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PJL_SendTextFiled.h"
@interface PJL_SendTextViewMainView : UIView

@property (nonatomic,strong)PJL_SendTextFiled *pjl_SendTextFieldView;

@property (nonatomic)CGRect selfOrginFrame;

@end
