//
//  PJLAttributeWay.h
//  PJLCustomsTest
//
//  Created by pjl on 2017/2/28.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PJLLabelMembers_1.h" //设计模型的Members
#import "PJLNSTextAttachment.h" //记录图片的相关参数
@interface PJLAttributeWay : NSObject



/***********************UILablel,textView,textField*****************************************/
/*
 数组里面就是PJLLabelMembers_1 模型。
 注意  NSMutableParagraphStyle 这个属性在一个段落中只有第一个生效，字体颜色可以在同一段落生成
 
 */

- (NSAttributedString *)setModel:(NSArray *)PJLLabelMembersArry; //自适应模型

/****
 根据参数拼装参数
 eachMember  PJLLabelMembers_1  模型
 defaultStyle 默认风格可以穿nil。 优先读模型里面的Style
 
 ****/

- (NSMutableAttributedString *)setEach:(PJLLabelMembers_1 *)eachMember
                           withDefault:(NSMutableParagraphStyle *)defaultStyle;


/*
 获取相对的高度 
 */

- (CGRect)PJLANSAttributedStringWith:(CGFloat)width
                      WithString:(NSAttributedString *)string;

/*
高度固定，宽度变量
*/
- (CGRect)PJLANSAttributedStringHeight:(CGFloat)Height
                            WithString:(NSAttributedString *)string;

/*
 内容里面涉及到自定义图片 (没验证) sizeThatFits
 */

- (CGSize)setLabelOrTextViewID:(id)LabelOrTextView
                  setFrameSize:(CGSize)setSize;

- (CGSize)setLabelOrTextViewID:(id)LabelOrTextView
                  setFrameSize:(CGSize)setSize
          withNeedSetFrameKind:(NSInteger)paramKind; //0  是宽度更具获得实际大小自适应，1 是宽度就是传的宽度

/*****************************************************************************************/

//获取对象的所有属性
- (NSArray *)getAllProperties:(NSObject *)model;

//Model 到字典
- (NSDictionary *)properties_aps:(NSObject *)model;

/*!
 
 * @brief 把格式化的JSON格式的字符串转换成字典
 
 * @param jsonString JSON格式的字符串
 
 * @return 返回字典
 
 */

- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

/*
 字典转json
 */

- (NSString*)dictionaryToJson:(NSDictionary *)dic;


/*
 聊天发送内容需要转类型发送的内容是json字符串 接受到自己处理
 */
- (NSString *)PJLChartModelToJson:(NSAttributedString *)attributeString;

/*
 字典转模型
 */

- (PJLLabelMembers_1 *)dicToModel:(NSDictionary *)dic;


@end
