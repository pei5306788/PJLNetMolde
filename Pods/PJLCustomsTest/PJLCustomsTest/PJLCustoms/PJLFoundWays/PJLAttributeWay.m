//
//  PJLAttributeWay.m
//  PJLCustomsTest
//
//  Created by pjl on 2017/2/28.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLAttributeWay.h"
#import <objc/runtime.h>
@implementation PJLAttributeWay

/*
 数组里面就是PJLLabelMembers_1 模型。
 注意  NSMutableParagraphStyle 这个属性在一个段落中只有第一个生效，字体颜色可以在同一段落生成
 */

- (NSAttributedString *)setModel:(NSArray *)PJLLabelMembersArry{
    
    NSMutableAttributedString *detailStr = [[NSMutableAttributedString alloc]init];
    
    for(PJLLabelMembers_1 *eachMember in PJLLabelMembersArry){
        
        NSMutableParagraphStyle *defaultStyle;
        
        if(eachMember.DeFaultIndex >= 0 && eachMember.DeFaultIndex < PJLLabelMembersArry.count){
            
            PJLLabelMembers_1 *modelMember = PJLLabelMembersArry[eachMember.DeFaultIndex];
            
            defaultStyle = modelMember.pjlNSMutableParagraphStyle;
            
        }
        
        [detailStr appendAttributedString:[self setEach:eachMember withDefault:defaultStyle]];
    }
    
    return detailStr;
    
}

/****
 根据参数拼装参数
 ****/
- (NSMutableAttributedString *)setEach:(PJLLabelMembers_1 *)eachMember
                           withDefault:(NSMutableParagraphStyle *)defaultStyle{
    
    
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:eachMember.contentStr];
    
    [str addAttribute:NSForegroundColorAttributeName value:eachMember.contentColor
                range:NSMakeRange(0,eachMember.contentStr.length)];
    
    [str addAttribute:NSFontAttributeName value:eachMember.contentFont
                range:NSMakeRange(0, eachMember.contentStr.length)];
    
    if(eachMember.imageName.length != 0 ){
     
        PJLNSTextAttachment *getImageTextAttachment = [[PJLNSTextAttachment alloc]init];
        
        getImageTextAttachment.image = [UIImage imageNamed:eachMember.imageName];
        getImageTextAttachment.imageName = eachMember.imageName;
        
        if(eachMember.imageFrame.size.height == 0){
            CGFloat with = 0;
            with =  [eachMember.contentFont pointSize] * (getImageTextAttachment.image.size.width/ getImageTextAttachment.image.size.height);
            getImageTextAttachment.bounds = CGRectMake(0, 0, with,[eachMember.contentFont pointSize]);
            getImageTextAttachment.imageFrame = getImageTextAttachment.bounds;
        }else{
            
            getImageTextAttachment.bounds = eachMember.imageFrame;
            getImageTextAttachment.imageFrame = eachMember.imageFrame;
        }
        
        
        NSAttributedString *string = [NSAttributedString attributedStringWithAttachment:getImageTextAttachment];

        [str appendAttributedString:string];
    }
    
    if(eachMember.imagePath.length != 0){
    
        PJLNSTextAttachment *getImageTextAttachment = [[PJLNSTextAttachment alloc]init];
        getImageTextAttachment.image = [UIImage imageWithContentsOfFile:eachMember.imagePath];
        getImageTextAttachment.imageName = eachMember.imagePath;
      
        if(eachMember.imageFrame.size.height == 0){
            CGFloat with = 0;
            with =  [eachMember.contentFont pointSize] * (getImageTextAttachment.image.size.width/ getImageTextAttachment.image.size.height);
            getImageTextAttachment.bounds = CGRectMake(0, 0, with,[eachMember.contentFont pointSize]);
            getImageTextAttachment.imageFrame = getImageTextAttachment.bounds;
        }else{
            
            getImageTextAttachment.bounds = eachMember.imageFrame;
            getImageTextAttachment.imageFrame = eachMember.imageFrame;
        }
       
        NSAttributedString *string = [NSAttributedString attributedStringWithAttachment:getImageTextAttachment];
        [str appendAttributedString:string];

    }
    
    
    if(eachMember.pjlNSMutableParagraphStyle != nil){ //优先读取自己的参数
        
        [str addAttribute:NSParagraphStyleAttributeName
                    value:eachMember.pjlNSMutableParagraphStyle
                    range:NSMakeRange(0, eachMember.contentStr.length)];
    }else if(defaultStyle != nil){
        [str addAttribute:NSParagraphStyleAttributeName
                    value:eachMember.pjlNSMutableParagraphStyle
                    range:NSMakeRange(0, eachMember.contentStr.length)];
    }
    
    return str;
}

/*
 获取相对的高度
*/

- (CGRect)PJLANSAttributedStringWith:(CGFloat)width
                      WithString:(NSAttributedString *)string {
    CGRect frame = [string boundingRectWithSize:CGSizeMake(width, FLT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) context:nil];
    //NSLog(@"2:%@", NSStringFromCGRect(frame));
    
    return frame;
}

/*
 高度固定，宽度变量
 */
- (CGRect)PJLANSAttributedStringHeight:(CGFloat)Height
                            WithString:(NSAttributedString *)string{
    CGRect frame = [string boundingRectWithSize:CGSizeMake(FLT_MAX, Height)
                                        options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) context:nil];
    NSLog(@"2:%@", NSStringFromCGRect(frame));
    
    return frame;
}

/*
 内容里面涉及到自定义图片
 */

- (CGSize)setLabelOrTextViewID:(id)LabelOrTextView
                      setFrameSize:(CGSize)setSize{

    if([LabelOrTextView isKindOfClass:[UILabel class]] || [LabelOrTextView isKindOfClass:[UITextView class]]){
    
      return   [LabelOrTextView sizeThatFits:setSize];
    
    }
    
    
    return CGSizeMake(0, 0);
    

}
- (CGSize)setLabelOrTextViewID:(id)LabelOrTextView
                  setFrameSize:(CGSize)setSize
          withNeedSetFrameKind:(NSInteger)paramKind{

    CGSize returnSize = [self setLabelOrTextViewID:LabelOrTextView setFrameSize:setSize];

    UIView *setFrameWay = LabelOrTextView;
    if(paramKind == 0){
    
        
        setFrameWay.frame = CGRectMake(setFrameWay.frame.origin.x, setFrameWay.frame.origin.y, returnSize.width, returnSize.height);
        
    
    }
    
    if(paramKind == 1){
    
          setFrameWay.frame = CGRectMake(setFrameWay.frame.origin.x, setFrameWay.frame.origin.y, setSize.width, returnSize.height);
    }
    

    return  returnSize;
}



/*******************************聊天涉及的方法 ***************************************/


//获取对象的所有属性
- (NSArray *)getAllProperties:(NSObject *)model
{
     u_int count;
    objc_property_t *properties  =class_copyPropertyList([model class], &count);
    NSMutableArray *propertiesArray = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i<count; i++)
    {
        const char* propertyName =property_getName(properties[i]);
        [propertiesArray addObject: [NSString stringWithUTF8String: propertyName]];
    }
    free(properties);
    return propertiesArray;
}

//Model 到字典
- (NSDictionary *)properties_aps:(NSObject *)model
{
    
    NSMutableDictionary *props = [NSMutableDictionary dictionary];
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([model class], &outCount);
    for (i = 0; i<outCount; i++)
    {
        objc_property_t property = properties[i];
        const char* char_f =property_getName(property);
        NSString *propertyName = [NSString stringWithUTF8String:char_f];
     
        id propertyValue = [model valueForKey:(NSString *)propertyName];

        if (propertyValue)
            
            [props setObject:[NSString stringWithFormat:@"%@",propertyValue] forKey:propertyName];
        
    }
    free(properties);
    return props;
}

/*!
 
 * @brief 把格式化的JSON格式的字符串转换成字典
 
 * @param jsonString JSON格式的字符串
 
 * @return 返回字典
 
 */

- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    
    if (jsonString == nil) {
        
        return nil;
        
        
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *err;
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                         
                                                        options:NSJSONReadingMutableContainers
                         
                                                          error:&err];
    if(err) {
        
        NSLog(@"json解析失败：%@",err);
        
        return nil;
        
    }
    
    return dic;
    
}


/*
 字典转json
 */

- (NSString*)dictionaryToJson:(NSDictionary *)dic

{
    NSError *parseError = nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&parseError];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
}


/*
  聊天发送内容需要转类型
 */
- (NSString *)PJLChartModelToJson:(NSAttributedString *)attributeString{
    
    NSLog(@"***%lu",(unsigned long)attributeString.string.length);
    
    NSInteger stringLength = (unsigned long)attributeString.string.length;
    
    NSInteger i = 0;
    NSMutableArray *dataList = [[NSMutableArray alloc]init];
    while ( i != stringLength) {
       
        NSRange range;
        NSDictionary *getDic =  [attributeString  attributesAtIndex:i effectiveRange:&range];
        
        NSString *jsonStr  =  [self setModelStr:[attributeString.string substringWithRange:range]
                                                 withDic:getDic];
        [dataList addObject:jsonStr];
        i = range.location + range.length;
    }
    
    
    NSDictionary *getModelDic = [NSDictionary dictionaryWithObjectsAndKeys:dataList,@"PJLLabelMembers_1List", nil];
    
    NSData *data=[NSJSONSerialization dataWithJSONObject:getModelDic
                                                 options:NSJSONWritingPrettyPrinted
                                                   error:nil];
    NSString *jsonStr=[[NSString alloc]initWithData:data
                                           encoding:NSUTF8StringEncoding];
    return jsonStr;

}


- (NSString *)setModelStr:(NSString *)paramStr
                           withDic:(NSDictionary *)paramDic{
    PJLLabelMembers_1 *modelMember = [[PJLLabelMembers_1 alloc]init];
    
    if([[paramDic objectForKey:@"NSAttachment"] isKindOfClass:[PJLNSTextAttachment class]] ){
    
        PJLNSTextAttachment *pjlTextAttachment = [paramDic objectForKey:@"NSAttachment"];
        modelMember.imageName = pjlTextAttachment.imageName;
        modelMember.imagePath = pjlTextAttachment.imagePath;
        modelMember.imageFrame = pjlTextAttachment.bounds;
        
    }else{
    
        modelMember.contentFont = [paramDic objectForKey:@"NSFont"];
        modelMember.contentColor = [paramDic objectForKey:@"NSColor"];
//        modelMember.pjlNSMutableParagraphStyle = [paramDic objectForKey:@"NSParagraphStyle"];
        modelMember.contentStr = paramStr;
        modelMember.pjlNSMutableParagraphStyleJsonStr = [self dictionaryToJson:[self properties_aps: modelMember.pjlNSMutableParagraphStyle]];
      
    }
    
    modelMember.pjlNSMutableParagraphStyle = [paramDic objectForKey:@"NSParagraphStyle"];
    
    
     NSDictionary *restltDic = [self properties_aps:modelMember];
    
     NSString *restultStr  = [self dictionaryToJson:restltDic];
    return restultStr;
}

- (PJLLabelMembers_1 *)dicToModel:(NSDictionary *)dic{

    PJLLabelMembers_1 *returnModel = [[PJLLabelMembers_1 alloc]init];
    
    unsigned int useCount = 0,
                outCount;
    
    Ivar *ivars = class_copyIvarList([PJLLabelMembers_1 class], &outCount);
    
    for (const Ivar *p = ivars; p < ivars + outCount; p++) {
        Ivar const ivar = *p;
        
        //获取变量名
        NSString *varName = [NSString stringWithUTF8String:ivar_getName(ivar)];
        
        //获取变量值
        id varValue = [returnModel valueForKey:varName];
        
        //如果是直接包含此名称的
        if ([dic.allKeys containsObject:varName]) {
            varValue = [dic objectForKey:varName];
            useCount++;
        }else{
            NSString *tmp_varName = [self removeFirstUnderlined:varName];
            if ([dic.allKeys containsObject:tmp_varName]) {
                varValue = [dic objectForKey:tmp_varName];
                useCount++;
            }
        }
        
        [returnModel setValue:varValue forKey:varName];
    }
    

    return returnModel;
}


/*
 *  移除掉第一个下划线
 */
-(NSString *)removeFirstUnderlined:(NSString *)str
{
    if ([[str substringToIndex:1] isEqual:@"_"]) {
        return [str substringFromIndex:1];
    }
    return str;
}

///*
// *  获取所有属性
// */
//- (NSArray*)propertyKeys:(id)saveModel
//{
//    unsigned int outCount, i;
//    objc_property_t *properties = class_copyPropertyList([self class], &outCount);
//    NSMutableArray *keys = [[NSMutableArray alloc] initWithCapacity:outCount];
//    for (i = 0; i < outCount; i++) {
//        objc_property_t property = properties[i];
//        NSString *propertyName = [[NSString alloc] initWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
//        [keys addObject:propertyName];
//    }
//    free(properties);
//    return keys;
//}

//- (NSMutableParagraphStyle *)modelParagraphStyle:(NSString *)getParagraphString{
//
//    NSMutableParagraphStyle *returnModelStyle;
//    
//    
//    return returnModelStyle;
//}


@end
