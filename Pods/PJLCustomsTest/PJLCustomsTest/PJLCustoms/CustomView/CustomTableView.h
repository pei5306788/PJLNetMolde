//
//  CustomTableView.h
//  QinXin
//
//  Created by rs-mac-mini on 15/6/4.
//  Copyright (c) 2015年 rs-mac-mini. All rights reserved.
//

#import <UIKit/UIKit.h>


//#import "PJLNODataView.h"

typedef void(^PJLTouchBegub)(NSSet<UITouch *>  *touches,UIEvent *event); //touchBegin

typedef void(^PJLTouchMove)(NSSet<UITouch *>  *touches,UIEvent *event); //touchMove

typedef void(^PJLTouchEnd)(NSSet<UITouch *>  *touches,UIEvent *event); //touchEnd

// 下拉更新数据用到的block
typedef void(^PulltorefreshBlock)(NSInteger kind,id model);

// 上拉 加载数据 用到 的block
typedef void(^PullUpLoadingBlock)(NSInteger page,NSInteger limit);

//这个就是监听 tableView WillDisplayCell 这个方法用的
typedef void(^WillDisplayCellBlock)(NSIndexPath *indexPath,id model);

@interface CustomTableView : UITableView


@property (nonatomic) PulltorefreshBlock pulltorefreshBlock;


@property (nonatomic) PullUpLoadingBlock pullUpLoadingBlock;

@property (nonatomic) WillDisplayCellBlock willDisplayCellBlock;

@property (nonatomic) PJLTouchBegub pjlTouchBegin;

@property (nonatomic) PJLTouchMove pjlTouchMove;

@property (nonatomic) PJLTouchEnd pjlTouchEnd;


@property (nonatomic)NSInteger pageIndex;

@property (nonatomic)NSInteger eachNumber;
/*
 参数:paramStatus: 0 //上拉下拉都要
                  1 //只要上拉
                  2 // 只要下拉
 
 headButtonClip
 footButtonClip
 参数:otherIndex 扩展类型
 */
- (void)setRefresh:(NSInteger)paramStatus
    withOtherIndex:(NSInteger)otherIndex;

-(void)headButtonClip;
-(void)footButtonClip;
@property (nonatomic,strong)NSString *noDataImageStr; //没数据的显示图片

@property (nonatomic,strong)id noDataView; //没数据的图片

@property (nonatomic,strong) id selectId; //触发的事件id

@property (nonatomic) NSInteger tableViewRefreshStatus; //0 没事 ,1 正在刷新,2 结束事件


- (void)getDataError; //网络错误状态

- (void)getData;

- (void)endFreshNumbers:(NSInteger)paramNumbers withAllNuber:(NSInteger)allNumber; //获取到的数据量

- (void)endFreshNumbers:(NSInteger)paramNumbers;

- (void)setTouchActionWith:(PJLTouchBegub)begin
                  withMove:(PJLTouchMove)move
                   withEnd:(PJLTouchEnd)end;

- (void)mj_headerBeginRefreshing;

- (void)mj_footerBeginRefreshing;


/**
 上面是老方法，保存的 但是新的方法 在下面
 */


/**
 新的方法
 
 *isNeedPulltorefresh //是否需要下拉更新
 *param refreshString 更新等待的时候提示语言
 *param tintColor 更新的显示的颜色
 *param return 主要是 适配iOS8.0，9.0 的
 */

- (UIRefreshControl *)pjlNeedPulltorefreshKind:(NSInteger)kind
                            withPull_to_refresh:(BOOL)isNeedPulltorefresh
                            withAttentionString:(NSMutableAttributedString *)refreshString
                                    withColor:(UIColor *)tintColor
                                  withBlock:(PulltorefreshBlock)block;


@property (nonatomic) BOOL isLoading;// 正在加载中

@property (nonatomic) NSInteger recordPage;//记录 第几页了哇

@property (nonatomic) NSInteger limit; //每页多少个

@property (nonatomic) NSInteger list_count; //目前加载多少个数据了哇

@property (nonatomic) BOOL isAllFinsh; //全都完成了哇

/**
 *param maxRefreshNumber 距离多少个 触法更新方法
 *param dataList 加载内容多少用户判断是否要加载的
 *param page 从第几页开始
 *pparam limit  每页多少个
 */
- (void)setPulluploading:(BOOL)isNeed
                withKind:(NSInteger)kind
     withToRefreshNumber:(NSInteger)maxRefreshNumber
             withSection:(NSInteger)section
                withPage:(NSInteger)page
                withLimit:(NSInteger)limit
withPullUpLoadingBlock:(PullUpLoadingBlock)block;


/**
 * 插入的数据模型 一定要在 数据请求完并且添加完成 不然会闪退的
 * param allNumber 总共有多少数据
 * param addNumber 变化的数量
 * param section 第几个section 插入
 */
- (void)pjlReloadAllNumber:(NSInteger)allNumber
                 addOriginnumber:(NSInteger)Originnumber
                 withSection:(NSInteger)section
withUITableViewRowAnimation:(UITableViewRowAnimation)tableViewRowAnimation;



@end
