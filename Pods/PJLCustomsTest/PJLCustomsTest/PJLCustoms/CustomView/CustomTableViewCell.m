//
//  CustomTableViewCell.m
//  QinXin
//
//  Created by rs-mac-mini on 15/5/30.
//  Copyright (c) 2015年 rs-mac-mini. All rights reserved.
//

#import "CustomTableViewCell.h"

@interface CustomTableViewCell(){
    
    
    
}
@end

@implementation CustomTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self)
    {
        [self.layer setMasksToBounds:YES];
        _topView = [[CustomView alloc]init];
        _topView.userInteractionEnabled = NO;
        [self.contentView insertSubview:_topView atIndex:100];
        
        _downView = [[CustomView alloc]init];
        _downView.userInteractionEnabled = NO;
        [self.contentView insertSubview:_downView atIndex:100];
        
        _topView.translatesAutoresizingMaskIntoConstraints = false;
        _downView.translatesAutoresizingMaskIntoConstraints = false;
        
        NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:_topView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];


//
//
          NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:_downView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];

//        [_downView addConstraint:bottom];

        [top setActive:true];
//
        [bottom setActive:true];
//
        
        
    }
    return self;
}

- (void)setDic:(NSDictionary *)paramDic
{

}

/**
 *自定义线密封性的，这个是分隔用的
 *@param topColor 上面线的颜色
 *@param bottomColor 下面颜色
 *@param borderWidth  线的宽度
 *@param topLeft  上左边的距离
 *@param topRight 上右边的距离
 *@param bottomLeft 下左边的距离
 *@param bottomRight 下右边的距离
 **/
- (void)setTopCplor:(UIColor *)topColor
     setBottomColor:(UIColor *)bottomColor
     setBorderWidth:(CGFloat )borderWidth
         setTopLeft:(CGFloat)topLeft
        setTopRight:(CGFloat)topRight
      setBottomLeft:(CGFloat)bottomLeft
     setBottomRight:(CGFloat)bottomRight{
    
    
    [_topView setBackgroundColor:[UIColor clearColor]];
    [_downView setBackgroundColor:[UIColor clearColor]];
    if(topColor!= nil){
        [_topView setBackgroundColor:topColor];
        
        
        NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:_topView
                                                                attribute:NSLayoutAttributeLeft
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.contentView
                                                                attribute:NSLayoutAttributeLeft
                                                               multiplier:1.0
                                                                 constant:topLeft];
        
        
        NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem:_topView
                                                                 attribute:NSLayoutAttributeRight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeRight
                                                                multiplier:1.0
                                                                  constant:topRight];
        
        NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:_topView
                                                               attribute:NSLayoutAttributeTop
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.contentView
                                                               attribute:NSLayoutAttributeTop
                                                              multiplier:1.0
                                                                constant:0];
        
        NSLayoutConstraint *hegit = [NSLayoutConstraint constraintWithItem:_topView
                                                                 attribute:NSLayoutAttributeHeight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:nil
                                                                 attribute:NSLayoutAttributeNotAnAttribute
                                                                multiplier:1.0
                                                                  constant:borderWidth];
        
        
        [hegit setActive:true];
        [left setActive:true];
        [right setActive:true];
        [top setActive:true];
        
    }
    
    if(bottomColor!= nil){
        
        [_downView setBackgroundColor:bottomColor];
        
        NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:_downView
                                                                attribute:NSLayoutAttributeLeft
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.contentView
                                                                attribute:NSLayoutAttributeLeft multiplier:1.0
                                                                 constant:bottomLeft];
        
        
        NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem:_downView
                                                                 attribute:NSLayoutAttributeRight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeRight
                                                                multiplier:1.0
                                                                  constant:bottomRight];
        
        
        [left setActive:true];
        [right setActive:true];
        
        NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:_downView
                                                                  attribute:NSLayoutAttributeBottom
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self.contentView
                                                                  attribute:NSLayoutAttributeBottom
                                                                 multiplier:1.0
                                                                   constant:0];
        
        NSLayoutConstraint *hegit = [NSLayoutConstraint constraintWithItem:_downView
                                                                 attribute:NSLayoutAttributeHeight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:nil
                                                                 attribute:NSLayoutAttributeNotAnAttribute
                                                                multiplier:1.0
                                                                  constant:borderWidth];
        [bottom setActive:true];
        [hegit setActive:true];
        
    }
    
}

/**
 *自定义线密封性的，这个是分隔用的
 *@param topColor 上面线的颜色
 *@param bottomColor 下面颜色
 *@param borderWidth  线的宽度
 **/
- (void)setTopCplor:(UIColor *)topColor
    setBottomColor:(UIColor *)bottomColor
     setBorderWidth:(CGFloat )borderWidth
{
  
//    [self setTopCplor:topColor setBottomColor:bottomColor setBorderWidth:borderWidth];
    
    [self setTopCplor:topColor
       setBottomColor:bottomColor
       setBorderWidth:borderWidth
           setTopLeft:0
          setTopRight:0
        setBottomLeft:0
       setBottomRight:0];
    
}




- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
