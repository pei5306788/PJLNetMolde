//
//  PJLActionSheet.h
//  pjlWork1
//
//  Created by 裴建兰 on 8/14/15.
//  Copyright (c) 2015 裴建兰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PJLActionSheet : UIActionSheet

@property(nonatomic,strong)NSArray *dataList;

@end
