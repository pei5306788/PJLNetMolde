//
//  PJLURLConnection.h
//  QinXin
//
//  Created by rs-mac-mini on 15/6/1.
//  Copyright (c) 2015年 rs-mac-mini. All rights reserved.
//
#import "CustomImageView.h"
#import <Foundation/Foundation.h>

@interface PJLURLConnection : NSURLConnection

@property (nonatomic,strong)CustomImageView *needToAddImageView;

@property (nonatomic,strong)NSMutableData *getData;

@property (nonatomic,strong)NSString *PJLUrl;

@end
