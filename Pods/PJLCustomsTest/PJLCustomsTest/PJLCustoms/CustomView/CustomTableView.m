//
//  CustomTableView.m
//  QinXin
//
//  Created by rs-mac-mini on 15/6/4.
//  Copyright (c) 2015年 rs-mac-mini. All rights reserved.
//




#import "CustomTableView.h"

#import "MJRefresh.h"




@interface CustomTableView(){

    NSInteger _paramStatus;
    
    NSInteger _otherIndex;
 
    
 

 
}

@property (nonatomic)UIRefreshControl *modelControl;

@end

@implementation CustomTableView

- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if(self){
        
        if(@available(iOS 11.0, *)){
            self.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            self.estimatedRowHeight = 0;
            self.estimatedSectionHeaderHeight = 0;
            self.estimatedSectionFooterHeight = 0;
        }
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setRefresh:(NSInteger)paramStatus
    withOtherIndex:(NSInteger)otherIndex{

    _paramStatus = paramStatus;
    
    _otherIndex = otherIndex;
    if(_selectId == nil){
    
        NSLog(@"选中的vc没有设置");
        return;
    }
    
    
    
    
    if(paramStatus == 0){ //上下都要
    
        self.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:_selectId
                                                          refreshingAction:@selector(headButtonClip)];
        
        self.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:_selectId
                                                              refreshingAction:@selector(footButtonClip)];
        
    }
    
    if(paramStatus == 1){
    
        self.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:_selectId
                                                          refreshingAction:@selector(headButtonClip)];
    }

    if(paramStatus == 2){
    
        self.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:_selectId
                                                              refreshingAction:@selector(footButtonClip)];

    
    }
    
    
}

- (void)mj_headerBeginRefreshing{
    
    [self.mj_header beginRefreshing];
    
}

- (void)mj_footerBeginRefreshing{
    
    [self.mj_footer beginRefreshing];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

// Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cerDefine = @"CustomTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cerDefine];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cerDefine];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    
    return cell;
}

-(void)headButtonClip{
    [_noDataView setHidden:YES];
    [self setHidden:NO];
    _pageIndex = 1;
    _eachNumber = 10;
    [self.mj_footer setHidden:NO];
}
-(void)footButtonClip{
   
    _pageIndex = _pageIndex + 1;
    
    _eachNumber = 10;

    
}

- (void)endFreshNumbers:(NSInteger)paramNumbers withAllNuber:(NSInteger)allNumber{

    if(_paramStatus == 0){
        
        [self.mj_header endRefreshing];
        if(allNumber > 0 &&  paramNumbers < _eachNumber ){ //数据不是为空，但已经到数据末尾了
            
            [self.mj_footer endRefreshingWithNoMoreData];
            
        }
        
        if(allNumber == 0){ //没获取到任何数据
        
            [self.mj_footer setHidden:YES];
        }
        
        if(allNumber > 0 && paramNumbers >= _eachNumber){ //数据不为空，后面应该还有数据
        
            [self.mj_footer endRefreshing];
        }
        
    }
    
    if(_paramStatus == 1){
    
        [self.mj_header endRefreshing];
        
        if(allNumber == 0){ //没获取到任何数据
            
            
        }

    }

    
    if(_paramStatus == 2){
    
    
        if(allNumber > 0 &&  paramNumbers < _eachNumber ){ //数据不是为空，但已经到数据末尾了
            
            [self.mj_footer endRefreshingWithNoMoreData];
            
        }
        
        if(allNumber == 0){ //没获取到任何数据
            
            [self.mj_footer setHidden:YES];
        }
        
        if(allNumber > 0 && paramNumbers >= _eachNumber){ //数据不为空，后面应该还有数据
            
            [self.mj_footer endRefreshing];
        }

        
    }
    [self getData];
}

- (void)getDataError{

  
    [self.mj_footer endRefreshing];
    [self.mj_header endRefreshing];
    [self.mj_footer setHidden:YES];
    
    self.tableFooterView = _noDataView;

}

- (void)getData{

    self.tableFooterView = nil;

}


- (void)endFreshNumbers:(NSInteger)paramNumbers{

      [self.mj_header endRefreshing];
      [self.mj_footer endRefreshing];
}

- (void)setTouchActionWith:(PJLTouchBegub)begin
                  withMove:(PJLTouchMove)move
                   withEnd:(PJLTouchEnd)end{

    _pjlTouchBegin = begin;
    
    _pjlTouchMove = move;
    
    _pjlTouchEnd = end;

}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    [super touchesBegan:touches withEvent:event];
  
    if(_pjlTouchBegin != nil){
    
        _pjlTouchBegin(touches,event);
    }
  
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    [super touchesMoved:touches withEvent:event];
    
    if(_pjlTouchMove != nil){
    
        _pjlTouchMove(touches,event);
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    [super touchesEnded:touches withEvent:event];
    
    if(_pjlTouchEnd != nil){
    
        _pjlTouchEnd(touches,event);
    }
}


/**
 新的方法
 
 *isNeedPulltorefresh //是否需要下拉更新
 *param refreshString 更新等待的时候提示语言
 *param tintColor 更新的显示的颜色
 *param return 主要是 适配iOS8.0，9.0 的
 */

- (UIRefreshControl *)pjlNeedPulltorefreshKind:(NSInteger)kind
          withPull_to_refresh:(BOOL)isNeedPulltorefresh
          withAttentionString:(NSMutableAttributedString *)refreshString
                    withColor:(UIColor *)tintColor
                    withBlock:(PulltorefreshBlock)block
{
    
    if(!isNeedPulltorefresh){
        
        if(_modelControl != nil){
            
            if([_modelControl isRefreshing]){
                //如果正在更新  就不走了防止多次
                
                [_modelControl endRefreshing];
            }
            
            
            [_modelControl removeFromSuperview];
        }
        
        return nil;
    }
    
    if(_modelControl  == nil){
        
        _modelControl = [[UIRefreshControl alloc]init];
        
    }
    
    _modelControl.tintColor = tintColor;
    
    _modelControl.attributedTitle = refreshString;
    
    if(@available(iOS 10.0,*)){
        //iOS 10 以上的
        self.refreshControl = _modelControl;
    }else{
        //iOS 6.0 到 iOS 10.0
        [self addSubview:_modelControl];
    
    }
    
    _pulltorefreshBlock = block;
    
    [_modelControl addTarget:self
                     action:@selector(refreshClip:)
           forControlEvents:UIControlEventValueChanged];
    
    
    return _modelControl;

}

#pragma mark refreshClip

- (void)refreshClip:(UIRefreshControl *)refreshControl{
  
    if(_pulltorefreshBlock != nil){
        
        self.isAllFinsh = NO;
        
        if(_pulltorefreshBlock != nil){
            _pulltorefreshBlock(0,@"");
            
        }
        
    }
    
}


/**
 *param maxRefreshNumber 距离多少个 触法更新方法
 *param dataList 加载内容多少用户判断是否要加载的
 *param page 从第几页开始
 *pparam limit  每页多少个
 */
- (void)setPulluploading:(BOOL)isNeed
                withKind:(NSInteger)kind
     withToRefreshNumber:(NSInteger)maxRefreshNumber
             withSection:(NSInteger)section
                withPage:(NSInteger)page
                withLimit:(NSInteger)limit
    withPullUpLoadingBlock:(PullUpLoadingBlock)block
        
{
    
    _recordPage = page;
    
    self.limit = limit;
    
    if(!isNeed){
        
        if(_willDisplayCellBlock != nil){
            
            _willDisplayCellBlock = nil;
        }
        
        if(self.pullUpLoadingBlock != nil){
            
            self.pullUpLoadingBlock = nil;
        }
        
        return;
    }
    
    self.isAllFinsh = NO;
    
    __weak typeof(self) _weakSelf = self;
    [self set_1WillDisplayCellBlock:^(NSIndexPath *indexPath, id model) {
      
        //这个 是监听  距离最后一个还有多远的方法
        if(indexPath.section != section){
            
            return;
        }
        
        if(_weakSelf.modelControl != nil){
            
            if([_weakSelf.modelControl isRefreshing]){
                
                return;
                
            }
            
        }
        
      
        
        if(indexPath.row >= _weakSelf.list_count - maxRefreshNumber){
            
            //这边可以开始继续请求了
            if(_weakSelf.isLoading == NO && _weakSelf.isAllFinsh == NO){
                
                //如果可以继续请求 那就
                _weakSelf.isLoading = true;
                
                block(_weakSelf.recordPage,limit);
                
                _weakSelf.recordPage = _weakSelf.recordPage + 1;
                
            }
            
        }
        
        
    }];
    
}

//监听 willDisplayCell 方法
- (void)set_1WillDisplayCellBlock:(WillDisplayCellBlock)block{
    
    
    self.willDisplayCellBlock = block;
    
    
}





/**
 * 插入的数据模型 一定要在 数据请求完并且添加完成 不然会闪退的
 * param allNumber 总共有多少数据
 * param addNumber 变化的数量
 * param section 第几个section 插入
 */
- (void)pjlReloadAllNumber:(NSInteger)allNumber
                 addOriginnumber:(NSInteger)Originnumber
                 withSection:(NSInteger)section
 withUITableViewRowAnimation:(UITableViewRowAnimation)tableViewRowAnimation{
    
    
    
    NSInteger sectionRow = self.list_count;
    
    

    //目前已经有的数量
    
    if(allNumber < sectionRow){
        
        //出现删除
        
        NSInteger deleteNumber = sectionRow - allNumber ;
        
        NSMutableArray *ins = [NSMutableArray arrayWithCapacity:deleteNumber];
        
        //要删除的东西
        for(NSInteger i = sectionRow - deleteNumber ; i < sectionRow; i++){
    
               NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:section];
       
               [ins addObject:indexPath];
           }
        
        [self deleteRowsAtIndexPaths:ins withRowAnimation:(tableViewRowAnimation)];
        
    }else if(allNumber == sectionRow){
        //出现不增加也不减小
        
        [self reloadData]; //刷新下
        
        
        
    }else if(allNumber > sectionRow){
        
        NSInteger addNumber = allNumber - sectionRow;
        NSMutableArray *ins = [NSMutableArray arrayWithCapacity:addNumber];
        
        for(NSInteger i = sectionRow ; i < sectionRow + addNumber; i ++ ){
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:section];
    
            [ins addObject:indexPath];
            
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    //需要在主线程执行的代码
            [self insertRowsAtIndexPaths:ins withRowAnimation:tableViewRowAnimation];
        }];
        
    }
        
    if(_modelControl != nil){
        
        if( [_modelControl isRefreshing]){
        
            [_modelControl endRefreshing];
            
        }
    }
    
    self.list_count = allNumber;
    
    self.isLoading = NO;
}

@end
