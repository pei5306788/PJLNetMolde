//
//  CustomLabel.m
//  QinXin
//
//  Created by rs-mac-mini on 15/5/30.
//  Copyright (c) 2015年 rs-mac-mini. All rights reserved.
//

#import "CustomLabel.h"
//#import "ShareData.h"

@implementation CustomLabel
@synthesize verticalAlignment = verticalAlignment_;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setTextColor:[UIColor blackColor]];
        [self setFont:[UIFont systemFontOfSize:14.0]];
        self.verticalAlignment = VerticalAlignmentMiddle;
    }
    
    return self;
}
- (void)addCopy{
    
    [ self attachTapHandler];
}

-(void)copy:(id)sender

{
    
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    
    pboard.string = _pjlCopyString;
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self becomeFirstResponder];
}

//还需要针对复制的操作覆盖两个方法：

// 可以响应的方法

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender

{
    
    return (action == @selector(copy:));
    
}

-(void)attachTapHandler

{
    
    self.userInteractionEnabled = YES;  //用户交互的总开关
    
    UILongPressGestureRecognizer *pressGesture=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPress:)];
    [self addGestureRecognizer:pressGesture];
    
    
    
}

-(BOOL)canBecomeFirstResponder
{
    return YES;
}

-(void)handleLongPress:(UIGestureRecognizer*) recognizer

{
    
    
    
    //    UIMenuItem *copyLink = [[UIMenuItem alloc] initWithTitle:@"复制"
    //
    //                                                      action:@selector(copy:)];
    
    //    [[UIMenuController sharedMenuController] setMenuItems:[NSArray arrayWithObjects: nil]];
    
    [[UIMenuController sharedMenuController] setTargetRect:self.frame inView:self.superview];
    
    [[UIMenuController sharedMenuController] setMenuVisible:YES animated: YES];
    
}

- (void)setMessageAttenteionLabel
{
    
    [self setBackgroundColor:[UIColor redColor]];
    [self setFont:[UIFont systemFontOfSize:14.0]];
    [self.layer setCornerRadius:self.frame.size.width/2.0];
    [self.layer setMasksToBounds:YES];
    self.textAlignment = NSTextAlignmentCenter;
    [self setTextColor:[UIColor whiteColor]];
}

- (void)setSexLabelBackColor:(UIColor *)paramColor
{
    if(paramColor != nil)
    {
        [self setBackgroundColor:paramColor];
    }else{
        [self setBackgroundColor:[UIColor clearColor]];
    }
    [self setTextColor:[UIColor whiteColor]];
    [self.layer setCornerRadius:5.0];
    [self setFont:[UIFont systemFontOfSize:12.0]];
    [self.layer setMasksToBounds:YES];
    [self setTextAlignment:NSTextAlignmentCenter];
    
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
- (void)setVerticalAlignment:(VerticalAlignment)verticalAlignment {
    verticalAlignment_ = verticalAlignment;
    [self setNeedsDisplay];
}

- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines {
    CGRect textRect = [super textRectForBounds:bounds limitedToNumberOfLines:numberOfLines];
    switch (self.verticalAlignment) {
        case VerticalAlignmentTop:
            textRect.origin.y = bounds.origin.y;
            break;
        case VerticalAlignmentBottom:
            textRect.origin.y = bounds.origin.y + bounds.size.height - textRect.size.height;
            break;
        case VerticalAlignmentMiddle:
            // Fall through.
        default:
            textRect.origin.y = bounds.origin.y + (bounds.size.height - textRect.size.height) / 2.0;
    }
    return textRect;
}

-(void)drawTextInRect:(CGRect)requestedRect {
    CGRect actualRect = [self textRectForBounds:requestedRect limitedToNumberOfLines:self.numberOfLines];
    [super drawTextInRect:actualRect];
}


- (void)pjlSetTitleStr:(NSString *)paramSender
         setTitleColor:(UIColor *)titleColor
          settitleFont:(UIFont *)titleFont
         setContentStr:(NSString *)paramConstr
       setContentColor:(UIColor *)contentColor
        setContentFont:(UIFont *)contentFont
{
    
    NSString *getStr = [NSString stringWithFormat:@"%@%@",paramSender,paramConstr];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:getStr];
    [str addAttribute:NSForegroundColorAttributeName value:titleColor range:NSMakeRange(0,paramSender.length)];
    [str addAttribute:NSForegroundColorAttributeName value:contentColor range:NSMakeRange(paramSender.length,paramConstr.length)];
    //    [str addAttribute:NSForegroundColorAttributeName value:[UIColor greenColor] range:NSMakeRange(19,6)];
    [str addAttribute:NSFontAttributeName value:titleFont range:NSMakeRange(0, paramSender.length)];
    [str addAttribute:NSFontAttributeName value:contentFont range:NSMakeRange(paramSender.length, paramConstr.length)];
    //    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Courier-BoldOblique" size:30.0] range:NSMakeRange(19, 6)];
    self.attributedText = str;
}

- (void)pjlSetTitleStr:(NSString *)paramSender
         setTitleColor:(UIColor *)titleColor
          settitleFont:(UIFont *)titleFont
setTitleParagraphStyle:(NSMutableParagraphStyle *)titleParaghStyle
         setContentStr:(NSString *)paramConstr
       setContentColor:(UIColor *)contentColor
        setContentFont:(UIFont *)contentFont
setContentParagraphStyle:(NSMutableParagraphStyle *)contentParaghStyle
{
    
    NSString *getStr = [NSString stringWithFormat:@"%@%@",paramSender,paramConstr];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:getStr];
    [str addAttribute:NSForegroundColorAttributeName value:titleColor range:NSMakeRange(0,paramSender.length)];
    
    [str addAttribute:NSParagraphStyleAttributeName value:titleParaghStyle range:NSMakeRange(0,paramSender.length)];
    
    [str addAttribute:NSForegroundColorAttributeName value:contentColor range:NSMakeRange(paramSender.length,paramConstr.length)];
   
    [str addAttribute:NSFontAttributeName value:titleFont range:NSMakeRange(0, paramSender.length)];
    [str addAttribute:NSFontAttributeName value:contentFont range:NSMakeRange(paramSender.length, paramConstr.length)];
    [str addAttribute:NSParagraphStyleAttributeName value:contentParaghStyle range:NSMakeRange(paramSender.length, paramConstr.length)];
    
    self.attributedText = str;
}


- (void)pjlMoreInfo:(NSArray *)getArry
{
    NSString *getStr = @"";
    NSString *showStr = @"";
    
    for(PJLLableMember *pjlModel in getArry)
    {
        showStr = [NSString stringWithFormat:@"%@%@",showStr,pjlModel.lableText];
        
    }
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:showStr];
    for(PJLLableMember *pjlModel in getArry)
    {
        
        [str addAttribute:NSForegroundColorAttributeName value:pjlModel.labelColor range:NSMakeRange(getStr.length,pjlModel.lableText.length)];
        
        [str addAttribute:NSFontAttributeName value:pjlModel.labeFont range:NSMakeRange(getStr.length, pjlModel.lableText.length)];
        
        getStr = [NSString stringWithFormat:@"%@%@",getStr,pjlModel.lableText];
        
    }
    
    
    self.attributedText = str;
    
}

#pragma mark 格式化高度

- (void)setFormartHeight:(CGFloat)formartHeight
               setNumber:(NSInteger)paranIndex{
    
    
    [self setNumberOfLines:paranIndex];
    NSString *gettext;
    if(self.attributedText == nil)
    {
        gettext = self.text;
    }else
    {
        gettext = self.text;
    }
    if(gettext.length == 0){
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 10);
        
        return ;
    }
    CGSize sizeToFit = [gettext sizeWithFont:self.font
                           constrainedToSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    
    
    CGFloat hegiht = (sizeToFit.height/formartHeight) * formartHeight + ((NSInteger)sizeToFit.height%(NSInteger)formartHeight > 0 ? formartHeight : 0);
    
    if(hegiht > formartHeight * paranIndex && paranIndex != 0){
        hegiht = formartHeight * paranIndex;
    }
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, hegiht);
    
}


- (void)setFormartHeight:(CGFloat)formartHeight
               setNumber:(NSInteger)paranIndex
             setMaxWidth:(CGFloat)maxWidth
{
    
    
    [self setNumberOfLines:paranIndex];
    NSString *gettext;
    if(self.attributedText == nil)
    {
        gettext = self.text;
    }else
    {
        gettext = self.text;
    }
    
    if(_pjlModelStr!= nil){
        
        gettext = _pjlModelStr;
    }
    
    CGSize sizeToFit = [gettext sizeWithFont:self.font
                           constrainedToSize:CGSizeMake(maxWidth, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
    
    //    sizeToFit.width  = _pjlImageCount * 9 + sizeToFit.width;
    
    
    
    if(sizeToFit.width < maxWidth ){
        
        maxWidth  = sizeToFit.width;
    }
    
    
    CGFloat hegiht = (sizeToFit.height/formartHeight) * formartHeight + ((NSInteger)sizeToFit.height%(NSInteger)formartHeight > 0 ? formartHeight : 0);
    
    if(hegiht > formartHeight * paranIndex && paranIndex != 0){
        hegiht = formartHeight * paranIndex;
    }
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y,maxWidth, hegiht);
    
}



- (CGFloat)getTextFieldSize
{
    [self setNumberOfLines:0];
    NSString *gettext;
    if(self.attributedText == nil)
    {
        gettext = self.text;
    }else
    {
        gettext = self.text;
    }
    CGSize sizeToFit = [gettext sizeWithFont:self.font  constrainedToSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
    
    return sizeToFit.height + 10;
}


//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [[ShareData sharedInstance] clearnView];
//    [_controlTimer invalidate];
//    _controlTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeControl:) userInfo:nil repeats:YES];
//    [_controlTimer fire];
//    _countTimer = 0;
//    NSLog(@"开始");
//    NSSet *allTouches = [event allTouches];    //返回与当前接收者有关的所有的触摸对象
//    UITouch *touch = [allTouches anyObject];   //视图中的所有对象
//    CGPoint point = [touch locationInView:[touch window]]; //返回触摸点在视图中的当前坐标
//    int x = point.x;
//    int y = point.y;
//    
//    
//    //    NSSet *allTouches1 = [event allTouches];    //返回与当前接收者有关的所有的触摸对象
//    //    UITouch *touch1 = [allTouches1 anyObject];   //视图中的所有对象
//    CGPoint point1 = [touch locationInView:[touch view]]; //返回触摸点在视图中的当前坐标
//    int x1 = point1.x;
//    int y1 = point1.y;
//    
//    
//    x = x - x1 ;
//    y = y - y1;
//    
//    _windowsPoint = CGPointMake(x, y);
//    NSLog(@"touch (x, y) is (%d, %d)", x, y);
//    
//}
//
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [_controlTimer invalidate];
//    _controlTimer = nil;
//    
//    
//    if(_countTimer <= 1)  //点击效果
//    {
//        [self.selectDelegate  singleSelect:_needDeliverDic];
//    }
//    NSLog(@"结束");
//}
//
//- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [_controlTimer invalidate];
//    _controlTimer = nil;
//    NSLog(@"取消");
//}
//
- (void)timeControl:(NSTimer *)paramTimer
{
    _countTimer ++;
    
    if(_countTimer == 2)
    {
        NSLog(@"长按了哇");
        //        if(_actionButtonView == nil)
        //        {
        //            _actionButtonView = [[PJLWindowsView alloc]initWithFrame:CGRectMake(0, 0, 100, 30)] ;
        //            [[UIApplication sharedApplication].keyWindow addSubview:_actionButtonView];
        //        }
        //        _actionButtonView.windowDelegate = self;
        //        [_actionButtonView setHidden:NO];
        //        [_actionButtonView setShowFrame:_windowsPoint];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    
}

- (void)endTouchDelegate
{
    [self setBackgroundColor:[UIColor clearColor]];
}


- (void)setZanImageStr:(NSString *)imageStr
        setNumberOfZan:(NSString *)zans
         setTitleColor:(UIColor *)zanTitleColor     //标题颜色
setTitleParagraphStyle:(NSMutableParagraphStyle *)zanTitleParaghStyle //标题风格
      setCommitImageStr:(NSString *)commitImageStr
      setNumberOfCommit:(NSString *)commits
         setCommitTitleColor:(UIColor *)commotTitleColor     //标题颜色
setCommitParagraphStyle:(NSMutableParagraphStyle *)commotTitleParaghStyle //标题风格
{
      NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@""];
      NSTextAttachment *zanAttchMent=[[NSTextAttachment alloc]init];
    
      UIImage *getZamImage = [UIImage imageNamed:imageStr];
      zanAttchMent.image = getZamImage;
    
      zanAttchMent.bounds = CGRectMake(0,
                                     -3,
                                     zanAttchMent.image.size.width,
                                     zanAttchMent.image.size.height);
    
      NSAttributedString *zamStr = [NSAttributedString attributedStringWithAttachment:zanAttchMent];
      [str appendAttributedString:zamStr]; //完成第一个图片
    
    NSMutableAttributedString *zansStr = [[NSMutableAttributedString alloc] initWithString:zans];
    [zansStr addAttribute:NSForegroundColorAttributeName value:zanTitleColor range:NSMakeRange(0,zansStr.length)];
    [zansStr addAttribute:NSParagraphStyleAttributeName value:zanTitleParaghStyle range:NSMakeRange(0,zansStr.length)];
    [str appendAttributedString:zansStr]; //完成第一个赞的内容

    
    
    NSTextAttachment *commitAttchMent=[[NSTextAttachment alloc]init];
    
    UIImage *getCommitImage = [UIImage imageNamed:commitImageStr];
    commitAttchMent.image = getCommitImage;
    
    commitAttchMent.bounds = CGRectMake(0,
                                     -3,
                                     13.0,
                                     13.0);
    
    NSAttributedString *commitStr = [NSAttributedString attributedStringWithAttachment:commitAttchMent];
    [str appendAttributedString:commitStr]; //完成第二个图片
    
    NSMutableAttributedString *commitsStr = [[NSMutableAttributedString alloc] initWithString:commits];
    [commitsStr addAttribute:NSForegroundColorAttributeName value:commotTitleColor range:NSMakeRange(0,commitsStr.length)];
    [commitsStr addAttribute:NSParagraphStyleAttributeName value:commotTitleParaghStyle range:NSMakeRange(0,commitsStr.length)];
    [str appendAttributedString:commitsStr]; //完成第一个赞的内容

     self.attributedText = str;
    
}


- (void)pjlSetTitleStr:(NSString *)paramSender      //标题
         setTitleColor:(UIColor *)titleColor     //标题颜色
          settitleFont:(UIFont *)titleFont       //标题字体
setTitleParagraphStyle:(NSMutableParagraphStyle *)titleParaghStyle //标题风格
             withImage:(UIImage *)paramImage
{
    //    paramSender = @"asdasdasdasdasyhkhkhkhkadasdasdasdasdsadasdasdasdasdasdasdasdasdasdadh111  ";
    NSString *getStr = [NSString stringWithFormat:@"%@  ",paramSender];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:getStr];
    [str addAttribute:NSForegroundColorAttributeName value:titleColor range:NSMakeRange(0,paramSender.length)];
    
    [str addAttribute:NSParagraphStyleAttributeName value:titleParaghStyle range:NSMakeRange(0,paramSender.length)];
    //
    if(paramImage != nil){
        NSTextAttachment *attach=[[NSTextAttachment alloc]init];
        attach.image = paramImage;
        attach.bounds = CGRectMake(0, -3, paramImage.size.width/2.0, paramImage.size.height/2.0);
        //3.创建属性字符串 通过图片附件
        NSAttributedString *attrStr = [NSAttributedString attributedStringWithAttachment:attach];
        //4.把NSAttributedString添加到NSMutableAttributedString里面
            [str appendAttributedString:attrStr];
    }
    [self setNumberOfLines:0];
    //    [str addAttribute:NSForegroundColorAttributeName value:[UIColor greenColor] range:NSMakeRange(19,6)];
    
    
    self.attributedText = str;
    
    NSDictionary *dic = @{NSFontAttributeName:titleFont,NSParagraphStyleAttributeName:titleParaghStyle};
    
    CGRect autoRect = [[NSString stringWithFormat:@"%@测试11",paramSender] boundingRectWithSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil];
    
    self.frame = CGRectMake(self.frame.origin.x,self.frame.origin.y,self.frame.size.width,autoRect.size.height);
    
}


-(void)pjlSetTitleStr:(NSString *)paramSender      //标题
        setTitleColor:(UIColor *)titleColor     //标题颜色
         settitleFont:(UIFont *)titleFont       //标题字体
    setTitleParagraphStyle:(NSMutableParagraphStyle *)titleParaghStyle //标题风格
{
    NSString *getStr = [NSString stringWithFormat:@"%@  ",paramSender];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:getStr];
    [str addAttribute:NSForegroundColorAttributeName value:titleColor range:NSMakeRange(0,paramSender.length)];
    
    [str addAttribute:NSParagraphStyleAttributeName value:titleParaghStyle range:NSMakeRange(0,paramSender.length)];

    self.attributedText = str;
    
    
}

- (NSMutableAttributedString *)pjlMoreInfo2:(NSArray *)getArry{
    
    
    NSString *getStr = @"";
    NSString *showStr = @"";
    
    for(PJLQRTitleNobject *pjlModel in getArry)
    {
        showStr = [NSString stringWithFormat:@"%@%@",showStr,pjlModel.lableText];
        
    }
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:showStr];
    
    for(PJLQRTitleNobject *pjlModel in getArry)
    {
        
        [str addAttribute:NSForegroundColorAttributeName
                    value:pjlModel.labelColor range:NSMakeRange(getStr.length,pjlModel.lableText.length)];
        
        [str addAttribute:NSFontAttributeName value:pjlModel.labeFont
                    range:NSMakeRange(getStr.length, pjlModel.lableText.length)];
        
        [str addAttribute:NSParagraphStyleAttributeName value:pjlModel.modelParagrapStyle range:NSMakeRange(getStr.length,pjlModel.lableText.length)];
        
        getStr = [NSString stringWithFormat:@"%@%@",getStr,pjlModel.lableText];
        
    }
    
    
    return str;
    
    
}

- (NSMutableAttributedString *)setImageStr:(NSString *)imageStr
                                 setCGSize:(CGRect)frame
                         withZanTitleStyle:(NSMutableParagraphStyle *)zanTitleParaghStyle{
    
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@""];
    NSTextAttachment *zanAttchMent=[[NSTextAttachment alloc]init];
    UIImage *getZamImage = [UIImage imageNamed:imageStr];
    zanAttchMent.image = getZamImage;
    
    zanAttchMent.bounds = frame;
    
    NSAttributedString *zamStr = [NSAttributedString attributedStringWithAttachment:zanAttchMent];
    [str appendAttributedString:zamStr]; //完成第一个图片
    
    [str addAttribute:NSParagraphStyleAttributeName
                value:zanTitleParaghStyle
                range:NSMakeRange(0,zamStr.length)];
    
    
    return  str;
    
}

- (void)setPJLSizeFitFrame{
    
    CGSize getSize = [self sizeThatFits:CGSizeMake(CGFLOAT_MAX,CGFLOAT_MAX)];
    
    self.frame = CGRectMake(self.frame.origin.x,
                            self.frame.origin.y,
                            getSize.width,
                            getSize.height);
    
}


@end
