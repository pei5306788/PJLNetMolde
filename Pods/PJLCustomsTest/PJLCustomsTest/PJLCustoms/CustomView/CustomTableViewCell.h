//
//  CustomTableViewCell.h
//  QinXin
//
//  Created by rs-mac-mini on 15/5/30.
//  Copyright (c) 2015年 rs-mac-mini. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "ShareData.h"
#import "CustomImageView.h"
#import "CustomButton.h"
#import "CustomLabel.h"
#import "CustomTableViewCell.h"
#import "CustomTextField.h"
#import "CustomUIScrollView.h"
#import "CustomUITextView.h"
#import "CustomView.h"
#import "CustomImageView.h"
@interface CustomTableViewCell : UITableViewCell
{
    CustomView *_topView;
    CustomView *_downView;
}

- (void)setDic:(NSDictionary *)paramDic; //加载数据


/*
 自定义线密封性的，这个是分隔用的
 @param topColor 上面线的颜色
 @param bottomColor 下面颜色
 @param borderWidth  线的宽度
 
 */

- (void)setTopCplor:(UIColor *)topColor
     setBottomColor:(UIColor *)bottomColor
     setBorderWidth:(CGFloat )borderWidth;


/**
 *自定义线密封性的，这个是分隔用的
 *@param topColor 上面线的颜色
 *@param bottomColor 下面颜色
 *@param borderWidth  线的宽度
 *@param topLeft  上左边的距离
 *@param topRight 上右边的距离
 *@param bottomLeft 下左边的距离
 *@param bottomRight 下右边的距离
 **/
- (void)setTopCplor:(UIColor *)topColor
     setBottomColor:(UIColor *)bottomColor
     setBorderWidth:(CGFloat )borderWidth
         setTopLeft:(CGFloat)topLeft
        setTopRight:(CGFloat)topRight
      setBottomLeft:(CGFloat)bottomLeft
     setBottomRight:(CGFloat)bottomRight;

@end
