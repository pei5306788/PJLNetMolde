//
//  PJLQRTitleNobject.m
//  BDYue
//
//  Created by pjl on 2017/5/17.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLQRTitleNobject.h"

@implementation PJLQRTitleNobject

- (id)init{
    
    self = [super init];
    if(self){
        
        self.labeFont = [UIFont systemFontOfSize:14.0];
        
        self.lableText = @"";
        
        self.labelColor = [UIColor whiteColor];
        
        self.modelParagrapStyle = [[NSMutableParagraphStyle alloc]init];
        
    }
    return self;
    
}

/* 快速初始化
 * @param labelTextString 字符串内容
 * @param font 字体
 * @param textColor 字体颜色
 * @param modelStyle 字体风格
 */

- (void)newInitString:(NSString *)labelTextString
             withFont:(UIFont *)font
        withTextColor:(UIColor *)textColor
       withModelStyle:(NSMutableParagraphStyle *)modelStyle{
    
    self.lableText = labelTextString;
    self.labeFont = font;
    self.labelColor = textColor;
    if(modelStyle != nil){
         self.modelParagrapStyle = modelStyle;
    }
   
}

/*
 * 配置富文本图片信息
 * @param imageStr  本地图片 网络图片目前没做
 * @param frame   图片的大小
 * @param paragraphStyle 图片风格
 */
- (NSMutableAttributedString *)setImageStr:(NSString *)imageStr
                                 setCGSize:(CGRect)frame
                         withParagraphStyle:(NSMutableParagraphStyle *)paragraphStyle{
    if(paragraphStyle == nil){
        
        paragraphStyle = self.modelParagrapStyle;
        
    }
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@""];
    NSTextAttachment *zanAttchMent=[[NSTextAttachment alloc]init];
    UIImage *getZamImage = [UIImage imageNamed:imageStr];
    if(getZamImage == nil){
         //图片不存在的话 那就没办法了哇
        return str;
    }
    zanAttchMent.image = getZamImage;
    zanAttchMent.bounds = frame;
    NSAttributedString *zamStr = [NSAttributedString attributedStringWithAttachment:zanAttchMent];
    [str appendAttributedString:zamStr]; //完成第一个图片
    
    [str addAttribute:NSParagraphStyleAttributeName
                value:paragraphStyle
                range:NSMakeRange(0,zamStr.length)];
    
    
    return  str;
    
}

/*
 * return model 自己 字符串 NSMutableAttributedString
 *
 *
 */

- (NSMutableAttributedString *)modelToAttributedString{
    
    NSString *showStr = @"";
    showStr = [NSString stringWithFormat:@"%@",self.lableText];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:showStr];
    //设置字体颜色
    [str addAttribute:NSForegroundColorAttributeName
                value:self.labelColor range:NSMakeRange(showStr.length,self.lableText.length)];
    //设置字体大小
    [str addAttribute:NSFontAttributeName value:self.labeFont
                range:NSMakeRange(showStr.length, self.lableText.length)];
    //设置字体风格
    [str addAttribute:NSParagraphStyleAttributeName value:self.modelParagrapStyle range:NSMakeRange(showStr.length,self.lableText.length)];
    return str;
    
}

@end
