//
//  PJLQRTitleNobject.h
//  BDYue
//
//  Created by pjl on 2017/5/17.
//  Copyright © 2017年 PJL. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


@interface PJLQRTitleNobject : NSObject

@property (nonatomic,strong)NSString *lableText;

@property (nonatomic,strong)UIColor *labelColor;

@property (nonatomic,strong)UIFont *labeFont;

@property (nonatomic,strong)NSMutableParagraphStyle *modelParagrapStyle;



/* 快速初始化
 * @param labelTextString 字符串内容
 * @param font 字体
 * @param textColor 字体颜色
 * @param modelStyle 字体风格
 */

- (void)newInitString:(NSString *)labelTextString
             withFont:(UIFont *)font
        withTextColor:(UIColor *)textColor
       withModelStyle:(NSMutableParagraphStyle *)modelStyle;


/*
 * 配置富文本图片信息
 * @param imageStr  本地图片 网络图片目前没做
 * @param frame   图片的大小
 * @param paragraphStyle 图片风格
 */
- (NSMutableAttributedString *)setImageStr:(NSString *)imageStr
                                 setCGSize:(CGRect)frame
                        withParagraphStyle:(NSMutableParagraphStyle *)paragraphStyle;


/*
 * return model 自己 字符串 NSMutableAttributedString
 *
 *
 */
- (NSMutableAttributedString *)modelToAttributedString;

@end
