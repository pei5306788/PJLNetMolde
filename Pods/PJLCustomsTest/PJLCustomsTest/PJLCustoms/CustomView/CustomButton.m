//
//  CustomButton.m
//  QinXin
//
//  Created by rs-mac-mini on 15/5/30.
//  Copyright (c) 2015年 rs-mac-mini. All rights reserved.
//

#import "CustomButton.h"

@implementation CustomButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        _pjlImageView = [[CustomImageView alloc]initWithFrame:frame];
        [self addSubview:_pjlImageView];
    }
    return self;
}

- (void)upStatus
{
    if(_pjlImageView == nil){
    _pjlImageView = [[CustomImageView alloc]init];
        [self addSubview:_pjlImageView];
    }

    _pjlImageView.frame = self.bounds;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
