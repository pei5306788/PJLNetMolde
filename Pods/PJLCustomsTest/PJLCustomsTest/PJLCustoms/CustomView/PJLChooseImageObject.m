//
//  ChooseOneImageObject.m
//  pjlWork2
//
//  Created by 裴建兰 on 16/3/9.
//  Copyright © 2016年 裴建兰. All rights reserved.
//
//#import "ShareData.h"
#import "PJLChooseImageObject.h"
#import <MobileCoreServices/UTCoreTypes.h>

@implementation PJLChooseImageObject

- (id)init{
    self = [super init];
    if(self){
    
        _pjlIsFont = NO;
        
        _pjlIsEdit = YES;
    }
    return self;
}


- (void)createActionWithTitle:(NSArray *)getTitle
                     setTitle:(NSString *)titile{
    
    NSString *myTitle = @"";
    if(titile == nil){
    
        myTitle = @"选择图片来源";
    }else{
        
        myTitle = titile;
    }
    
    if(getTitle.count == 0 || getTitle == nil){
    
        getTitle = @[@"从相册中获取",@"拍照获取"];
    }
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]init];
    actionSheet.delegate = self;
    actionSheet.title = myTitle;
    for(NSString *eachTitle in getTitle){
     
        [actionSheet addButtonWithTitle:eachTitle];
    }
     actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"取消"];
     [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    
}


- (void)setListenBlock:(PJLChooseImageBlock)block{

    _pjlChooseImageBlock = block;

}

-(void)takePhoto
{
    
    if(_pjlIsFont){
    
        BOOL isCamera = [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
        if (!isCamera) {
            NSLog(@"没有前置摄像头");
            return ;
        }

        
    }else{
    
        BOOL isCamera = [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
        if (!isCamera) {
            NSLog(@"没有后置摄像头");
            return ;
        }
        

    }
       UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        //设置拍照后的图片可被编辑
        picker.allowsEditing = _pjlIsEdit;
        picker.sourceType = sourceType;
        if(_pjlIsFont){
            picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        if(_viewControll == nil){

        }else{
            [_viewControll presentViewController:picker animated:YES completion:nil];
            
        }

    }else
    {
        NSLog(@"模拟其中无法打开照相机,请在真机中使用");
    }
}

//打开本地相册
-(void)LocalPhoto
{
    
    if(_viewControll == nil){
     
        NSLog(@"没有设置选中的VC");
        return;
    }

    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    //设置选择后的图片可被编辑
    picker.allowsEditing = _pjlIsEdit;
    
    [_viewControll presentViewController:picker animated:YES completion:nil];
    


   }

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //通过UIImagePickerControllerMediaType判断返回的是照片还是视频
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    
    
    //  //如果返回的type等于kUTTypeImage，代表返回的是照片,并且需要判断当前相机使用的sourcetype是拍照还是相册
    if ([type isEqualToString:(NSString*)kUTTypeImage]
        ||picker.sourceType==UIImagePickerControllerSourceTypeCamera) {
        
        //当选择的是图片的话 还是以前的操作
        
        
      UIImage* image;
      
   
      
      if(_pjlIsEdit){
      
          image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
          
          CGSize imagesize = image.size;
          
          imagesize.height = 500;
          imagesize.width =  500;
          image = [self imageWithImage:image scaledToSize:imagesize];
      
      }else{
      
          image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
          
         
          if(_screenWith == 0 || _isNeedOrigin == true){
              
          }else{
          //设置image的尺寸
              CGSize imagesize = image.size;
          
              imagesize.height = _screenWith  * (image.size.height/image.size.width);
              imagesize.width =  _screenWith;
          //对图片大小进行压缩--
              image = [self imageWithImage:image scaledToSize:imagesize];
              
          }
             }
      
      if(image == nil){
                
          image = [[UIImage alloc]init];
                
          }
      if(_pjlChooseImageBlock != nil){
      
        
          
          _pjlChooseImageBlock(picker,info,@[image]);
          
  //        _pjlChooseImageBlock = nil;
      
      }
      
      if(self.pjlPickOmeImageDelegate != nil){
      
           [self.pjlPickOmeImageDelegate PJLImagePickerController:picker didFinishPickingMediaWithInfo:info withImageArry:@[image]];
      
      }
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        
    }
    
    
   


}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    NSLog(@"您取消了选择图片");
    [picker dismissViewControllerAnimated:YES completion:nil];
}

//对图片尺寸进行压缩--
-(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    
    // Tell the old image to draw in this new context, with the desired
    // new size
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Get the new image from the context
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // End the context
    UIGraphicsEndImageContext();
    
    // Return the new image.
    return newImage;
}

#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
   
    if(buttonIndex == actionSheet.cancelButtonIndex){
    
        return;
    }
    //从相册中获取，
    //从本地获取
    if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"从相册中获取"]){
        if(_chooseNumber == 0){
            [self LocalPhoto];
        }else{
            [self setChooseImage];
        }
    }
    
    if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"拍照获取"]){
        [self takePhoto];
        
    }
    
}

#pragma mark ELCImagePick

- (void)setChooseImage{

    if(_viewControll == nil){
        
        NSLog(@"没有设置选中的VC");
        return;
    }

    
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
    
    elcPicker.maximumImagesCount = _chooseNumber; //Set the maximum number of images to select to 100
    elcPicker.returnsOriginalImage = NO; //Only return the fullScreenImage, not the fullResolutionImage
    elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
    elcPicker.onOrder = YES; //For multiple image selection, display and return order of selected images
    elcPicker.mediaTypes = @[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie]; //Supports image and movie types
    
    elcPicker.imagePickerDelegate = self;
    
    
    [_viewControll presentViewController:elcPicker animated:YES completion:nil];
    

}

#pragma mark ELCImagePickerControllerDelegate Methods

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
   
    NSMutableArray *imageDataList = [[NSMutableArray alloc]init];
    
//    CGRect workingFrame = _scrollView.frame;
//    workingFrame.origin.x = 0;
//    
//    NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
    for (NSDictionary *dict in info) {
        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
            if ([dict objectForKey:UIImagePickerControllerEditedImage]){
                UIImage* image=[dict objectForKey:UIImagePickerControllerEditedImage];
                
                
                //设置image的尺寸
                CGSize imagesize = image.size;
                
                
                if(_screenWith == 0 || _isNeedOrigin == true){
                    
                }else{
                    imagesize.height = _screenWith  * (image.size.height/image.size.width);
                    imagesize.width =  _screenWith;
                //对图片大小进行压缩--
                    image = [self imageWithImage:image scaledToSize:imagesize];
                    
                }

                [imageDataList addObject:image];
//                [images addObject:image];
//
//                UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
//                [imageview setContentMode:UIViewContentModeScaleAspectFit];
//                imageview.frame = workingFrame;
//                
//                [_scrollView addSubview:imageview];
//                
//                workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
            } else {
             
                UIImage* image = [dict objectForKey:UIImagePickerControllerOriginalImage];
                
                if(_screenWith == 0 || _isNeedOrigin == true){
                    
                }else{
                    //设置image的尺寸
                    CGSize imagesize = image.size;
                    
                    imagesize.height = _screenWith  * (image.size.height/image.size.width);
                    imagesize.width =  _screenWith;
                    //对图片大小进行压缩--
                        image = [self imageWithImage:image scaledToSize:imagesize];
                    
                }
                
                [imageDataList addObject:image];

                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        } else if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypeVideo){
            if ([dict objectForKey:UIImagePickerControllerEditedImage]){
                UIImage* image=[dict objectForKey:UIImagePickerControllerEditedImage];
                
                if(_screenWith == 0 || _isNeedOrigin == true){
                    
                }else{
                //设置image的尺寸
                CGSize imagesize = image.size;
                
                imagesize.height = _screenWith  * (image.size.height/image.size.width);
                imagesize.width =  _screenWith;
                //对图片大小进行压缩--
                    image = [self imageWithImage:image scaledToSize:imagesize];
                    
                }
                
                  [imageDataList addObject:image];
//                [images addObject:image];
//                
//                UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
//                [imageview setContentMode:UIViewContentModeScaleAspectFit];
//                imageview.frame = workingFrame;
//                
//                [_scrollView addSubview:imageview];
//                
//                workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
            } else {
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
                
                
                NSURL *referenceURL = [dict objectForKey:UIImagePickerControllerReferenceURL];
                ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                __block UIImage *returnValue = nil;
                [library assetForURL:referenceURL resultBlock:^(ALAsset *asset) {
                    
                    returnValue = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullResolutionImage]];
               
                                 CGSize imagesize = returnValue.size;
                                 
                    imagesize.height = self->_screenWith  * (returnValue.size.height/returnValue.size.width);
                    imagesize.width =  self->_screenWith;
                                 //对图片大小进行压缩--
                                 returnValue = [self imageWithImage:returnValue scaledToSize:imagesize];
                                 
                                 [imageDataList addObject:returnValue];
                    
                    
                    if(info.count == imageDataList.count){
                        if(self->_pjlChooseImageBlock != nil){
                               
                            self->_pjlChooseImageBlock(picker,nil,imageDataList);
                               
                       //        _pjlChooseImageBlock = nil;
                               
                           }
                           
                           if(self.pjlPickOmeImageDelegate != nil){
                           
                               [self.pjlPickOmeImageDelegate PJLImagePickerController:picker didFinishPickingMediaWithInfo:nil withImageArry:imageDataList];
                           }
                           
                       }
                    
                } failureBlock:^(NSError *error) {
                   // error handling
                    
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"选择照片出问题" message:@"会有缺失" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles: nil];
                    
                    [alert show];
                    
                    
                }];
                
              
                
                
             

            }
        } else {
            NSLog(@"Uknown asset type");
        }
    }

    
    if(info.count == imageDataList.count){
        if(_pjlChooseImageBlock != nil){
            
            _pjlChooseImageBlock(picker,nil,imageDataList);
            
    //        _pjlChooseImageBlock = nil;
            
        }
        
        if(self.pjlPickOmeImageDelegate != nil){
        
            [self.pjlPickOmeImageDelegate PJLImagePickerController:picker didFinishPickingMediaWithInfo:nil withImageArry:imageDataList];
        }
        
    }

}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
//    if(_viewControll == nil){
//        [[ShareData sharedInstance].BaseNavigationV.viewControllers.lastObject dismissViewControllerAnimated:YES completion:nil];
//    }else{
//        [_viewControll presentViewController:picker animated:YES completion:nil];
//        
//    }
}


//(IBAction)launchSpecialController
//{
//    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
//    self.specialLibrary = library;
//    NSMutableArray *groups = [NSMutableArray array];
//    [_specialLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
//        if (group) {
//            [groups addObject:group];
//        } else {
//            // this is the end
//            [self displayPickerForGroup:[groups objectAtIndex:0]];
//        }
//    } failureBlock:^(NSError *error) {
//        self.chosenImages = nil;
//        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"Album Error: %@ - %@", [error localizedDescription], [error localizedRecoverySuggestion]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
//        
//        NSLog(@"A problem occured %@", [error description]);
//        // an error here means that the asset groups were inaccessable.
//        // Maybe the user or system preferences refused access.
//    }];
//}
//
//- (void)displayPickerForGroup:(ALAssetsGroup *)group
//{
//    ELCAssetTablePicker *tablePicker = [[ELCAssetTablePicker alloc] initWithStyle:UITableViewStylePlain];
//    tablePicker.singleSelection = YES;
//    tablePicker.immediateReturn = YES;
//    
//    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initWithRootViewController:tablePicker];
//    elcPicker.maximumImagesCount = 1;
//    elcPicker.imagePickerDelegate = self;
//    elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
//    elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
//    elcPicker.onOrder = NO; //For single image selection, do not display and return order of selected images
//    tablePicker.parent = elcPicker;
//    
//    // Move me
//    tablePicker.assetGroup = group;
//    [tablePicker.assetGroup setAssetsFilter:[ALAssetsFilter allAssets]];
//    
//    [self presentViewController:elcPicker animated:YES completion:nil];
//}


@end
