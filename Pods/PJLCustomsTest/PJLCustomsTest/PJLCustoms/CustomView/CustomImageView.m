//
//  CusotmUIImageView.m
//  QinXin
//
//  Created by rs-mac-mini on 15/5/30.
//  Copyright (c) 2015年 rs-mac-mini. All rights reserved.
//
#import "PJLURLConnection.h"
#import "CustomImageView.h"
//#import "ShareData.h"

#import "XHImageViewer.h"

#import "PJLSavePhotoImageNSObject.h"

#import "PJLXHImageTopView.h"

@interface CustomImageView()<XHImageViewerDelegate>{

    XHImageViewer *_imageShow;
    
    PJLSavePhotoImageNSObject *_savePhotoNSObject;
    
    PJLXHImageTopView *_pjlXHImageTopView;
}

@end

@implementation CustomImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self setContentMode:UIViewContentModeCenter];
        [self.layer setMasksToBounds:YES];
    }
    return self;
}
/********************************加载图片**************************************/
- (void)setimageUrl:(NSString *)pramUrl
{
    
//    NSString *url = [NSString stringWithFormat:@"%@/%@",[ShareData sharedInstance].imageAddress,pramUrl];
    
    
      NSString* encodedString = [pramUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
     [self sd_setImageWithURL:[NSURL URLWithString:encodedString]];
//    if([[ShareData sharedInstance].uility getFileData:pramUrl] != nil)
//    {
//        self.image = [[ShareData sharedInstance].uility getFileData:pramUrl];
//        return;
//    }
//    self.image = nil;
//    NSURL *url = [NSURL URLWithString:pramUrl];
//    
//    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];//通过url，获得request;
//    PJLURLConnection *pjlConnection = [[PJLURLConnection alloc] initWithRequest:request
//                                                   delegate:self];//根据request得到connection;
//    pjlConnection.PJLUrl = pramUrl;
//    pjlConnection.needToAddImageView = self;
//    [pjlConnection start];
}

- (void)setimageUrl:(NSString *)pramUrl
     withDefaultUrl:(NSString *)defaultImage{
    
     NSString *url = pramUrl;
    
    NSString* encodedString = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self sd_setImageWithURL:[NSURL URLWithString:encodedString]
            placeholderImage:[UIImage imageNamed:defaultImage]];
  
    
}

- (void)setimageUrl:(NSString *)pramUrl
     withDefaultUrl:(NSString *)defaultImage
          withBlock:(void(^)(UIImage *image,
                             NSError *error,
                             SDImageCacheType cacheType,
                             NSURL *imageURL))complete{
      NSString *url = pramUrl;
    
    NSString* encodedString = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self sd_setImageWithURL:[NSURL URLWithString:encodedString]
            placeholderImage:[UIImage imageNamed:defaultImage] options:SDWebImageContinueInBackground completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
                
                complete(image,error,cacheType,imageURL);
                
                           
            }];

}

- (void)setDownLoadAndSetImageID:(NSString *)imageUrl

              setDefasultImage:(NSString *)defaulitName{

    [self setimageUrl:imageUrl withDefaultUrl:defaulitName];

}


/*********************************************************************************/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    
        
//    if([ShareData sharedInstance].pjlSendObjectWay.keyBoardView != nil){
//        
//        [[ShareData sharedInstance].pjlSendObjectWay.keyBoardView  endEditing:YES];
//        
//    }

    NSMutableArray *_imageViews = [[NSMutableArray alloc]init];

    
    if(_imageShow == nil){
        _imageShow = [[XHImageViewer alloc]
                        initWithImageViewerWillDismissWithSelectedViewBlock:
                        ^(XHImageViewer *imageViewer, UIImageView *selectedView) {
                            NSInteger index = [_imageViews indexOfObject:selectedView];
                            NSLog(@"willDismissBlock index : %ld", (long)index);
                        }
                        didDismissWithSelectedViewBlock:^(XHImageViewer *imageViewer,
                                                          UIImageView *selectedView) {
                            NSInteger index = [_imageViews indexOfObject:selectedView];
                            NSLog(@"didDismissBlock index : %ld", (long)index);
                        }
                        didChangeToImageViewBlock:^(XHImageViewer *imageViewer,
                                                    UIImageView *selectedView) {
                            NSInteger index = [_imageViews indexOfObject:selectedView];
                            
                            _savePhotoNSObject.saveImage = selectedView.image;
                          
                            NSLog(@"显示哪个页面%ld",index);
                            
                              [_pjlXHImageTopView setTopLabelString:[NSString stringWithFormat:@"%ld/%ld",index + 1,_imageViews.count]];
//                            [self.bottomToolBar.unLikeButton setTitle:[NSString stringWithFormat:@"%ld/%lu", index + 1, (unsigned long)_imageViews.count] forState:UIControlStateNormal];
                            
                        }];
        
       UILongPressGestureRecognizer *pressGesture= [[UILongPressGestureRecognizer alloc]initWithTarget:self
                                                        action:@selector(handleLongPress:)];
        [_imageShow addGestureRecognizer:pressGesture];
        
        _savePhotoNSObject = [[PJLSavePhotoImageNSObject alloc]init];
        
        _savePhotoNSObject.saveImage = self.image;
    }
    
    if(_needShowArryUrl.count == 0){
        
        if(_pjlSelectImageView == nil){
//            [_imageShow showWithImageViews:@[self] selectedView:self];
            
            [_imageViews addObject:self];
        }else{
            
            [_imageViews addObject:_pjlSelectImageView];
        }
    }else
    {
         [_imageViews addObjectsFromArray:_needShowArryUrl];
    }
    _imageShow.delegate = self;

    _imageShow.delegate = self;
    _imageShow.disableTouchDismiss = NO;
    
    if(_pjlXHImageTopView == nil){
        _pjlXHImageTopView = [[PJLXHImageTopView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44.0)];
        
    }
    

    
    if(_pjlSelectImageView == nil){
        
        [_imageShow showWithImageViews:_imageViews
                          selectedView:(UIImageView *)self];
        
        NSInteger index = [_imageViews indexOfObject:(UIImageView *)self];
        
        
        [_pjlXHImageTopView setTopLabelString:[NSString stringWithFormat:@"%ld/%ld",index + 1,_imageViews.count]];
    
    }else{
    
        [_imageShow showWithImageViews:_imageViews
                          selectedView:_pjlSelectImageView];
        NSInteger index = [_imageViews indexOfObject:_pjlSelectImageView];
        
        
        [_pjlXHImageTopView setTopLabelString:[NSString stringWithFormat:@"%ld/%ld",index + 1,_imageViews.count]];
    }
    
    
    
    
   
   
}

- (void)imageViewer:(XHImageViewer *)imageViewer  willDismissWithSelectedView:(UIImageView*)selectedView{

}

- (void)handleLongPress:(UILongPressGestureRecognizer *)param{

    if(param.state == UIGestureRecognizerStateBegan) {
      
        [_savePhotoNSObject createSaveLocalShowView:_imageShow];
    }else {
       
    }
}


- (UIView *)customTopToolBarOfImageViewer:(XHImageViewer *)imageViewer{
    if(_pjlXHImageTopView == nil){
        _pjlXHImageTopView = [[PJLXHImageTopView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44.0)];
    
    }
    
    return _pjlXHImageTopView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
