//
//  PJLLayoutConsTraint.h
//  PJLCustomsTest
//
//  Created by 裴建兰 on 2018/5/22.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface PJLLayoutConsTraint : NSObject


/*这个是简单经常用的方法 分装就是便于简单使用提高速度
 *@param itemView 需要适配的对象
 *@param toItem  目标的对象
 *@param multiplierEdgeInsets  top, left, bottom, right > 0 才适配
 *@param edgeInsts  top, left, bottom, right 相关参数距离配置
 */
- (void)setRelationWithItemView:(UIView *)itemView
                     withToItem:(UIView *)toItem
                 withMultiplier:(UIEdgeInsets )multiplierEdgeInsets
                  withEdgeInsts:(UIEdgeInsets )edgeInsts;

/* multiplierArry  kindArry 里面必须有2个数据包
 *@param itemView 目标对象
 *@param size 对象大小
 *@param multiplierArry @[@"widh",@"height"] > 0 才设置
 *@param kindArry @[@"=",@">=",@"<="]  这个会特殊判断
 */
- (void)setItemView:(UIView *)itemView
      withHAndWSize:(CGSize)size
 withMultiplierArry:(NSArray *)multiplierArry
   withSizeKindArry:(NSArray *)kindArry;

/* 同层之间的关系特殊处理
 * @param itemView 目标itemview
 * @param itemAttribute 参数左边右边等等
 * @param relateBy  大于 等于多余
 * @param toItemView 另一个目标
 * @param toItemAttribute  参数左边右边等等
 * @param multiplier 乘数
 * @param constant  常量
 * @param isAvtivity 开关
 * @return NSLayoutConstraint 类型
 */
- (NSLayoutConstraint *)setItemView:(UIView *)itemView
                  withItemAttribute:(NSLayoutAttribute)itemAttribute
                      withRelatedBy:(NSLayoutRelation)relateBy
                     withToItemView:(UIView *)toItemView
                withToItemAttribute:(NSLayoutAttribute)toItemAttribute
                    withmMultiplier:(CGFloat)multiplier
                       withConstant:(CGFloat)constant
                       withActivity:(BOOL)isAvtivity;

@end
