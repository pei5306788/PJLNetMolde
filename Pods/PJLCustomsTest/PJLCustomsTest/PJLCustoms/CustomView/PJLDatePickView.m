//
//  PJLDatePickView.m
//  ThreeMins
//
//  Created by PJL on 15/7/14.
//  Copyright (c) 2015年 Xjj. All rights reserved.
//
//#import "ShareData.h"
#import "PJLDatePickView.h"

@implementation PJLDatePickView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
        CustomView *secondView = [[CustomView alloc]initWithFrame:CGRectMake(0, frame.size.height - 216 - 30 , frame.size.width, 216 + 50.0)];
        [secondView setBackgroundColor:[UIColor  whiteColor]];
        
        
        
        _canceButton = [CustomButton buttonWithType:UIButtonTypeCustom];
        _canceButton.frame = CGRectMake(0, 0, self.frame.size.width/2.0, 30);
        [_canceButton.layer setCornerRadius:5.0];
        [_canceButton.layer setMasksToBounds:YES];
        [_canceButton setBackgroundColor:[UIColor clearColor]];
        [_canceButton setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        [_canceButton setTitle:@"取消" forState:UIControlStateNormal];
        [_canceButton addTarget:self action:@selector(cancer) forControlEvents:UIControlEventTouchUpInside];
        [secondView addSubview:_canceButton];
        
        _makeSureButton = [CustomButton buttonWithType:UIButtonTypeCustom];
        _makeSureButton.frame = CGRectMake(self.frame.size.width/2.0, _canceButton.frame.origin.y, _canceButton.frame.size.width, _canceButton.frame.size.height);
        
        [_makeSureButton.layer setCornerRadius:5.0];
        [_makeSureButton.layer setMasksToBounds:YES];
        [_makeSureButton setBackgroundColor:[UIColor clearColor]];
        [_makeSureButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [_makeSureButton setTitle:@"确定" forState:UIControlStateNormal];
        [secondView addSubview:_makeSureButton];

        _pjlDate = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_canceButton.frame),
                                                                 secondView.frame.size.width,
                                                                 secondView.frame.size.height - CGRectGetMaxY(_canceButton.frame))];
        
        _pjlDate.datePickerMode = UIDatePickerModeDate;
        [_pjlDate setDate:[NSDate date] animated:YES];
         _pjlDate.minimumDate = [NSDate date];
        [_pjlDate addTarget:self action:@selector(dataChoose:) forControlEvents:UIControlEventValueChanged];
        [secondView addSubview:_pjlDate];
        [self addSubview:secondView];
        
        [_pjlDate setBackgroundColor:[UIColor whiteColor]];
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        _chooseDate = [formatter stringFromDate:[NSDate date]];
        _makeSureButton.otherFlag = _chooseDate;


    }
    return self;
}

- (void)dataChoose:(UIDatePicker *)paramSender
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //    [formatter setDateFormat:@"YYYY-MM-dd"];
    _chooseDate = [formatter stringFromDate:[_pjlDate date]];
    
    _makeSureButton.otherFlag = _chooseDate;
    
    NSLog(@"%@",_chooseDate);

}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [self setHidden:YES];
}

- (void)cancer
{
   

    [self setHidden:YES];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
