//
//  CustomTextView.m
//  QinXin
//
//  Created by rs-mac-mini on 15/6/9.
//  Copyright (c) 2015年 rs-mac-mini. All rights reserved.
//
//#import "ShareData.h"
#import "CustomTextView.h"

@implementation CustomTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        self.delegate = self;
    }
    
    return self;
}


//开始编辑
- (void)textViewDidBeginEditing:(CustomTextView *)textView
{
    if([textView.text isEqualToString:_attentinContent]
       && self.textColor == [UIColor grayColor])
    {
        self.text = @"";
    }
    
    self.textColor = [UIColor blackColor];
}


- (void)textViewDidEndEditing:(CustomTextView *)textView //结束编辑
{
    if(textView.text.length == 0)
    {
        self.textColor = [UIColor grayColor];
        self.text = _attentinContent;
    }
}


- (void)setAttentionStr:(NSString *)paramStr
{
    _attentinContent = paramStr;
    self.text = _attentinContent;
    self.textColor = [UIColor grayColor];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
