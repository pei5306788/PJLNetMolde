//
//  CustomLabel.h
//  QinXin
//
//  Created by rs-mac-mini on 15/5/30.
//  Copyright (c) 2015年 rs-mac-mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PJLLableMember.h"
#import "PJLQRTitleNobject.h"
//#import "PJLWindowsView.h"
typedef enum
{
    VerticalAlignmentTop = 0, // default
    VerticalAlignmentMiddle,
    VerticalAlignmentBottom,
} VerticalAlignment;

@protocol SelectWayDelegate <NSObject>

- (void)singleSelect:(NSDictionary *)getDic;

@end


@interface CustomLabel : UILabel
{
    //    @private VerticalAlignment _verticalAlignment;
    
    NSTimer *_controlTimer;
    
    NSInteger _countTimer;
    
    CGPoint _windowsPoint;
}

@property (nonatomic,strong)NSString  *pjlCopyString;

@property (nonatomic,strong)NSString * pjlModelStr;

@property (nonatomic,weak)id <SelectWayDelegate> selectDelegate;

@property (nonatomic,strong)NSDictionary *needDeliverDic; //需要穿的数据

//@property (nonatomic,strong)PJLWindowsView *actionButtonView;

- (void)setMessageAttenteionLabel; //消息提醒状态

- (void)setSexLabelBackColor:(UIColor *)paramColor ;//设置显示性别的状态 设置颜色

@property (nonatomic) VerticalAlignment verticalAlignment;

- (void)pjlSetTitleStr:(NSString *)paramSender  //标题
         setTitleColor:(UIColor *)titleColor    //标题颜色
          settitleFont:(UIFont *)titleFont      //标题字体
         setContentStr:(NSString *)paramConstr  //内容
       setContentColor:(UIColor *)contentColor  //内容颜色
        setContentFont:(UIFont *)contentFont;   //内容字体


- (void)pjlSetTitleStr:(NSString *)paramSender      //标题
         setTitleColor:(UIColor *)titleColor     //标题颜色
          settitleFont:(UIFont *)titleFont       //标题字体
setTitleParagraphStyle:(NSMutableParagraphStyle *)titleParaghStyle //标题风格
         setContentStr:(NSString *)paramConstr              //内容
       setContentColor:(UIColor *)contentColor              //内容颜色
        setContentFont:(UIFont *)contentFont                //内容字体
setContentParagraphStyle:(NSMutableParagraphStyle *)contentParaghStyle; //内容风格

- (void)pjlMoreInfo:(NSArray *)getArry; //多种内容方式 数组里面放 PJLLableMember

- (CGFloat)getTextFieldSize;//获取高度


- (void)setFormartHeight:(CGFloat)formartHeight //格式化高度
               setNumber:(NSInteger)paranIndex;

- (void)setFormartHeight:(CGFloat)formartHeight
               setNumber:(NSInteger)paranIndex
             setMaxWidth:(CGFloat)maxWidth; //宽度设置

- (void)pjlSetTitleStr:(NSString *)paramSender      //标题
         setTitleColor:(UIColor *)titleColor     //标题颜色
          settitleFont:(UIFont *)titleFont       //标题字体
setTitleParagraphStyle:(NSMutableParagraphStyle *)titleParaghStyle //标题风格
             withImage:(UIImage *)paramImage;


-(void)pjlSetTitleStr:(NSString *)paramSender      //标题
        setTitleColor:(UIColor *)titleColor     //标题颜色
         settitleFont:(UIFont *)titleFont       //标题字体
setTitleParagraphStyle:(NSMutableParagraphStyle *)titleParaghStyle; //标题风格
- (void)addCopy; //添加复制


/*
 点赞的东西 特殊处理
 */
- (void)setZanImageStr:(NSString *)imageStr
        setNumberOfZan:(NSString *)zans
         setTitleColor:(UIColor *)zanTitleColor     //标题颜色
setTitleParagraphStyle:(NSMutableParagraphStyle *)zanTitleParaghStyle //标题风格
     setCommitImageStr:(NSString *)commitImageStr
     setNumberOfCommit:(NSString *)commits
   setCommitTitleColor:(UIColor *)commotTitleColor     //标题颜色
setCommitParagraphStyle:(NSMutableParagraphStyle *)commotTitleParaghStyle; //标题风格

- (NSMutableAttributedString *)pjlMoreInfo2:(NSArray *)getArry; //瓶装

- (NSMutableAttributedString *)setImageStr:(NSString *)imageStr
                                 setCGSize:(CGRect)frame
                         withZanTitleStyle:(NSMutableParagraphStyle *)zanTitleParaghStyle;


- (void)setPJLSizeFitFrame;
@end
