//
//  PJLLableMember.m
//  DaDaMai
//
//  Created by rs-mac-mini on 15/5/26.
//  Copyright (c) 2015年 wu song. All rights reserved.
//

#import "PJLLableMember.h"

@implementation PJLLableMember

- (id)init
{
    self = [super init];
    if(self)
    {
        self.labeFont = [UIFont systemFontOfSize:14.0];
        self.labelColor = [UIColor blackColor];
        self.lableText = @"";
    }
    
    return self;
}


@end
