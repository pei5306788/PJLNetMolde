//
//  CusotmUIImageView.h
//  QinXin
//
//  Created by rs-mac-mini on 15/5/30.
//  Copyright (c) 2015年 rs-mac-mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomLabel.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface CustomImageView : UIImageView<NSURLConnectionDelegate>
{
    CustomLabel *_numLabel;
}

@property (nonatomic,weak)CustomImageView *pjlSelectImageView;

@property (nonatomic,weak) CustomImageView *selectView;

@property (nonatomic,strong)NSArray *needShowArryUrl; //点击方法图片组

/**
 * 加载网络图片 细节处理 缓存地址
 *  @param pramUrl 网络图片 或者本地图片 地址
 *  @param defaultImage  加载失败显示的默认图片
 *  @param complete  加载成功后保存的缓存图片位置
 *
 */

- (void)setimageUrl:(NSString *)pramUrl
     withDefaultUrl:(NSString *)defaultImage
          withBlock:(void(^)(UIImage *image,
                             NSError *error,
                             SDImageCacheType cacheType,
                             NSURL *imageURL))complete;

/**
 * 简单的加载图片
 * @param imageUrl 图片地址（本地file:// 或者网络的）
 * @param defaulitName 默认图片
 */

- (void)setDownLoadAndSetImageID:(NSString *)imageUrl

                setDefasultImage:(NSString *)defaulitName;

- (void)setimageUrl:(NSString *)pramUrl; //加载网络图片


- (void)setimageUrl:(NSString *)pramUrl
     withDefaultUrl:(NSString *)defaultImage;

@end
