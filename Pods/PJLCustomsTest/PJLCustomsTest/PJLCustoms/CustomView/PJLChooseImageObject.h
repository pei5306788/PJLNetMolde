//
//  ChooseOneImageObject.h
//  pjlWork2
//
//  Created by 裴建兰 on 16/3/9.
//  Copyright © 2016年 裴建兰. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ELCImagePickerHeader.h"

@protocol PJLPickOneImageDelegate <NSObject>

-(void)PJLImagePickerController:(id )picker
  didFinishPickingMediaWithInfo:(NSDictionary *)info
                  withImageArry:(NSArray *)imageArry;

@end
typedef void(^PJLChooseImageBlock)(id picker,NSDictionary *info,NSArray *imageArry);

@interface PJLChooseImageObject : NSObject<UINavigationControllerDelegate,
                                            UIImagePickerControllerDelegate,
                                            UIActionSheetDelegate,ELCImagePickerControllerDelegate>

@property (nonatomic) PJLChooseImageBlock pjlChooseImageBlock;

@property(nonatomic,weak)id <PJLPickOneImageDelegate>pjlPickOmeImageDelegate;

@property (nonatomic) NSInteger chooseNumber; //可以选择张照片

@property (nonatomic,strong) UIViewController *viewControll; //打开页面所对应的vc

@property (nonatomic) CGFloat screenWith; //编辑宽度 相册选择图片默认压缩的

@property (nonatomic) BOOL isNeedOrigin; //默认是 no  就会进行图片压缩 大小 和screenWith 有关系



//@property (nonatomic) id superVC; //打开页面所对应的vc

-(void)takePhoto;

//打开本地相册
-(void)LocalPhoto;

@property (nonatomic) BOOL pjlIsFont; //是不是打开前置摄像头

@property (nonatomic) BOOL pjlIsEdit; //照片是否可以被编辑


- (void)createActionWithTitle:(NSArray *)getTitle  //从相册中获取，拍照获取。
                     setTitle:(NSString *)titile;

- (void)setListenBlock:(PJLChooseImageBlock)block;

@end
