//
//  CustomButton.h
//  QinXin
//
//  Created by rs-mac-mini on 15/5/30.
//  Copyright (c) 2015年 rs-mac-mini. All rights reserved.
//
#import "CustomImageView.h"
#import <UIKit/UIKit.h>

@interface CustomButton : UIButton

@property (nonatomic,strong)CustomImageView *pjlImageView;

@property (nonatomic,strong)NSDictionary *dataDic;

@property (nonatomic,strong)NSString *otherFlag;

@property (nonatomic,weak) id showViewID;

@property (nonatomic,strong)NSString *otherFlagSecond;

@property (nonatomic,strong)NSString *secondViewID;

@property (nonatomic,strong) NSObject *modelSaveModel;

@property (nonatomic,strong) NSString *nameStr;

- (void)upStatus;

@end
