//
//  PJLLableMember.h
//  DaDaMai
//
//  Created by rs-mac-mini on 15/5/26.
//  Copyright (c) 2015年 wu song. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface PJLLableMember : NSObject

@property (nonatomic,strong)NSString *lableText;

@property (nonatomic,strong)UIColor *labelColor;

@property (nonatomic,strong)UIFont *labeFont;

@end
