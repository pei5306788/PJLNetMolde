//
//  PJLDatePickView.h
//  ThreeMins
//
//  Created by PJL on 15/7/14.
//  Copyright (c) 2015年 Xjj. All rights reserved.
//
#import "CustomButton.h"
#import "CustomView.h"

@interface PJLDatePickView : CustomView
{
    UIDatePicker *_pjlDate;
    CustomButton *_canceButton;

}

@property (nonatomic,strong)NSString *chooseDate;

@property (nonatomic,strong)CustomButton *makeSureButton;

@end
