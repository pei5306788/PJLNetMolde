//
//  CustomTextView.h
//  QinXin
//
//  Created by rs-mac-mini on 15/6/9.
//  Copyright (c) 2015年 rs-mac-mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextView : UITextView<UITextViewDelegate>
{
   
}

@property (nonatomic,strong) NSString *attentinContent;

- (void)setAttentionStr:(NSString *)paramStr;

- (void)textViewDidBeginEditing:(CustomTextView *)textView;

- (void)textViewDidEndEditing:(CustomTextView *)textView; //结束编辑


@end
