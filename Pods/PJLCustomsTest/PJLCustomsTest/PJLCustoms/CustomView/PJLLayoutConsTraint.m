//
//  PJLLayoutConsTraint.m
//  PJLCustomsTest
//
//  Created by 裴建兰 on 2018/5/22.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import "PJLLayoutConsTraint.h"

@interface PJLLayoutConsTraint()

@end

@implementation PJLLayoutConsTraint


/*  下层和上层的关系这个是简单经常用的方法 分装就是便于简单使用提高速度
 *@param itemView 需要适配的对象
 *@param toItem  目标的对象
 *@param multiplierEdgeInsets  top, left, bottom, right > 0 才适配
 *@param edgeInsts  top, left, bottom, right 相关参数距离配置
 */
- (void)setRelationWithItemView:(UIView *)itemView
                     withToItem:(UIView *)toItem
                 withMultiplier:(UIEdgeInsets )multiplierEdgeInsets
                  withEdgeInsts:(UIEdgeInsets )edgeInsts{
    itemView.translatesAutoresizingMaskIntoConstraints = false;
    
    
    if(multiplierEdgeInsets.left > 0){
        NSLayoutConstraint *left = [NSLayoutConstraint
                                    constraintWithItem:itemView
                                    attribute:(NSLayoutAttributeLeft)
                                    relatedBy:(NSLayoutRelationEqual)
                                    toItem:toItem
                                    attribute:(NSLayoutAttributeLeft)
                                    multiplier:multiplierEdgeInsets.left
                                    constant:edgeInsts.left];
            [left setActive:YES];
    }
    
    if(multiplierEdgeInsets.top > 0){
        
        NSLayoutConstraint *top = [NSLayoutConstraint
                                    constraintWithItem:itemView
                                    attribute:(NSLayoutAttributeTop)
                                    relatedBy:(NSLayoutRelationEqual)
                                    toItem:toItem
                                    attribute:(NSLayoutAttributeTop)
                                    multiplier:multiplierEdgeInsets.top
                                    constant:edgeInsts.top];
        [top setActive:YES];
        
    }
    
    
    if(multiplierEdgeInsets.right > 0){
        
        NSLayoutConstraint *right = [NSLayoutConstraint
                                   constraintWithItem:itemView
                                   attribute:(NSLayoutAttributeRight)
                                   relatedBy:(NSLayoutRelationEqual)
                                   toItem:toItem
                                   attribute:(NSLayoutAttributeRight)
                                   multiplier:multiplierEdgeInsets.right
                                   constant:edgeInsts.right];
        [right setActive:YES];
        
    }
    
    if(multiplierEdgeInsets.bottom > 0){
        
        NSLayoutConstraint *bottom = [NSLayoutConstraint
                                     constraintWithItem:itemView
                                     attribute:(NSLayoutAttributeBottom)
                                     relatedBy:(NSLayoutRelationEqual)
                                     toItem:toItem
                                     attribute:(NSLayoutAttributeBottom)
                                     multiplier:multiplierEdgeInsets.bottom
                                     constant:edgeInsts.bottom];
        [bottom setActive:YES];
        
    }
}


/* 单层关系特殊处理 multiplierArry  kindArry 里面必须有2个数据包
 *@param itemView 目标对象
 *@param size 对象大小
 *@param multiplierArry @[@"widh",@"height"] > 0 才设置
 *@param kindArry @[@"=",@">=",@"<="]  这个会特殊判断
 */
- (void)setItemView:(UIView *)itemView
      withHAndWSize:(CGSize)size
 withMultiplierArry:(NSArray *)multiplierArry
   withSizeKindArry:(NSArray *)kindArry{
   
    if(kindArry.count != 2 || multiplierArry.count != 2){
        
        
        
        return;
    }
   
    itemView.translatesAutoresizingMaskIntoConstraints = false;
    
    CGFloat widthMultiplier = [[NSString stringWithFormat:@"%@",multiplierArry[0]] floatValue];
    CGFloat hegihtMultiplier = [[NSString stringWithFormat:@"%@",multiplierArry[1]] floatValue];
    
    if(widthMultiplier > 0){
        
       
        
        
        NSLayoutConstraint *width = [NSLayoutConstraint
                                      constraintWithItem:itemView
                                      attribute:(NSLayoutAttributeWidth)
                                      relatedBy:[self detailStr:kindArry[0]]
                                      toItem:nil
                                      attribute:(NSLayoutAttributeNotAnAttribute)
                                      multiplier:widthMultiplier
                                     constant:size.width];
        
        [width setActive:true];
        
    }
    
    if(hegihtMultiplier > 0){
        
        NSLayoutConstraint *height = [NSLayoutConstraint
                                     constraintWithItem:itemView
                                     attribute:(NSLayoutAttributeHeight)
                                     relatedBy:[self detailStr:kindArry[1]]
                                     toItem:nil
                                     attribute:(NSLayoutAttributeNotAnAttribute)
                                     multiplier:hegihtMultiplier
                                     constant:size.height];
        
        [height setActive:true];
        
    }
    
}
/* 做些特殊处理的
 * @param string  @"=",@">=",@"<="
 * @return NSLayoutRelation
 */
- (NSLayoutRelation)detailStr:(NSString *)string{
    if([string isEqualToString:@"="]){
        
        return  NSLayoutRelationEqual;
    }
    
    if([string isEqualToString:@"<="]){
        
        return NSLayoutRelationLessThanOrEqual;
    }
    
    if([string isEqualToString:@">="]){
        
        return NSLayoutRelationGreaterThanOrEqual;
    }
    
    return NSLayoutRelationEqual;
}

/* 同层之间的关系特殊处理
 * @param itemView 目标itemview
 * @param itemAttribute 参数左边右边等等
 * @param relateBy  大于 等于多余
 * @param toItemView 另一个目标
 * @param toItemAttribute  参数左边右边等等
 * @param multiplier 乘数
 * @param constant  常量
 * @param isAvtivity 开关
 * @return NSLayoutConstraint 类型
 */

- (NSLayoutConstraint *)setItemView:(UIView *)itemView
                  withItemAttribute:(NSLayoutAttribute)itemAttribute
                      withRelatedBy:(NSLayoutRelation)relateBy
                     withToItemView:(UIView *)toItemView
                withToItemAttribute:(NSLayoutAttribute)toItemAttribute
                    withmMultiplier:(CGFloat)multiplier
                       withConstant:(CGFloat)constant
                       withActivity:(BOOL)isAvtivity
    {
    
        if(multiplier == 0){
            
            
            return nil;
        }
        
        itemView.translatesAutoresizingMaskIntoConstraints = false;
    
    NSLayoutConstraint *height = [NSLayoutConstraint
                                  constraintWithItem:itemView
                                  attribute:itemAttribute
                                  relatedBy:relateBy
                                  toItem:toItemView
                                  attribute:toItemAttribute
                                  multiplier:multiplier
                                  constant:constant];
        
        [height setActive:isAvtivity];
        
       
    
        return  height;
}








@end
