//
//  PJLSavePhotoImageNSObject.m
//  BDYue
//
//  Created by pjl on 2016/12/2.
//  Copyright © 2016年 PJL. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "PJLSavePhotoImageNSObject.h"

@interface PJLSavePhotoImageNSObject ()<UIActionSheetDelegate>{


}

@end

@implementation PJLSavePhotoImageNSObject


- (void)createSaveLocalShowView:(UIView *)showView{

    UIActionSheet *uiactionSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"保存相册" otherButtonTitles:nil];
    
    
    [uiactionSheet showInView:showView];

}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{

    if(actionSheet.cancelButtonIndex == buttonIndex){
    
        return;
    }
    
    if(_saveImage == nil || ![_saveImage isKindOfClass:[UIImage class]]){
    
        return;
    }

    [self saveImageToPhotos:_saveImage];
}

- (void)saveImageToPhotos:(UIImage*)savedImage

{

    UIImageWriteToSavedPhotosAlbum(savedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);

}

// 指定回调方法

- (void)image: (UIImage *) image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo

{

    NSString *msg = nil ;

    if(error != NULL){
   
        msg = @"保存图片失败" ;
  
    }else{

        msg = @"保存图片成功" ;
       
    }

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"保存图片结果提示"
                        
                                                    message:msg
                       
                                                   delegate:self
                     
                                          cancelButtonTitle:@"确定"
                       
                                          otherButtonTitles:nil];
    
    [alert show];
    
    if(_pjlListenBlock != nil){
        
        _pjlListenBlock(true);
        
    }
}

- (void)setListenBlcok:(PJLListenFinshLoadingBlock)block{
    
    _pjlListenBlock = block;
}

@end
