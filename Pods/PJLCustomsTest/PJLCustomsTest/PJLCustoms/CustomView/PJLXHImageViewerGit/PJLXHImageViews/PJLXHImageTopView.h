//
//  PJLXHImageTopView.h
//  BDYue
//
//  Created by pjl on 2017/7/19.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "CustomView.h"

@interface PJLXHImageTopView : CustomView

- (void)setTopLabelString:(NSString *)topString;

@end
