//
//  PJLSavePhotoImageNSObject.h
//  BDYue
//
//  Created by pjl on 2016/12/2.
//  Copyright © 2016年 PJL. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^PJLListenFinshLoadingBlock)(BOOL isSucess);

@interface PJLSavePhotoImageNSObject : NSObject

- (void)createSaveLocalShowView:(UIView *)showView;

- (void)saveImageToPhotos:(UIImage*)savedImage;

@property(nonatomic) PJLListenFinshLoadingBlock pjlListenBlock;

@property (nonatomic)UIImage *saveImage;

- (void)setListenBlcok:(PJLListenFinshLoadingBlock)block;

@end
