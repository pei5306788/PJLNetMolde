//
//  PJLXHImageTopView.m
//  BDYue
//
//  Created by pjl on 2017/7/19.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLXHImageTopView.h"
#import "CustomLabel.h"
@interface PJLXHImageTopView(){

    CustomLabel *_topLabel;
    
}

@end

@implementation PJLXHImageTopView

- (id)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if(self){
        
        _topLabel = [[CustomLabel alloc]initWithFrame:self.bounds];
        
        [_topLabel setBackgroundColor:[UIColor clearColor]];
        
        _topLabel.textAlignment = NSTextAlignmentCenter;
        
        [_topLabel setTextColor:[UIColor whiteColor]];
        
        [_topLabel setFont:[UIFont systemFontOfSize:14.0]];
        
        [self addSubview:_topLabel];
        
        [self setBackgroundColor:[UIColor clearColor]];
    
    }
    return self;
}

- (void)setTopLabelString:(NSString *)topString{

    _topLabel.text = topString;

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
