//
//  PJLVCShareVCObject.m
//  PJLCarLearn
//
//  Created by 裴建兰 on 2017/9/13.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLVCShareVCObject.h"

static PJLVCShareVCObject *pjlVCShareInstance;

@implementation PJLVCShareVCObject

+(PJLVCShareVCObject *)pjlVCShareInstance{
    
    @synchronized (self) {
        
        if(pjlVCShareInstance == nil){
            
            pjlVCShareInstance = [[self alloc]init];
           
            
            pjlVCShareInstance.pjlAttrebuteWay = [[PJLAttributeWay alloc]init];
            
        }
    }
    
    
    return pjlVCShareInstance;
}


@end
