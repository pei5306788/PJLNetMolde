//
//  PJLBaseVC.m
//  PJLCarLearn
//
//  Created by 裴建兰 on 2017/9/13.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLBaseVC.h"
#import "PJLVCShareVCObject.h"
@interface PJLBaseVC ()

@end

@implementation PJLBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    

    if(_isNeedTableView){
    
        self.edgesForExtendedLayout = UIRectEdgeNone;
       
        
        _myTableView = [[CustomTableView alloc]init];;
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.translatesAutoresizingMaskIntoConstraints = false;
        [self.view addSubview:_myTableView];
        
        
        _left = [NSLayoutConstraint constraintWithItem:_myTableView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:self.leftFloat];
        
        
          _top = [NSLayoutConstraint constraintWithItem:_myTableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:self.topFloat];
        
        
          _right = [NSLayoutConstraint constraintWithItem:_myTableView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0 constant:self.rightFloat];
        
        
          _bottom = [NSLayoutConstraint constraintWithItem:_myTableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:self.bottomFloat];
        
    
        [_left setActive:true];
        [_top setActive:true];
        [_right setActive: true];
        [_bottom setActive:true];
        
    }
    
   
   
    // Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
    // Dispose of any resources that can be recreated.
}





- (void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    if(self.navigationController != nil){
        [PJLVCShareVCObject pjlVCShareInstance].selectNagateionController = self.navigationController;
    }

    [PJLVCShareVCObject pjlVCShareInstance].selectNowVC = self;

    if(self.tabBarController != nil){


        [PJLVCShareVCObject pjlVCShareInstance].selectTabBarController = self.tabBarController;

    }
    
    if(_isNeedTableView == true){
        
        _myTableView.delegate = self;
        
        _myTableView.dataSource = self;
        
    }
    
    
}

- (void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:animated];

    if(_isNeedTableView == true){
        
        _myTableView.delegate = nil;
        
        _myTableView.dataSource = nil;
    
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifitent = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifitent];
    if(cell == nil){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifitent];
        
    }
    
    return cell;
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
