//
//  PJLVCShareVCObject.h
//  PJLCarLearn
//
//  Created by 裴建兰 on 2017/9/13.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//这个是方便直接读取相关内存
#import "PJLAttributeWay.h"

#define PJLStatusHeight [[UIApplication sharedApplication] statusBarFrame].size.height

#define PJLNAFrameHeight [[[UINavigationController alloc]init].navigationBar frame].size.height

#define PJLTabbarFrameHeight (PJLStatusHeight > 20 ? 83 : 49)

@interface PJLVCShareVCObject : NSObject

+(PJLVCShareVCObject *)pjlVCShareInstance;

@property (nonatomic) UINavigationController *selectNagateionController; //当前导航栏
@property (nonatomic) UITabBarController *selectTabBarController; //当前选中的tabbar

@property (nonatomic) UIViewController *selectNowVC; //当前页面所在的vc

@property (nonatomic) PJLAttributeWay *pjlAttrebuteWay; //相关内容的方法

@end
