//
//  PJLBaseVC.h
//  PJLCarLearn
//
//  Created by 裴建兰 on 2017/9/13.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTableView.h"
@interface PJLBaseVC : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic) BOOL isNeedTableView;
//这个是兼容最好的 如果不是那就需要自己适配了啊
@property (nonatomic) CustomTableView *myTableView;

@property (nonatomic) NSLayoutConstraint *left;
@property (nonatomic) NSLayoutConstraint *top;
@property (nonatomic) NSLayoutConstraint *right;
@property (nonatomic) NSLayoutConstraint *bottom;



/*
 *适配的简单约束方便
 */

@property (nonatomic) CGFloat topFloat;

@property (nonatomic) CGFloat leftFloat;

@property (nonatomic) CGFloat rightFloat;

@property (nonatomic) CGFloat bottomFloat;


@end
