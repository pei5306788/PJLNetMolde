//
//  PJLLabelMembers_1.h
//  PJLCustomsFrame
//
//  Created by pjl on 2017/2/27.
//  Copyright © 2017年 PJL. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface PJLLabelMembers_1 : NSObject

//NSParagraphStyleAttributeName 段落的风格（设置首行，行间距，对齐方式什么的）看自己需要什么属性，写什么

//NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];

//paragraphStyle.lineSpacing = 10;// 字体的行间距

//paragraphStyle.firstLineHeadIndent = 20.0f;//首行缩进

//paragraphStyle.alignment = NSTextAlignmentJustified;//（两端对齐的）文本对齐方式：（左，中，右，两端对齐，自然）

//paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;//结尾部分的内容以……方式省略 ( "...wxyz" ,"abcd..." ,"ab...yz")

//paragraphStyle.headIndent = 20;//整体缩进(首行除外)

//paragraphStyle.tailIndent = 20;//

//paragraphStyle.minimumLineHeight = 10;//最低行高

//paragraphStyle.maximumLineHeight = 20;//最大行高

//paragraphStyle.paragraphSpacing = 15;//段与段之间的间距

//paragraphStyle.paragraphSpacingBefore = 22.0f;//段首行空白空间/* Distance between the bottom of the previous paragraph (or the end of its paragraphSpacing, if any) and the top of this paragraph. */

//paragraphStyle.baseWritingDirection = NSWritingDirectionLeftToRight;//从左到右的书写方向（一共➡️三种）

//paragraphStyle.lineHeightMultiple = 15;/* Natural line height is multiplied by this factor (if positive) before being constrained by minimum and maximum line height. */

//paragraphStyle.hyphenationFactor = 1;//连字属性 在iOS，唯一支持的值分别为0和1


@property (nonatomic)NSMutableParagraphStyle *pjlNSMutableParagraphStyle; //详细属性

@property (nonatomic) NSString *pjlNSMutableParagraphStyleJsonStr; //详细属性字典

@property (nonatomic) NSString *contentStr;  //内容

@property (nonatomic) UIColor *contentColor; //内容字体颜色

@property (nonatomic) UIFont *contentFont;   //内容字体大小

@property (nonatomic) NSInteger DeFaultIndex; //目前已经有的模型的Index

/*
 优先读取是否存在 imageName ，然后再读取 imagePath 两个都填写的话 就是按照顺序 文字 + imageName + imagePath
 */

@property (nonatomic) NSString *imagePath; //路径地址 地址 + 图片名字 读取速度相对变慢

@property (nonatomic) NSString *imageName; //图片名字本地的图片 直接从内存里面本地读取啊

@property (nonatomic) CGRect imageFrame; //图片设置大小



@end
