//
//  PJLNSTextAttachment.h
//  PJLCustomsTest
//
//  Created by pjl on 2017/3/9.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PJLNSTextAttachment : NSTextAttachment

@property (nonatomic) NSString *imageName;

@property (nonatomic) NSString *imagePath;

@property (nonatomic) CGRect imageFrame;

@end
