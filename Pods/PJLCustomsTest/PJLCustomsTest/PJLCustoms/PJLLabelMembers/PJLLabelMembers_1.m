//
//  PJLLabelMembers_1.m
//  PJLCustomsFrame
//
//  Created by pjl on 2017/2/27.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLLabelMembers_1.h"

@implementation PJLLabelMembers_1

- (id)init{

    self = [super init];
    if(self){
        
        self.contentStr = @"";
        self.contentFont = [UIFont systemFontOfSize:14.0];
        self.contentColor = [UIColor blackColor];
        self.DeFaultIndex = -1;
        self.imageName = @"";
        self.imagePath = @"";
    }
    return self;
}

@end
