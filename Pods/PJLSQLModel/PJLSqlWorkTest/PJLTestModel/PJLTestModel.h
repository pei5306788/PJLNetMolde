//
//  PJLTestModel.h
//  PJLSqlWorkTest
//
//  Created by 裴建兰 on 2017/8/22.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLSQLModel.h"
//#import "PJLContentModel.h"
@interface PJLTestModel : PJLSQLModel

@property (nonatomic) NSInteger id;

@property (nonatomic) NSString *userName;

@property (nonatomic) NSString *passWord;


@property (nonatomic) NSDictionary *getDic;

//@property (nonatomic) PJLContentModel *pjlContentModel;

@property (nonatomic) NSArray *contentArryModel;

/*
 
 PJLTestModel *pjlTestModel = [[PJLTestModel alloc]init];
 
 [pjlTestModel deleteContinue:nil withSucessBlock:^(BOOL isSucess) {
 
 }];
 
 [pjlTestModel dropTableWithBlock:^(BOOL isSucess) {
 
 
 
 
 
 }];
 
 
 for(NSInteger i = 0 ; i <= 20; i ++){
 
 
 
 pjlTestModel.userName = [NSString stringWithFormat:@"名字%ld",i];
 pjlTestModel.passWord = [NSString stringWithFormat:@"密码%ld",i];
 
 [pjlTestModel saveModel];  //保存数据
 
 }
 
 
 
 [pjlTestModel getSqlCheckContinue:nil withSucessBlock:^(NSArray *returnArry) { //条件查询
 
 }]; //条件查询
 
 
 
 [pjlTestModel saveOrUpDataModel:@[@"userName",@"=",@"名字1",@"passWord",@"=",@"密码1"]]; //更新或者保存
 
 
 [pjlTestModel getQueryWithContinue:nil
 withPageIndex:0
 withEachNumber:100
 withDescyKey:@"id"
 withDesc:nil
 withBlock:^(NSArray *returnArry) {
 
 
 
 
 _dataList = returnArry;
 
 [_pjlTableView reloadData];
 }];
 

 */

@end
