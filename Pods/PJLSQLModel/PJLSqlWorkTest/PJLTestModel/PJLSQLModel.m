//
//  PJLSQLModel.m
//  PJLSqlWorkTest
//
//  Created by 裴建兰 on 2017/8/22.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import "PJLSQLModel.h"
#import "MJExtension.h"

#import "BGFMDB.h"
//#import <objc/runtime.h>

@interface PJLSQLModel(){

    BOOL _isCheckOrCreate;

}

@end

@implementation PJLSQLModel

- (id)init{

    self = [super init];
    if(self){

        [self pjlInit];
    }
    return self;
}

- (NSString *)getPjlTableName{
    
    NSString *pjlTableName = [NSString stringWithFormat:@"%@",[self class]]; //获取表单名字
    
    pjlTableName = [pjlTableName stringByReplacingOccurrencesOfString:@"." withString:@"_"];
    
    return pjlTableName;
}

- (void)pjlInit{ //初始化表单
    
    if(_isCheckOrCreate == YES){
    
        return;
    }
    
    NSString *pjlTableName = [self getPjlTableName];
    [[BGFMDB intance] isExistWithTableName:pjlTableName complete:^(BOOL isExist) { //初始化数据库
        if(!isExist){
           
            
            NSArray *keyArry = [self getModelMember:self]; //获取所有的参数keyValues
            
            NSMutableArray *testArry = [[NSMutableArray alloc]initWithArray:keyArry];
            [testArry removeObject:@"id"]; //默认 每个数据库都有id 的 所以创建的时候不需要 不过建议最好数据库返回不要存在id这个对象或者二次处理一下
            
            [[BGFMDB intance] createTableWithTableName:pjlTableName keys:testArry complete:^(BOOL isSuccess) {
                if (isSuccess) {
                    NSLog(@"创表成功");
                } else {
                    NSLog(@"创表失败");
                }
            }];//建表语句
            
        }else{ //既然已经存在表了 那就再检查是否有新加的数据 如果有新加的那么久添加新的数据类型
            
            NSArray *keyArry = [self getModelMember:self]; //获取所有的keys
            
            NSMutableArray *testArry = [[NSMutableArray alloc]initWithArray:keyArry];
            [testArry removeObject:@"id"]; //默认 每条数据 都会有id 建议数据库
            
            [[BGFMDB intance] pjlAddKeyTableName:pjlTableName addKey:keyArry];
            
            NSLog(@"存在表了");
            
        }
        
    }];
    _isCheckOrCreate = YES;

    self.sqlPath = [BGFMDB intance].sqlFilePath;
}

/*
 条件为空 
 那就是直接新加内容，
 如果加上条件的话 
 那就是更新
 */

- (void)saveOrUpDataModel:(NSArray *)condition{
    
    if(_isCheckOrCreate == NO){
    
        [self pjlInit];
    }
    
    NSString *pjlTableName = [self getPjlTableName];
    
    if(condition == nil){
    
       
        [self saveModel];
        
        
    }else{
    
        [[BGFMDB intance] queryWithTableName:pjlTableName
                                        keys:nil
                                       where:condition
                                    complete:^(NSArray *array) {
                                      
                                        if(array.count == 0){  //如果没有一个满足的话
                                        
                                            [self saveModel]; //那就保存到数据库中
                                        
                                        }else{ //如果搜索到数据的话 那就更新数据
                                        
                                            [self upDataModelContine:condition
                                                          withUpData:nil];
                                        }
                                    }];
    }
}

/*
 保存数据
 */
- (void)saveModel{

    if(_isCheckOrCreate == NO){
        
        [self pjlInit];
    }

    NSString *pjlTableName = [self getPjlTableName];
    NSDictionary *getModelDic = [self getObjectData:self];
    
    
    getModelDic = [self saveDicSecondDeatal:getModelDic];
    
    [[BGFMDB intance] insertIntoTableName:pjlTableName Dict:getModelDic complete:^(BOOL isSuccess) {
        
        if(isSuccess){
            
            NSLog(@"添加数据成功");
            
        }
        
    }];

}

/*
 更新本地数据
  @param saveDic 如果为nil 那么就更新整个模型
 */
- (void)upDataModelContine:(NSArray *)conditionArry
                withUpData:(NSDictionary *)saveDic{

    if(_isCheckOrCreate == NO){
        
        [self pjlInit];
    }


    NSString *pjlTableName = [self getPjlTableName];
    if(saveDic == nil){ //如果没有准确更新那就自断 那么就是整体更新的问题
        saveDic = [self getObjectData:self];
    }
    
    
    [[BGFMDB intance] updateWithTableName:pjlTableName
                                valueDict:saveDic
                                    where:conditionArry
                                 complete:^(BOOL isSuccess) {
                                     
                                     if(isSuccess){
                                         NSLog(@"更新数据成功");
                                     }else{
                                         NSLog(@"更新数据失败");
                                     }
                                 }];
}

/*
 获取数据 
 @param continueArry 条件语句
 
 */

- (void)getSqlCheckContinue:(NSArray *)continueArry
            withSucessBlock:(PJLSqlCheckBlock)pjlBlock{

    if(_isCheckOrCreate == NO){
        
        [self pjlInit];
    }

    NSString *pjlTableName = [self getPjlTableName];
    
    [[BGFMDB intance] queryWithTableName:pjlTableName
                                    keys:nil
                                   where:continueArry
                                complete:^(NSArray *array) {
                                    
        if(self->_userModel != nil ){
                                        
            if([[self->_userModel class] respondsToSelector:@selector(mj_objectArrayWithKeyValuesArray:)]){
                                        
                                              array = [self getSqlModelArry:array];
                                            
                NSArray *modelArry = [[self->_userModel class] mj_objectArrayWithKeyValuesArray:array];
                                            
                                            pjlBlock(modelArry);
                                        }else{
                                        
                                            pjlBlock(array);

                                        }
                                        
                                      
                                    }else{
                                        
                                        pjlBlock(array);
                                    }
                                    
                                }];

}

/*
 获取分页数据
 */

- (void)getQueryWithContinue:(NSArray *)continueArry
               withPageIndex:(NSInteger)paramIndex
              withEachNumber:(NSInteger)paramNumber
                withDescyKey:(NSString *)descyKey
                    withDesc:(NSString *)paramDesc // //ASC/DESC,升序,降序
                   withBlock:(PJLSqlCheckBlock)pjlBlock{
    
    if(_isCheckOrCreate == NO){
        
        [self pjlInit];
    }

    NSString *pjlTableName = [self getPjlTableName];
    if(paramDesc == nil){
    
        paramDesc = @"ASC";
    
    }
    
        [[BGFMDB intance] pjlGetPageSql:pjlTableName
                          withPageIndex:paramIndex
                         withPageNumber:paramNumber
                           withDescyKey:descyKey
                               withDesc:paramDesc
                      withWhereContiune:continueArry
                               complete:^(NSArray *array) {
                            
            if(self->_userModel != nil ){
                                    
                if([[self->_userModel class] respondsToSelector:@selector(mj_objectArrayWithKeyValuesArray:)]){
                                           
                                           array = [self getSqlModelArry:array];
                                           
                    NSArray *modelArry = [[self->_userModel class] mj_objectArrayWithKeyValuesArray:array];
                                           
                                           pjlBlock(modelArry);
                                       }else{
                                           
                                           pjlBlock(array);
                                           
                                       }
                                       
                                       
                                   }else{
                                       
                                       pjlBlock(array);
                                   }
                                   
            
        }];

    

}

/*
 删除活晴空数据
 */

- (void)deleteContinue:(NSArray *)continueArry
            withSucessBlock:(void (^)(BOOL isSucess))block
{
    if(_isCheckOrCreate == NO){
        
        [self pjlInit];
    }


    NSString *pjlTableName = [self getPjlTableName];
    if(continueArry == nil){
    
        [[BGFMDB intance] clearTable:pjlTableName complete:^(BOOL isSuccess) {
            
            block(isSuccess);
        }];
    
    }else{
    
    
        [[BGFMDB intance] deleteWithTableName:pjlTableName
                                        where:continueArry
                                     complete:^(BOOL isSuccess) {
            
            
              block(isSuccess);
            
        }];
        
    }

}

- (void)dropTableWithBlock:(void (^)(BOOL isSucess))block{ //删除这张表

    if(_isCheckOrCreate == NO){
        
        [self pjlInit];
    }
    
    
    NSString *pjlTableName = [self getPjlTableName];
    [[BGFMDB intance] dropTable:pjlTableName complete:^(BOOL isSuccess) {
        
        self->_isCheckOrCreate = NO;
        block(isSuccess);
        
    }];

}

/*
 工具方法
 */
/**************************开始***********************************/
- (NSMutableArray *)getModelMember:(id)model{ //获取所有对象所有模型的对象
    
    NSMutableArray *memberArry = [[NSMutableArray alloc]init];
    unsigned int propsCount;
    
    objc_property_t *props = class_copyPropertyList([model class], &propsCount);//获得属性列表
    for(int i = 0;i < propsCount; i++)
    {
        objc_property_t prop = props[i];
        
        NSString *propName = [NSString stringWithUTF8String:property_getName(prop)];
        
        [memberArry addObject:propName];
        NSLog(@"%@",propName);
        
    }
    
    free(props);
    
    return memberArry;
    
    
}


- (NSDictionary*)getObjectData:(id)obj  //把model 转成字典
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    unsigned int propsCount;
    objc_property_t *props = class_copyPropertyList([obj class], &propsCount);//获得属性列表
    
    for(int i = 0;i < propsCount; i++)
    {
        objc_property_t prop = props[i];
        
        NSString *propName = [NSString stringWithUTF8String:property_getName(prop)];//获得属性的名称
        id value = [obj valueForKey:propName];//kvc读值
        if(value == nil || [propName isEqualToString:@"id"])// id 数据库里面已经存在了就不保存了
        {
            value = [NSNull null];
            continue; //如果时空的话 那么就不保存了啊
        }
        else
        {
            value = [self getObjectInternal:value];//自定义处理数组，字典，其他类
        }
        [dic setObject:value forKey:propName];
    }
    free(props);
    return dic;
}

- (id)getObjectInternal:(id)obj //获取对象参数
{
    if([obj isKindOfClass:[NSString class]]
       || [obj isKindOfClass:[NSNumber class]]
       || [obj isKindOfClass:[NSNull class]])
    {
        return obj;
    }
    
    if([obj isKindOfClass:[NSArray class]])
    {
        NSArray *objarr = obj;
        NSMutableArray *arr = [NSMutableArray arrayWithCapacity:objarr.count];
        for(int i = 0;i < objarr.count; i++)
        {
            [arr setObject:[self getObjectInternal:[objarr objectAtIndex:i]] atIndexedSubscript:i];
        }
        return arr;
    }
    if([obj isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *objdic = obj;
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:[objdic count]];
        for(NSString *key in objdic.allKeys)
        {
            [dic setObject:[self getObjectInternal:[objdic objectForKey:key]] forKey:key];
        }
        return dic;
    }
    return [self getObjectData:obj];
}

/*******************结束*******************************/
//保存前特殊处理一下
- (NSDictionary *)saveDicSecondDeatal:(NSDictionary *)oldDic{
    
    NSMutableDictionary *returnNewDic = [[NSMutableDictionary alloc]initWithDictionary:oldDic];
    
    for(NSString *eachKey  in returnNewDic.allKeys){
        
        id  getVlue = [returnNewDic objectForKey:eachKey];
        
       
        if([getVlue isKindOfClass:[NSArray class]]
           || [getVlue isKindOfClass:[NSDictionary class]]
           || [getVlue isKindOfClass:[NSObject class]]){
             NSString *getString = @"";
            getString = [getVlue mj_JSONString];
            
        
            [returnNewDic setValue:getString forKey:eachKey];
            
        }
        
        
        
    }
    return returnNewDic;
}

- (NSArray *)getSqlModelArry:(NSArray *)returnArry{
    
    NSMutableArray *modelArry = [[NSMutableArray alloc]initWithArray:returnArry];
    
    for(NSInteger i = 0 ; i < modelArry.count; i ++){
        
        NSMutableDictionary *getEachDic = [[NSMutableDictionary alloc]initWithDictionary:modelArry[i]];
        
        for(NSInteger j = 0; j < getEachDic.allKeys.count; j ++){
            
            NSString *eachKey = getEachDic.allKeys[j];
            
            NSString *jsonString = [getEachDic objectForKey:eachKey];
            
            if (jsonString == nil ) {
//                return nil;
                continue;
            }
            
            if(![jsonString isKindOfClass:[NSString class]]){
                
                jsonString = [NSString stringWithFormat:@"%@",jsonString];
                
            }
            
            
            NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
            NSError *err;
             id getValue = [NSJSONSerialization JSONObjectWithData:jsonData
                                                           options:NSJSONReadingMutableContainers
                                                             error:&err];
            if(!err)
            {
                if([getValue isKindOfClass:[NSObject class]]
                   ||[getValue isKindOfClass:[NSArray class]]
                   ||[getValue isKindOfClass:[NSDictionary class]]){
                    
//                    [getEachDic setValue:getValue forKey:eachKey];
                    [getEachDic setObject:getValue forKey:eachKey];
                    
                    
                }
                
            }
            
        }
        
        modelArry[i] = getEachDic;
        
    }
    

    return modelArry;
}

/* 活动切换数据库
 * @param newFileName 新的数据库地址 相对document
 * @param backLoacalSql  yes  读取本地数据库  NO 切换数据库
 *
 */
- (void)setNewSqlFilePath:(NSString *)newFileName
           backToLocalSql:(BOOL)backLoacalSql{
    
    [[BGFMDB intance] setNewSqlFilePath:newFileName backToLocalSql:backLoacalSql];
    
    _isCheckOrCreate = NO;
    
    [self pjlInit];
    
}


@end
