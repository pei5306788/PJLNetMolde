//
//  PJLSQLModel.h
//  PJLSqlWorkTest
//
//  Created by 裴建兰 on 2017/8/22.
//  Copyright © 2017年 PJL. All rights reserved.
//

#import <Foundation/Foundation.h>




/*
 返回的可能是数组（字典） 也可能是数据（模型）跟userModel 是否复制的有效值 有关

 */
typedef void(^PJLSqlCheckBlock)(NSArray *returnArry); //
@interface PJLSQLModel : NSObject  //这边可以居于jsonModel 来做其他操作

@property (nonatomic) id userModel; //返回的数组模型如果没有的话 就是字典 当前正在用的数据 //此方法不适合顺便带二次树立


- (void)pjlInit; //初始化表单 默认都要检查的

@property (nonatomic) NSString *sqlPath;


/*
 获取数据
 @param continueArry 条件语句
 
 */
- (void)getSqlCheckContinue:(NSArray *)continueArry
            withSucessBlock:(PJLSqlCheckBlock)pjlBlock;


/*
 更新本地数据
 @param saveDic 如果为nil 那么就更新整个模型
 */
- (void)upDataModelContine:(NSArray *)conditionArry
                withUpData:(NSDictionary *)saveDic;


/*
 保存数据
 */
- (void)saveModel;

/*
 条件为空
 那就是直接新加内容，
 如果加上条件的话
 那就是更新
 */
- (void)saveOrUpDataModel:(NSArray *)condition;


/*
 获取分页数据
 */

- (void)getQueryWithContinue:(NSArray *)continueArry
               withPageIndex:(NSInteger)paramIndex
              withEachNumber:(NSInteger)paramNumber
                withDescyKey:(NSString *)descyKey
                    withDesc:(NSString *)paramDesc  //ASC/DESC,升序,降序
                   withBlock:(PJLSqlCheckBlock)pjlBlock;


/*
 删除活晴空数据
 */

- (void)deleteContinue:(NSArray *)continueArry
       withSucessBlock:(void (^)(BOOL isSucess))block;


- (void)dropTableWithBlock:(void (^)(BOOL isSucess))block; //删除这张表


/* 活动切换数据库  注意要先把文件路径创建好。
 * @param newFileName 新的数据库地址 相对document 例如: sss/sqiteName(.sqite)  .sqite不需要填写
 * @param backLoacalSql  yes  读取本地数据库  NO 切换数据库
 *
 */
- (void)setNewSqlFilePath:(NSString *)newFileName
           backToLocalSql:(BOOL)backLoacalSql;

@end
